package server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class TClient extends JFrame implements Runnable{
	
	JTextField tfMsg;
	JButton btnOrder;
	
	String ip = "192.168.0.6";
	//String ip = "192.168.0.12";
	int port = 5000;
	Socket s;
	PrintWriter pw;
	BufferedReader br;
	
	String msg = "";
	TClient() {
		
		setLayout(null);
		tfMsg = new JTextField();
		btnOrder = new JButton("전송");
		
		tfMsg.setBounds(10, 200, 360, 50);
		btnOrder.setBounds(150, 260, 100, 50);
		
		vchat();	// 현재 클라이언트를 쓰레드로 실행하는 메서드
		
		btnOrder.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {			
				// 클라이언트 -> 서버 메시지 전송하기
				msg = tfMsg.getText();
				pw.println("[손님이보냄]" + msg);
				pw.flush();
				tfMsg.setText("");
				tfMsg.requestFocus();
				
			}
		});
		
		add(tfMsg);
		add(btnOrder);	
		
		setTitle("Client");
		setBounds(750, 20, 400, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	@Override
	public void run() {
		try {
			s = new Socket(ip, port);
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
		
			String data = null;
			while(true){	// 서버로부터 가져온 것을 계속 반복해서 읽는다.
				data = br.readLine();
				System.out.println(data);
				if (data.contains("[서버가보냄] 음료완성/")) {
					String drink = data.replace("[서버가보냄] 음료완성/", "");
					int result = JOptionPane.showConfirmDialog(null, "주문하신 " + drink +" 음료나왔습니다.", "[음료완성]", JOptionPane.OK_OPTION);
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("포트번호 오류");
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.out.println("IP 번호 오류");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void vchat() {

		Thread th = new Thread(this); // 현재 객체를 쓰레드로
		th.start(); // 쓰레드 시작

	} // vchat() end

	public static void main(String[] args) {
		new TClient();
	}
}
