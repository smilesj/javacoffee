package server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.plaf.synth.SynthSplitPaneUI;

public class TServer extends JFrame {

	ServerSocket ss;
	PrintWriter pw;
	BufferedReader br;

	JButton btnComplete;
	JList orderList;
	DefaultListModel<String> orderModel;

	ArrayList<MTServer> list = new ArrayList<MTServer>();

	boolean make = false;
	int index;

	public TServer() {

		orderList = new JList(new DefaultListModel<String>());
		orderModel = (DefaultListModel<String>) orderList.getModel();
		btnComplete = new JButton("완성");

		setLayout(null);

		orderList.setBounds(10, 10, 360, 370);
		btnComplete.setBounds(10, 400, 360, 50);

		btnComplete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				make = true;
				index = -1;

				for (MTServer s : list) {
					System.out.println(s.clientIP);
					index = orderList.getSelectedIndex();
					if (index >= 0) {
						String ip = orderList.getSelectedValue().toString().substring(0, orderList.getSelectedValue().toString().indexOf("#"));
						System.out.println("서버에서 완료버튼 누를때 IP => " + ip);
						if (s.getClient().getInetAddress().getHostAddress().equals(ip)) {
							s.push();
							break;
						}
					}
				}
			}
		});

		add(btnComplete);
		add(orderList);

		setTitle("Server");
		setBounds(200, 20, 400, 20);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(false);
		startServer();
	}

	public void startServer() {
		System.out.println("--- Server Start ---");
		try {
			ss = new ServerSocket(5000); // 서버 소켓 생성
			while (true) {
				Socket client = ss.accept(); // client : 접속한 사용자
				MTServer mts = new MTServer(client); // 멀티 쓰레드 서버
				list.add(mts);
				mts.start();
				System.out.println(" >>>>> " + client.getInetAddress().getHostAddress() + " 접속");
			}
		} catch (IOException e) {
			System.out.println("서버에러");
		}
	}

	public static void main(String[] args) {
		new TServer();
	}

	class MTServer extends Thread {

		Socket client;

		public Socket getClient() {
			return client;
		}

		String clientIP;
		BufferedReader br;
		PrintWriter pw;

		MTServer(Socket client) {
			this.client = client;
			try {
				br = new BufferedReader(new InputStreamReader(client.getInputStream()));
				pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())));
				clientIP = client.getInetAddress().getHostAddress();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			while (true) {
				String clientMsg = null;
				try {
					clientMsg = br.readLine();
					if (clientMsg != null) {
						System.out.println("[" + clientIP + "] : " + clientMsg);
						clientMsg = clientIP + "#" + clientMsg;
						System.out.println("C->S >> clientMsg : " + clientMsg);
						String[] cmArr = clientMsg.split("#"); // 클라이언트 메시지 배열로

						for (int i = 0; i < cmArr.length; i++) {
							System.out.println("\tcmArr[" + i + "] = " + cmArr[i]);
						}

						if (cmArr[1].equals("[주문]")) {
							setVisible(true);
							int result = JOptionPane.showConfirmDialog(null, clientMsg, "주문요청", JOptionPane.YES_NO_OPTION);
							if (result == JOptionPane.YES_OPTION) {
								orderModel.addElement(clientMsg);
								String serverMsg = "주문완료";
								pw.println(serverMsg);
								pw.flush();
							} else if (result == JOptionPane.NO_OPTION) {
								String serverMsg = "주문취소";
								pw.println(serverMsg);
								pw.flush();
							}
						}
					}

				} catch (IOException e) {
					
				} 

			}

		}

		public void push() {
			if (orderList.getSelectedIndex() >= 0) {
				String sMsg = orderList.getSelectedValue().toString().replace("[주문]", "[완성]");
				pw.println(sMsg);
				pw.flush();
				orderModel.remove(index);
				if (orderModel.getSize() == 0) {
					setVisible(false);
					setBounds(200, 20, 400, 20);
				}
			}		
		}
	}
}