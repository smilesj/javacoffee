package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import database.dao.DrinkDAO;
import database.vo.DrinkVO;

public class TeaMenu implements ActionListener{
	JPanel jp;
	JButton[] btnTeaMenu = new JButton[12];
	int[] price = new int[12]; // menu price
	Order order;
	
	public TeaMenu(Order order) {
		// initialize
		this.jp = order.jpMenu;
		jp.removeAll();
		this.order = order;
		
		Font f = new Font("���� ����", Font.BOLD, 15);

		// menu naming
		for(int i =0; i<12; i++){
			btnTeaMenu[i] = new JButton();
			btnTeaMenu[i] = new JButton();
			btnTeaMenu[i].setBackground(new Color(241,195,90));
			btnTeaMenu[i].setBorderPainted(false);
			btnTeaMenu[i].setFont(f);
			btnTeaMenu[i].setForeground(Color.darkGray);
		}

		DrinkDAO d= new DrinkDAO();
		ArrayList<DrinkVO> list = d.selectTea();
		ArrayList li = new ArrayList();
		for(DrinkVO vo : list){
			li.add(vo.getDname());
		}
		for(int i=0; i<li.size(); i++){
			if(i%3==0)	btnTeaMenu[i/3].setText((String)li.get(i));
		}
	
		// set price
		for(int i =0; i<12; i++){
			price[i] = 0;
		}
		ArrayList li1 = new ArrayList();
		for(DrinkVO vo : list){
			li1.add(vo.getPrice());
		}
		for(int i=0; i<li1.size(); i++){
			if(i%3==0)	price[i/3] = (int) li1.get(i);
		}
		
		for(int i=0; i<li1.size(); i++){
			System.out.println((String)li.get(i) +" : "+(int)li1.get(i));
		}


		// button layout
		jp.setLayout(null);
		for (int i = 0; i < 12; i++) {
			btnTeaMenu[i].setBounds(170 * (i % 4) + 30, 170 * (i / 4) + 50, 140, 120);
			jp.add(btnTeaMenu[i]);
		}
		jp.repaint();

		// call action listener
		for (int i = 0; i < btnTeaMenu.length; i++) {
			btnTeaMenu[i].addActionListener(this);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		for (int i = 0; i < btnTeaMenu.length; i++) {
			if (obj == btnTeaMenu[i]&&price[i]!=0) {
				Popup popup = new Popup(btnTeaMenu[i].getLabel(), price[i], order);
			}
		}
	}

}
