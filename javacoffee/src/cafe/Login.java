package cafe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.dao.CafeDAO;
import server.TServer;

public class Login extends JPanel implements ActionListener, MouseListener {
	
	static String COSTOMER_HP = "01000000000";
	JPanel jp;
	JPanel jpCal;
	JLabel lbId;
	JLabel lbPw;
	JTextField tfId;
	JPasswordField tfPw;
	JButton btnLogin; // 로그인
	JButton[][] btnNum;
	String[] num = {"7", "8", "9", "4", "5", "6", "1", "2", "3", "0", "C", "입력"};
	
	Component clickCom;
	String cname = "";
	Login(){
	
		setLayout(null);
		
		// 컴포넌트 생성
		jp = new JPanel();
		jpCal = new JPanel();
		lbId = new JLabel("ID", JLabel.RIGHT);
		lbPw = new JLabel("PASSWORD", JLabel.RIGHT);
		tfId = new JTextField("cafe");
		tfPw = new JPasswordField();
		btnLogin = new JButton("로그인");
		btnNum = new JButton[4][3];
	
		
		// 컴포넌트 위치, 크기, 초기 설정
		jpCal.setLayout(null);
		jpCal.setBounds(600, 225, 250, 410);
		lbId.setBounds(80, 220, 120, 100);
		lbPw.setBounds(80, 370, 120, 100);
		tfId.setBounds(220, 220, 270, 100);
		tfPw.setBounds(220, 370, 270, 100);
		btnLogin.setBounds(220, 520, 270, 120);
		jpCal.setBackground(new Color(215,188,171));
		
		Font f = new Font("맑은 고딕", Font.BOLD, 20);
		Font c = new Font("맑은 고딕", Font.BOLD, 15);
		lbId.setFont(f);
		lbPw.setFont(f);
		tfId.setFont(f);
		tfPw.setFont(f);
		btnLogin.setFont(f);
		btnLogin.setBackground(new Color(173,93,94));
		btnLogin.setBorderPainted(false);
		btnLogin.setForeground(Color.white);
		
		
		int x = -70, y = 30;
		for(int i = 0; i < btnNum.length; i++){
			for(int j = 0; j < btnNum[i].length; j++){
				if(i > 0){
					btnNum[i][j] = new JButton(num[i*btnNum[i-1].length+j]);
				} else if(i == 0){
					btnNum[i][j] = new JButton(num[j]);
				}
			
				x += 80;
				btnNum[i][j].setBounds(x, y, 70, 80);	
				btnNum[i][j].setBackground(new Color(216,175,70));
				btnNum[i][j].setFont(c);
				btnNum[i][j].setBorderPainted(false);
				btnNum[i][j].addActionListener(new ActionListener(){

					
					@Override
					public void actionPerformed(ActionEvent e) {
						String in = e.getActionCommand();
						if(!in.equals("C") && !in.equals("입력")){
							if(cname.equals("tfId")){
								tfId.setText(tfId.getText()+in);
							} else if(cname.equals("tfPw")){
								tfPw.setText(tfPw.getText()+in);
							}
						}  else if(in.equals("C")){
							if(cname.equals("tfId")){
								tfId.setText("cafe");
							} else if(cname.equals("tfPw")){
								tfPw.setText("");
							}
						}
					}
					
				});
			
				jpCal.add(btnNum[i][j]);
				if(i == 3 && j == 2){
					btnNum[i][j].setVisible(false);
				}
				
			}
			x = -70;
			y += 90;
		}
		
		// 리스너 추가
		tfId.addMouseListener(this);
		tfPw.addMouseListener(this);
		btnLogin.addActionListener(this);
		
		// 컴포넌트 추가
		jp.setLayout(null);
		
		jp.add(lbId);
		jp.add(lbPw);
		jp.add(tfId);
		jp.add(tfPw);
		jp.add(btnLogin);
		
		jp.add(jpCal);
		add(jp);
		
		
		jp.setBounds(0, 0, 1100, 900);
		jp.setBackground(new Color(228, 202, 165));
		setBounds(0, 0, 1100, 900);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if(obj == btnLogin){
			CafeDAO cdao = new CafeDAO();
			if(cdao.cafeLogin(tfId.getText(), tfPw.getText())){
				COSTOMER_HP = tfId.getText();
				setVisible(false);
				new MainFrame();
			} else {
				int result = JOptionPane.showConfirmDialog(null, "ID와 PASSWORD를 다시 확인해주세요", "로그인실패", JOptionPane.YES_NO_CANCEL_OPTION);
				if(result == JOptionPane.YES_OPTION){
					tfId.setText("cafe");
					tfPw.setText("");
					tfId.setRequestFocusEnabled(true);
				}
			}
		}
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		clickCom = e.getComponent();	// 클릭한 컴포넌트의 정보 얻어오기
		if(clickCom instanceof JTextField){
			if(clickCom == tfId){
				cname = "tfId";
			} else if(clickCom == tfPw){
				cname = "tfPw";
				System.out.println("cname : " + cname  + " = " + tfPw.getText());
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public static void main(String[] args) {
		new Login();
		new TServer();
	}
}
