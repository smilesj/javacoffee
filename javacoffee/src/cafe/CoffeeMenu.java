package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import database.dao.DrinkDAO;
import database.vo.DrinkVO;

public class CoffeeMenu implements ActionListener {
	JPanel jp;
	JButton[] btnCoffMenu = new JButton[12];
	int[] price = new int[12]; // menu price
	Order order; 
	Popup popup;
	
	public CoffeeMenu(Order order) {
		// initialize
		this.jp = order.jpMenu;
		jp.removeAll();
		this.order = order;
		
		// ��Ʈ
		Font f = new Font("���� ����", Font.BOLD, 15);
		
		

		// menu naming
		for(int i =0; i<12; i++){
			btnCoffMenu[i] = new JButton();
			btnCoffMenu[i].setBackground(new Color(241,195,90));
			btnCoffMenu[i].setBorderPainted(false);
			btnCoffMenu[i].setFont(f);
			btnCoffMenu[i].setForeground(Color.darkGray);

		}

		DrinkDAO d= new DrinkDAO();
		ArrayList<DrinkVO> list = d.selectCoffee();
		ArrayList li = new ArrayList();
		for(DrinkVO vo : list){
			li.add(vo.getDname());
		}
		
		for(int i=0; i<li.size(); i++){
				if(i%3==0)	btnCoffMenu[i/3].setText((String)li.get(i));
		}

		// set price
		for(int i =0; i<12; i++){
			price[i] = 0;
		}
		ArrayList li1 = new ArrayList();
		for(DrinkVO vo : list){
			li1.add(vo.getPrice());
		}
		for(int i=0; i<li1.size(); i++){
				if(i%3==0)	price[i/3] = (int) li1.get(i);
		}
		


		// button layout
		jp.setLayout(null);
		for (int i = 0; i < 12; i++) {
			btnCoffMenu[i].setBounds(170 * (i % 4) + 30, 170 * (i / 4) + 50, 140, 120);
			jp.add(btnCoffMenu[i]);
		}
		jp.repaint();

		// call action listener
		for (int i = 0; i < btnCoffMenu.length; i++) {
			btnCoffMenu[i].addActionListener(this);
		}
		
	
	}

	// action performed
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		for (int i = 0; i < btnCoffMenu.length; i++) {
			if (obj == btnCoffMenu[i]&&price[i]!=0) {
				
				new Popup(btnCoffMenu[i].getLabel(), price[i], order);
				/*if(i!= 3) new Popup(btnCoffMenu[i].getLabel(), price[i], order);
				else {
					Object[] menu = { btnCoffMenu[i].getText(), 1, price[i]};
					order.model.addRow(menu);
					order.cal();
				}*/
			}
		}
	}

	
	
	
	
}
