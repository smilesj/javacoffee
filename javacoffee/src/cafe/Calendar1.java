package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calendar1 extends JPanel implements ActionListener {

	String[] days = { "일", "월", "화", "수", "목", "금", "토" };
	int year = 0;
	int month = 0;
	int day = 0;
	int todays = 0;
	int memoday = 0;
	Calendar today;
	Calendar cal;
	JButton btnbe, btnaf;
	JButton[] btncal = new JButton[49];
	JLabel thing;
	JLabel time;
	JPanel jp1;
	JPanel jpday;
	JPanel jpmonth;
	JTextField tfMonth, tfYear;

	JPanel jp;
	int a;

	public Calendar1(JPanel jp, int a) {
		this.jp = jp;
		this.a = a;
		jp1 = new JPanel();
		today = Calendar.getInstance();
		cal = new GregorianCalendar();
		year = today.get(Calendar.YEAR);
		month = today.get(Calendar.MONTH) + 1;

		jpmonth = new JPanel();

		btnbe = new JButton("Before");
		btnaf = new JButton("After");
		tfYear = new JTextField(" " + year + " 년", JLabel.CENTER);
		tfMonth = new JTextField(" " + month + " 월",3);

		tfYear.setEnabled(false);
		tfMonth.setEnabled(false);
	
		jpday = new JPanel();

		setLayout(null);
		jp1.setLayout(null);
		jpmonth.setLayout(null);

		Font f = new Font("맑은 고딕", Font.BOLD, 20);
		Font m = new Font("맑은 고딕", Font.BOLD, 10);

		tfYear.setFont(f);
		tfMonth.setFont(f);

		btnbe.setBounds(10, 10, 100, 80);
		btnbe.setBackground(new Color(100,110,81));
		btnbe.setForeground(Color.WHITE);
		btnbe.setFont(f);
		btnbe.setBorderPainted(true);
		
		btnaf.setBounds(280, 10, 100, 80);
		btnaf.setBackground(new Color(100,110,81));
		btnaf.setForeground(Color.WHITE);
		btnaf.setFont(f);
		btnaf.setBorderPainted(true);
		
		tfYear.setBounds(110, 10, 85, 80);
		tfMonth.setBounds(195, 10, 85, 80);
		
		jpmonth.add(btnbe);
		jpmonth.add(btnaf);
		jpmonth.add(tfYear);
		jpmonth.add(tfMonth);
		
		jpmonth.setBounds(10, 10, 380, 100);
		jpmonth.setBackground(new Color(215,188,171));

		jp1.add(jpmonth);
		

		jpday = new JPanel(new GridLayout(7, 7));
		jpday.setBackground(new Color(215,188,171));		
		jpday.setLayout(null);
		jp1.setBackground(new Color(215,188,171));

		this.jpday.removeAll();
		this.tfYear.setText(" " + year + " 년");
		this.tfMonth.setText(" " + month + " 월");
		calInput(0);
		gridInit();
		panelInit();
		calSet();
		hideInit();
		this.jpday.repaint();

		jp1.add(jpday);
		jpday.setBounds(10, 120, 380, 450);
/*		
		gridInit();
		calSet();
		hideInit();
*/		
		jp1.setBounds(0, 0, 400, 620);
		add(jp1);

		// jp2.add(jpmonth);
		add(jp1);
		
		setBounds(0, 0, 400, 600);
		setVisible(true);

		btnbe.addActionListener(this);
		btnaf.addActionListener(this);

	}

	public void calSet() {

		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, (month - 1));
		cal.set(Calendar.DATE, 1);

		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

		int j = 0;
		int hopping = 0;

		btncal[0].setForeground(new Color(255, 0, 0)); // 일요일
		btncal[6].setForeground(new Color(0, 0, 255)); // 토요일
		for (int i = cal.getMinimalDaysInFirstWeek(); i < dayOfWeek; i++) {
			j++;
		}
		// 일요일부터 첫시작 요일까지 빈칸으로
		// 안하면 무조건 처음부터 채워짐
		hopping = j;
		for (int k = 0; k < hopping; k++) {
			btncal[k + 7].setText("");
		}

		for (int i = cal.getMinimum(Calendar.DAY_OF_MONTH); i <= cal.getMaximum(Calendar.DAY_OF_MONTH); i++) {
			cal.set(Calendar.DATE, i);
			if (cal.get(Calendar.MONTH) != month - 1) {
				break;
			}

			todays = i;
			if (memoday == 1) {
				btncal[i + 6 + hopping].setForeground(new Color(0, 255, 0));
			} else {
				btncal[i + 6 + hopping].setForeground(new Color(0, 0, 0));
				btncal[i + 6 + hopping].setBackground(new Color(216,175,70));
				// 버튼 색 추가
				
				if ((i + hopping - 1) % 7 == 0) {// 일요일
					btncal[i + 6 + hopping].setForeground(new Color(255, 0, 0));
				}
				if ((i + hopping) % 7 == 0) {// 토요일
					btncal[i + 6 + hopping].setForeground(new Color(0, 0, 255));
				}
			}
			btncal[i + 6 + hopping].setText((i) + "");
		} // for

	}// calset()

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnbe) {
			this.jpday.removeAll();
			calInput(-1);
			gridInit();
			panelInit();
			calSet();
			hideInit();
			this.tfYear.setText(" " + year + " 년");
			this.tfMonth.setText(" " + month + " 월");

		} else if (obj == btnaf) {
			this.jpday.removeAll();
			this.repaint();
			calInput(1);
			gridInit();
			panelInit();
			calSet();
			hideInit();
			this.tfYear.setText(" " + year + " 년");
			this.tfMonth.setText(" " + month + " 월");
		}

		// 콘솔에 선택날짜 출력
		else if (Integer.parseInt(e.getActionCommand()) >= 1 && Integer.parseInt(e.getActionCommand()) <= 31) {
			day = Integer.parseInt(e.getActionCommand());
			if(a == 1){
				Sales.selectOK1 = true;
				Sales.day1 += Integer.toString(year);
				if(month<10){
					Sales.day1 += 0;
					Sales.day1 += Integer.toString(month);
				}else Sales.day1 += Integer.toString(month);
				if(day<10){
					Sales.day1 += 0;
					Sales.day1 += Integer.toString(day);
				}else Sales.day1 += Integer.toString(day);
			} else if(a == 2){
				Sales.selectOK2 = true;
				Sales.day2 += Integer.toString(year);
				if(month<10){
					Sales.day2 += 0;
					Sales.day2 += Integer.toString(month);
				}else Sales.day2 += Integer.toString(month);
				if(day<10){
					Sales.day2 += 0;
					Sales.day2 += Integer.toString(day);
				}else Sales.day2 += Integer.toString(day);
			}
			
			if(Sales.selectOK1 && Sales.selectOK2){
				new SalesCondition(this.jp, Sales.day1, Sales.day2);
				Sales.selectOK1 = false;
				Sales.selectOK2 = false;
			}
//			System.out.println(year + "-" + month + "-" + day);
			/////// 이것을 나중에 디비로 찾을것임
			calSet();

		}

	} // ends Actionperformed()

	public void hideInit() {
		for (int i = 0; i < btncal.length; i++) {
			if ((btncal[i].getText()).equals(""))
				btncal[i].setEnabled(false);
			// 일이 찍히지 않은 나머지 버튼을 비활성화 시킨다.

		} // for
	}// hideInit()

	public void gridInit() {
		for (int i = 0; i < days.length; i++) {
			jpday.add(btncal[i] = new JButton(days[i]));
			btncal[i].setContentAreaFilled(false);
			btncal[i].setBorderPainted(false);
		}
		for (int i = days.length; i < 49; i++) {
			jpday.add(btncal[i] = new JButton(""));
			btncal[i].addActionListener(this);

		}

	} // gridInit()

	public void panelInit() {
		GridLayout gridLayout1 = new GridLayout(7, 7);
		jpday.setLayout(gridLayout1);
	}

	public void calInput(int gap) { // 달 이동한뒤정렬
		month += (gap);
		if (month <= 0) {
			month = 12;
			year = year - 1;

		} else if (month >= 13) {
			month = 1;
			year = year + 1;

		}
	}// calInput
}
