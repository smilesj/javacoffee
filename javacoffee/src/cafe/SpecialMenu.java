package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import database.dao.DrinkDAO;
import database.vo.DrinkVO;

public class SpecialMenu implements ActionListener {
	JPanel jp;
	JButton[] btnSpecialMenu = new JButton[12];
	int[] price = new int[12]; // menu price
	Order order;

	public SpecialMenu(Order order) {
		// initialize
		this.jp = order.jpMenu;
		jp.removeAll();
		this.order = order;

		// ��Ʈ
		Font f = new Font("���� ����", Font.BOLD, 15);

		// menu naming
		for (int i = 0; i < 12; i++) {
			btnSpecialMenu[i] = new JButton();
			btnSpecialMenu[i] = new JButton();
			btnSpecialMenu[i] = new JButton();
			btnSpecialMenu[i].setBackground(new Color(241,195,90));
			btnSpecialMenu[i].setBorderPainted(false);
			btnSpecialMenu[i].setFont(f);
			btnSpecialMenu[i].setForeground(Color.darkGray);
		}

		DrinkDAO d = new DrinkDAO();
		ArrayList<DrinkVO> list = d.selectSpecial();
		ArrayList li = new ArrayList();
		for (DrinkVO vo : list) {
			li.add(vo.getDname());
		}
		for (int i = 0; i < li.size(); i++) {
			if (i % 3 == 0)
				btnSpecialMenu[i / 3].setText((String) li.get(i));
		}

		// set price
		for (int i = 0; i < 12; i++) {
			price[i] = 0;
		}
		ArrayList li1 = new ArrayList();
		for (DrinkVO vo : list) {
			li1.add(vo.getPrice());
		}
		for (int i = 0; i < li1.size(); i++) {
			if (i % 3 == 0)
				price[i / 3] = (int) li1.get(i);
		}

		// button layout
		jp.setLayout(null);
		for (int i = 0; i < 12; i++) {
			btnSpecialMenu[i].setBounds(170 * (i % 4) + 30, 170 * (i / 4) + 50, 140, 120);
			jp.add(btnSpecialMenu[i]);
		}
		jp.repaint();

		// call action listener
		for (int i = 0; i < btnSpecialMenu.length; i++) {
			btnSpecialMenu[i].addActionListener(this);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		for (int i = 0; i < btnSpecialMenu.length; i++) {
			if (obj == btnSpecialMenu[i] && price[i] != 0) {
				Popup2 popup = new Popup2(btnSpecialMenu[i].getLabel(), price[i], order);
			}
		}

	}

}
