package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.dao.OrderDetailDAO;
import database.dao.OrdersDAO;
import database.vo.OrdersVO;

public class SalesCondition implements ActionListener {
	JPanel jp;
	JScrollPane jsp;
	JTable jtb;
	DefaultTableModel model;
	JButton btnHistory, btnCoupon, btnRefund;
	String day1 = "";
	String day2 = "";

	SalesCondition(JPanel jp, String day1, String day2) {
		// initialize
		this.jp = jp;
		this.day1 = day1;
		this.day2 = day2;
		
	/*	if(day1.length()>8){
			day1.substring(day1.length()-8);
		}
		if(day2.length()>8){
			day2.substring(day2.length()-8);
		}*/
		
		jp.removeAll();
		btnHistory = new JButton("히스토리");
		btnCoupon = new JButton("쿠 폰");
		btnRefund = new JButton("환 불");

		// scrollpane & table
		model = new DefaultTableModel();
		String[] colName = { "시간", "OrderID", "ID", "결제방법", "합계" };
		model.setColumnIdentifiers(colName);
		jtb = new JTable(model);
		jtb.setRowHeight(30);
		jsp = new JScrollPane(jtb, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		// layout
		jp.setLayout(null);
		btnHistory.setBounds(50, 10, 150, 60);
		btnCoupon.setBounds(230, 10, 150, 60);
		btnRefund.setBounds(860, 10, 150, 60);
		jsp.setBounds(50, 100, 960, 600);

		// add
		jp.add(btnHistory);
		jp.add(btnCoupon);
		jp.add(btnRefund);
		jp.add(jsp);

		// panel layout
		jp.setBounds(20, 120, 1060, 750);

		// call action listener
		btnHistory.addActionListener(this);
		btnCoupon.addActionListener(this);
		btnRefund.addActionListener(this);

		jp.repaint();

		// basic window
		OrdersDAO dao = new OrdersDAO();
		ArrayList<OrdersVO> list = dao.selectList(day1, day2);
//		OrderDetailDAO dao1 = new OrderDetailDAO();
		
		
//		ArrayList li = new ArrayList();
		for (OrdersVO vo : list) {
			/*li.add(vo.getOrderTime());
			li.add(vo.getHp());
			li.add(vo.getPayMethod());
			li.add(vo.getUsePoint());
			li.add(vo.getTotalPrice());*/
			
			Object[] menu = {vo.getOrderTime(),vo.getOrderid(), vo.getHp(),vo.getPayMethod(),vo.getTotalPrice()};
			model.addRow(menu);
			
		}
		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnHistory) {
			new Sales(jp);

		} else if (obj == btnCoupon) {

		} else if (obj == btnRefund) {
			new Refund((String)jtb.getValueAt(jtb.getSelectedRow(), 1));
			new SalesCondition(jp,day1,day2);
		}

	}
}
