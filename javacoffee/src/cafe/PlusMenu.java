package cafe;

public class PlusMenu {
	Order order;

	PlusMenu(Order order) {
		this.order = order;
		if (order.jtb.getSelectedRow() != -1) {
			int num = (int) order.jtb.getValueAt(order.jtb.getSelectedRow(), 1);
			int price = (int) order.jtb.getValueAt(order.jtb.getSelectedRow(), 2);
			price /= (num);
			if (num > 0) {
				num++;
				order.jtb.setValueAt(num, order.jtb.getSelectedRow(), 1);
				order.jtb.setValueAt(num*price, order.jtb.getSelectedRow(), 2);
				order.cal();
			}
		}
	}
}
