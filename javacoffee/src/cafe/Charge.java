package cafe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import database.dao.CustomerDAO;

public class Charge extends JPanel implements ActionListener, MouseListener{

	JPanel jpCg;
	JButton btnCg;
	JButton btnsc;
	
	JLabel lbhp, lbpay, lbsc;
	
	JLabel lbrepay;///////////////
	
	private int currentBtnIdx = 0;
	//숫자창
	JPanel jp2;
	JTextField jtf[] = new JTextField[2];
	JButton[][] jbtn;
	JButton jbtnend;
	int x1, x2;
	int y1, y2;
	int Number1, Number2, Number3, Number4, Number5, Number6;
	String oper = "";
	Component c;
	CustomerDAO csdao = new CustomerDAO();	// 다오선언
	JPanel jp;
	
	public Charge(JPanel jp){
		this.jp = jp;
		jp.removeAll();
		
		//폰트
		Font f = new Font("맑은 고딕", Font.BOLD, 20);
		Font s = new Font("맑은 고딕", Font.PLAIN, 25);
		Font b = new Font("맑은 고딕", Font.BOLD, 25);
		jpCg = new JPanel();
		btnCg = new JButton("충전하기");
		
		lbhp = new JLabel("CP",JLabel.CENTER);
		lbhp.setFont(f);
		lbpay = new JLabel("금액",JLabel.CENTER);
		lbpay.setFont(f);
		lbsc = new JLabel("번호를 입력하세요.",JLabel.LEFT);
		lbsc.setFont(b);
		
		jtf[0] = new JTextField();
		jtf[1] = new JTextField();
		jtf[0].setFont(s);
		jtf[1].setFont(s);
		
		lbrepay = new JLabel();
		btnsc = new JButton("검색");
		
		jp.setLayout(null);
		jpCg.setLayout(null);
		jpCg.setBounds(0, 0, 1060, 750);
		jpCg.setBackground(Color.LIGHT_GRAY);
		lbsc.setBounds(260, 40, 300, 70);
		lbhp.setBounds(150, 140, 100, 100);
		jtf[0].setBounds(250, 150, 300, 80);
		
		btnsc.setBounds(600, 150, 100, 80);
		btnsc.setBackground(new Color(100,110,81));
		btnsc.setBorderPainted(false);
		btnsc.setForeground(Color.white);
		btnsc.setFont(f);
		
		lbrepay.setBounds(250, 250, 300, 70); ////////////////
		lbpay.setBounds(150, 340, 100, 100);
		jtf[1].setBounds(250, 350, 300, 80);
		
		btnCg.setBounds(250, 550, 300, 80);
		btnCg.setBackground(new Color(173,93,94));
		btnCg.setBorderPainted(false);
		btnCg.setForeground(Color.WHITE);
		btnCg.setFont(f);
		
		jpCg.add(btnCg);
		jpCg.add(lbhp);
		jpCg.add(lbsc);
		jpCg.add(lbpay);
		jpCg.add(jtf[0]);
		jpCg.add(lbrepay); /////////////
		jpCg.add(btnsc);
		jpCg.add(jtf[1]);
		


		String[] btn = { "7", "8", "9", "4", "5", "6", "1", "2", "3", "0", "c", "입력" };

		jbtn = new JButton[4][3];
		addMouseListener(this);
	//	setLayout(null);
		jp2 = new JPanel();
		jp2.setBackground(new Color(104, 180, 255));


		int x = 20, y = 20;
		for (int i = 0; i < jbtn.length; i++) {
			x = 20;
			for (int j = 0; j < jbtn[i].length; j++) {
				if (i != 0) {
					jbtn[i][j] = new JButton(btn[i * jbtn[i - 1].length + j]);
				} else {
					jbtn[i][j] = new JButton(btn[j]);
				}
				jbtn[i][j].setLocation(x, y);
				x += 80;
				jbtn[i][j].setSize(60, 60);
				jbtn[i][j].setBackground(new Color(216,175,70));
				jbtn[i][j].setBorderPainted(false);
				jp2.add(jbtn[i][j]);
				jbtn[i][j].addActionListener(this);

			}
			y += 70;
		}

		jp2.setLayout(null);
	//	setDefaultCloseOperation(EXIT_ON_CLOSE);
		jp2.setBounds(700, 370, 260, 310);
		
		jp.add(jp2);
		jp.add(jpCg);
		jpCg.setBackground(new Color(228, 202, 165));
		jp2.setBackground(new Color(215,188,171));
		jp.setBackground(new Color(228, 202, 165));
		
		jp.repaint();
		
		
	/*	setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(20, 20, 1100, 900);
		setVisible(true);
	*/	btnCg.addActionListener(this);
		jtf[0].addMouseListener(this);
		jtf[1].addMouseListener(this);
		btnsc.addActionListener(this);
		
	}


	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		c = e.getComponent();
		if (c instanceof JTextField) {
			if (c == jtf[0]) {
				currentBtnIdx = 0;
			} else if (c == jtf[1]) {
				currentBtnIdx = 1;
			} 

		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		String obj2 = e.getActionCommand();

		////////////////////////////////////////////

	
		
		if (c == jtf[0]&&obj!=btnCg&&obj!=jbtn[3][1]&&obj!=jbtn[3][2]&&obj!=btnsc) {
			String data = jtf[0].getText();
			jtf[0].setText(data + obj2);
			
			
		}if (c == jtf[1]&&obj!=btnCg&&obj!=jbtn[3][1]&&obj!=jbtn[3][2]&&obj!=btnsc) {
			String data = jtf[1].getText();
			jtf[1].setText(data + obj2);
	
			
		}
		if(c == jtf[0]&&obj== jbtn[3][1]){	// c버튼 누르면 텍스트필드 초기화
			jtf[0].setText("");
		}
		if(c == jtf[1]&&obj== jbtn[3][1]){
			jtf[1].setText("");
		}
		
		
		// 커스토머브이오 고객 목록에 있는지 확인
		database.vo.CustomerVO csvo = csdao.selectCustomerOne(jtf[0].getText());
		if(obj == btnsc|| obj==jbtn[3][2]){
		if(csvo!=null){
				lbsc.setText("충전금액을 입력하세요.");
				lbrepay.setText(""+csvo.getBalance());

			}else{
				lbsc.setText("존재하지않는 고객입니다.");
				jtf[0].setText("");
				lbrepay.setText("");
			
			}
		}
		
		if(obj==btnCg){	// 충전하기 누르면 팝업
			csvo.setBalance(Integer.parseInt(jtf[1].getText())+csvo.getBalance());
            csdao.UpdateCustomerBalance(csvo);
              
              
            ChargePop cp = new ChargePop();
          
            int blc = csvo.getBalance(); 

			
		}
		
		}
		
		
		
		
		
	}


