package cafe;

import database.dao.OrderDetailDAO;
import database.dao.OrdersDAO;

public class Refund {

	Refund(String orderid){
		OrderDetailDAO d1 = new OrderDetailDAO();
		OrdersDAO d2 = new OrdersDAO();
		d1.deleteOrder(orderid);
		d2.deleteOrder(orderid);
	}
}
