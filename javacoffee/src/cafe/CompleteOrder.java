package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import database.dao.DrinkDAO;
import database.dao.OrderDetailDAO;
import database.dao.OrdersDAO;
import database.vo.OrderDetailVO;
import database.vo.OrdersVO;

public class CompleteOrder extends JFrame implements ActionListener {
	Order order;
	JLabel lb;
	JButton btnYes, btnNo;
	String orderId = "";
	String cpId = "CP001";
	//String hp = "01011112222";
	String payMethod = "";
	int totalPrice = 0;
	int usePoint = 0;
	JPanel jp, jp1;
	JTable jtb;

	String cafeId = "cafe0001";

	CompleteOrder(Order order, String payMethod) {
		// initialize
		this.order = order;
		this.payMethod = payMethod;
		this.jp = order.jp;
		this.jtb = order.jtb;
		
		Font f = new Font("���� ����", Font.BOLD, 20);
		Font d = new Font("���� ����", Font.PLAIN, 25);
		
		jp1 = new JPanel();
		
		lb = new JLabel("���� �Ϸ� �Ͻðڽ��ϱ�?");
		lb.setFont(d);
		
		btnYes = new JButton("��");
		btnYes.setBackground(new Color(173,93,94));
		btnYes.setBorderPainted(false);
		btnYes.setFont(f);
		btnYes.setForeground(Color.white);
		
		btnNo = new JButton("�ƴϿ�");
		btnNo.setBackground(new Color(100,110,81));
		btnNo.setBorderPainted(false);
		btnNo.setFont(f);
		btnNo.setForeground(Color.white);

		// layout
		lb.setBounds(60, 60, 300, 50);
		btnYes.setBounds(80, 180, 100, 50);
		btnNo.setBounds(200, 180, 100, 50);

		// action
		btnYes.addActionListener(this);
		btnNo.addActionListener(this);

		// add
		jp1.add(lb);
		jp1.add(btnYes);
		jp1.add(btnNo);
		add(jp1);

		// frame layout
		setLayout(null);
		jp1.setLayout(null);
		jp1.setBounds(0, 0, 400, 300);
		jp1.setBackground(new Color(228, 202, 165));
		setBounds(300, 300, 400, 300);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		if (obj == btnYes) {
			setVisible(false);
			// call orders db
			OrdersDAO dao = new OrdersDAO();
			OrdersVO vo = new OrdersVO();

			
			
			
			System.out.println("HP : " + Login.COSTOMER_HP);
			
			totalPrice = Integer.parseInt(order.tf.getText());

			vo.setOrderid("A");
			vo.setCpId(cpId);
			vo.setCafeid(cafeId);
			vo.setHp(Login.COSTOMER_HP);
			vo.setPayMethod(payMethod);
			vo.setTotalPrice(totalPrice);
			vo.setUsePoint(usePoint);
			
			boolean a = dao.insertOrder(vo);
			System.out.println("Order Insert : " + a);

			// call ordersdetail db
			OrderDetailDAO dao1 = new OrderDetailDAO();
			OrderDetailVO vo1 = new OrderDetailVO();
			DrinkDAO dao2 = new DrinkDAO();
			
			System.out.println(orderId);
			System.out.println(cpId);
			System.out.println(cafeId);
			System.out.println(payMethod);

			int count = jtb.getRowCount();
			int shot = 0, whipping = 0, syrup = 0;

			for (int i = count - 1; i >= 0; i--) {
				String name = (String) jtb.getValueAt(i, 0);

				if (name == "SHOT") {
					int p = (int) jtb.getValueAt(i, 1);
					shot += p;
				} else if (name == "WHIPPING") {
					int p = (int) jtb.getValueAt(i, 1);
					whipping += p;
				} else if (name == "SYRUP") {
					int p = (int) jtb.getValueAt(i, 1);
					syrup += p;
				} else if (name != "SHOT" && name != "WHIPPING" && name != "SYRUP") {
					String odId = "order" + (int) (Math.random() * 100);

					String dname = name.substring(0, name.indexOf(' '));
					String name1 = name.substring(name.indexOf(' ') + 1);
					String icehot="COLD";
					String dsize =null;
					if(name1.length()>7){
						icehot = name1.substring(0, name1.indexOf(' '));
						dsize = name1.substring(name1.indexOf(' ') + 1);
					}else dsize = name1;
					
					int quant = (int) jtb.getValueAt(i, 1);

					String drinkId = null;
					drinkId = dao2.selectDrinkID(dname, dsize.charAt(0));

					
					System.out.println("orderId : " + orderId);
					System.out.println("drinkname : " + dname);
					System.out.println("icehot : " + icehot.charAt(0));
					System.out.println("dsize : " + dsize.charAt(0));
					System.out.println("drinkId : " + drinkId);
					System.out.println("Quantity : " + quant);
					System.out.println("shot : " + shot);
					System.out.println("whipping : " + whipping);
					System.out.println("syrup : " + syrup);
					System.out.println();

					orderId = dao.selectOrderID("cafe0001", Login.COSTOMER_HP);//"order" + (int) (Math.random() * 100);
					
					vo1.setOdId(odId);
					vo1.setOrderId(orderId);
					vo1.setDrinkId(drinkId);
					vo1.setQuantity((int) jtb.getValueAt(i, 1));
					vo1.setDsize(dsize.charAt(0));
					vo1.setIcehot(icehot.charAt(0));
					vo1.setShot(shot);
					vo1.setWhipping(whipping);
					vo1.setSyrup(syrup);

					dao1.insertODOne(vo1);

					shot = 0;
					whipping = 0;
					syrup = 0;
				}
			}
			Login.COSTOMER_HP = "01000000000";
			//
			new Order(this.jp);
		} else if (obj == btnNo) {
			setVisible(false);
			new Order(this.jp);
		}

	}
}
