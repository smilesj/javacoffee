package cafe;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame implements ActionListener {
	JButton btnHome;
	ImageIcon imgBack, imgHome, imgBackt, imgHomet;
	Image imgTemp1, imgTemp2;
	JPanel jp;
	JPanel jp1;

	MainFrame() {
		// initialize
		JPanel jp = new JPanel();
		jp.setLayout(null);
		jp.setBounds(20, 120, 1060, 750);

		jp1 = new JPanel();
		jp1.setLayout(null);
		jp1.setBackground(new Color(228, 202, 165));

		// home
		Home ho = new Home(jp);
		jp = ho.jp;
		
        imgHomet = new ImageIcon("src/img/home.png");

		// image size 변경 알고리즘
	     // imageback icon size 변경 및 버튼 입력
 
        // imagehome icon size 변경 및 버튼 입력
        imgTemp1 = imgHomet.getImage();
        imgTemp2 = imgTemp1.getScaledInstance(80, 80, java.awt.Image.SCALE_SMOOTH);
        imgHome = new ImageIcon(imgTemp2);
        btnHome = new JButton(imgHome);
        btnHome.setBorderPainted(false);
        btnHome.setContentAreaFilled(false);


		// button layout
		btnHome.setBounds(980, 40, 60, 60);

		// add
		jp1.add(btnHome);
		jp1.add(jp);
		add(jp1);
		jp1.setBounds(00, 0, 1100, 900);

		// layout
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(20, 20, 1105, 900);
		//setResizable(false);
		setVisible(true);

		// call action listener
		btnHome.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnHome) {
			setVisible(false);
			new MainFrame();
		}

	}

}
