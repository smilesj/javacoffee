package cafe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ServiceMenu implements ActionListener {
	JPanel jp;
	JButton[] btnServiceMenu = new JButton[12];
	int[] price = new int[12]; // menu price
	Order order; 
	
	public ServiceMenu(Order order) {
		// initialize
		this.jp = order.jpMenu;
		jp.removeAll();
		this.order = order;

		// naming
		for(int i =0; i<12; i++){
			btnServiceMenu[i] = new JButton();
		}

		btnServiceMenu[0].setText("SHOT");
		btnServiceMenu[1].setText("WHIPPING");
		btnServiceMenu[2].setText("SYRUP");
		
		// set price
		for(int i =0; i<12; i++){
			price[i] = 0;
		}

		price[0] = 1000;
		price[1] = 1000;
		price[2] = 1000;
		
		
		// button layout
		jp.setLayout(null);
		for (int i = 0; i < 12; i++) {
			btnServiceMenu[i].setBounds(170 * (i % 4) + 30, 170 * (i / 4) + 50, 140, 120);
			btnServiceMenu[i].setBackground(new Color(241,195,90));
			btnServiceMenu[i].setBorderPainted(false);
			btnServiceMenu[i].setForeground(Color.darkGray);
			jp.add(btnServiceMenu[i]);
		}
		jp.repaint();

		for (int i = 0; i < btnServiceMenu.length; i++) {
			btnServiceMenu[i].addActionListener(this);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		for (int i = 0; i < btnServiceMenu.length; i++) {
			if (obj == btnServiceMenu[i]&&price[i]!=0) {
				Object[] menu = { btnServiceMenu[i].getText(), 1, price[i]};
				order.model.addRow(menu);
				order.cal();
			}
		}

	}

}
