package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Popup extends JFrame implements ActionListener {
	JButton btn1, btn2, btn3, btn4, btn5, btn6;
	private int num = 1, price = 0;
	private String name = null;
	JPanel jp;
	Order order;
	int totalPrice = 0;

	public Popup(String title, int price, Order order) {
		// initialize
		this.price = price;
		this.name = title;
		this.order = order;
		
		jp = new JPanel();
		
		Font f = new Font("���� ����", Font.BOLD, 15);

		setTitle(title);
		btn1 = new JButton("hot tall");
		btn2 = new JButton("hot grande");
		btn3 = new JButton("hot venti");
		btn4 = new JButton("cold tall");
		btn5 = new JButton("cold grande");
		btn6 = new JButton("cold venti");

		// button layout
		btn1.setBounds(30, 30, 120, 120);
		btn2.setBounds(170, 30, 120, 120);
		btn3.setBounds(310, 30, 120, 120);
		btn4.setBounds(30, 170, 120, 120);
		btn5.setBounds(170, 170, 120, 120);
		btn6.setBounds(310, 170, 120, 120);
		
		btn1.setBackground(new Color(241,195,90));
		btn2.setBackground(new Color(241,195,90));
		btn3.setBackground(new Color(241,195,90));
		btn4.setBackground(new Color(241,195,90));
		btn5.setBackground(new Color(241,195,90));
		btn6.setBackground(new Color(241,195,90));
		
		btn1.setBorderPainted(false);
		btn2.setBorderPainted(false);
		btn3.setBorderPainted(false);
		btn4.setBorderPainted(false);
		btn5.setBorderPainted(false);
		btn6.setBorderPainted(false);
		
		btn1.setFont(f);
		btn2.setFont(f);
		btn3.setFont(f);
		btn4.setFont(f);
		btn5.setFont(f);
		btn6.setFont(f);
		
		btn1.setForeground(Color.darkGray);
		btn2.setForeground(Color.darkGray);
		btn3.setForeground(Color.darkGray);
		btn4.setForeground(Color.darkGray);
		btn5.setForeground(Color.darkGray);
		btn6.setForeground(Color.darkGray);

		// add
		jp.setLayout(null);
		
		jp.add(btn1);
		jp.add(btn2);
		jp.add(btn3);
		jp.add(btn4);
		jp.add(btn5);
		jp.add(btn6);
		
		jp.setBackground(new Color(228, 202, 165));
		jp.setBounds(0, 0, 600, 350);
		add(jp);

		// layout
		setLayout(null);
		setBounds(200, 200, 460, 350);
		setVisible(true);

		// call action listener
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		btn5.addActionListener(this);
		btn6.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn1) {
			price += 0;
			name += (" HOT TALL");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		} else if (obj == btn2) {
			price += 500;
			name += (" HOT GRANDE");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		} else if (obj == btn3) {
			price += 1000;
			name += (" HOT VENTI");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		} else if (obj == btn4) {
			price += 0;
			name += (" COLD TALL");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		}else if (obj == btn5) {
			price += 500;
			name += (" COLD GRANDE");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		}else if (obj == btn6) {
			price += 1000;
			name += (" COLD VENTI");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		}

	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
