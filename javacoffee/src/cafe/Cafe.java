package cafe;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import database.dao.CouponDAO;
import database.dao.CustomerDAO;
import database.dao.OwnCouponDAO;
import database.vo.CouponVO;
import database.vo.CustomerVO;
import database.vo.OwnCouponVO;

public class Cafe implements ActionListener, MouseListener {

	int sum; // 총 가격
	int cspay = 0; // 받은돈
	int cscg = 0; // 잔돈
	int pp; // 포인트패널에서 현재포인트
	int up = 0; // 포인트패널에서 사용할 포인트
	int pt = 0; // 적립예정 포인트
	int rep = 0; // 사용하고 남는 포인트
	int dsum = 0;
	Component c;
	int a;

	java.util.Date d = new java.util.Date();

	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);
	int month = cal.get(Calendar.MONTH) + 1;
	int day = cal.get(Calendar.DATE);

	ImageIcon cashpay, cashpayx, cardpay, cardpayx, coupon, couponx, pointx, point;
	Image img1, img2;

	////////////////////// 변수/////////////////////////////////

	JPanel jpPay; // 결제창 전체 패널

	// 정보

	// 결제 정보
	JPanel jpPrc; // 결제창 가격정보 패널
	JLabel lbPSum; // 가격합계 라벨
	JLabel lbDSum; // 가격합계 라벨
	JLabel lbCpSum; // 가격합계 라벨
	JLabel lbPtSum; // 가격합계 라벨
	JTextField tfPSum; // 가격합계 텍스트필드
	JTextField tfDSum; // 음료합계
	JTextField tfCpSum; // 쿠폰할인가격
	JTextField tfPtSum; // 포인트 할인

	// 고객정보
	JPanel jpInfo; // 고객정보
	JLabel lbName;
	JLabel lbCell;
	JLabel lbSavePt; // 적립예정 포인트
	JLabel lbRePt; // 현재포인트
	JTextField tfName; // 고객이름
	JTextField tfCell; // 핸드폰번호
	JTextField tfSavePt; // 적립예정 포인트
	JTextField tfRePt; // 현재포인트

	// 결제창들

	// 버튼
	JPanel jpPm;
	JButton btnCash;
	JButton btnCard;
	JButton btnCp; // 쿠폰 버튼
	JButton btnPt; // 포인트버튼

	// 카드레이아웃
	JPanel jpCardLayout;
	CardLayout cl;

	// 현금결제
	JPanel jpCash; // 현금결제 패널
	JLabel lbCsRcv; // 받은돈
	JLabel lbCsCng; // 잔돈
	JTextField tfCsRcv; // 받은돈
	JTextField tfCsCng; // 잔돈
	JButton btnCsPay; // 현금결제 결제

	// 카드결제
	JPanel jpCard; // 카드결제 패널
	JLabel lbCdNum; // 카드번호라벨
	JLabel lbCdSum; // 카드 결제금액
	JTextField tfCdNum; // 카드번호
	JTextField tfCdSum; // 카드 결제금액
	JButton btnCdPay; // 카드결제

	// 쿠폰
	JPanel jpCp; // 쿠폰 프레임
	JLabel lbCpCell; // 쿠폰조회 핸드폰번호
	JTextField tfCpCell; // 핸드폰번호 입력창
	JButton btnCpCS; // 핸드폰번호 검색버튼
	JButton btnCpCR; // 핸드폰번호 지우기
	JScrollPane jsCpList;
	JButton btnCpList; // 쿠폰선택

	JTable jtCpList; // 쿠폰테이블
	DefaultTableModel CpModel; // 쿠폰 테이블 모델

	// 포인트
	JPanel jpPt; // 포인트프레임
	JLabel lbPtCell; // 포인트검색 위한 핸드폰번호 입력
	JTextField tfPtCell; // 번호입력창
	JButton btnPtCell; // 검색버튼
	JLabel lbPtPs; // 사용가능 포인트
	JTextField tfPtPs;
	JLabel lbPtUse; // 사용포인트
	JTextField tfPtUse;
	JButton btnPtUse; // 사용값 입력
	JLabel lbPtRe; // 사용하고 남는 포인트
	JTextField tfPtRe;
	JButton btnPtIs; // 포인트사용 최종 버튼

	// 숫자창
	JPanel jp2;
	JTextField jtf[] = new JTextField[10];
	JButton[][] jbtn;
	JButton jbtnc, jbtninput;

	// order
	Order order;
	JScrollPane jsp;

	Cafe(Order order) {
		// super();

		// jp 상속
		this.jpPay = order.jp;
		jpPay.removeAll();
		this.order = order;
		this.jsp = order.jsp;

		jsp.setBounds(750, 20, 260, 350);
		jpPay.add(jsp);

		/////////////// 선언//////////////////////
		jpPay.setLayout(null);
		jpPay.setBackground(new Color(228, 202, 165));

		// 폰트
		Font f = new Font("맑은고딕", Font.BOLD, 20);
		Font s = new Font("맑은고딕", Font.BOLD, 15);
		Font p = new Font("맑은고딕", Font.BOLD, 30);
		Font in = new Font("맑은고딕", Font.PLAIN, 20);

		// 버튼
		jpPm = new JPanel();

		// 현금결제
		cashpayx = new ImageIcon("src/img/현금결제.png");
		img1 = cashpayx.getImage();
		img2 = img1.getScaledInstance(150, 80, java.awt.Image.SCALE_SMOOTH);
		cashpay = new ImageIcon(img2);
		btnCash = new JButton(cashpay);
		btnCash.setBorderPainted(false);
		btnCash.setContentAreaFilled(false);
		// btnOrder.setBackground(new Color(183, 157, 120));

		// 카드결제
		cardpayx = new ImageIcon("src/img/카드결제.png");
		img1 = cardpayx.getImage();
		img2 = img1.getScaledInstance(150, 80, java.awt.Image.SCALE_SMOOTH);
		cardpay = new ImageIcon(img2);
		btnCard = new JButton(cardpay);
		btnCard.setBorderPainted(false);
		btnCard.setContentAreaFilled(false);

		// 쿠폰
		couponx = new ImageIcon("src/img/쿠폰.png");
		img1 = couponx.getImage();
		img2 = img1.getScaledInstance(150, 80, java.awt.Image.SCALE_SMOOTH);
		coupon = new ImageIcon(img2);
		btnCp = new JButton(coupon);
		btnCp.setBorderPainted(false);
		btnCp.setContentAreaFilled(false);

		// 포인트
		pointx = new ImageIcon("src/img/포인트.png");
		img1 = pointx.getImage();
		img2 = img1.getScaledInstance(150, 80, java.awt.Image.SCALE_SMOOTH);
		point = new ImageIcon(img2);
		btnPt = new JButton(point);
		btnPt.setBorderPainted(false);
		btnPt.setContentAreaFilled(false);

		///// 정보////

		// 결제창 가격정보
		jpPrc = new JPanel(); // 결제창 가격정보 패널
		lbPSum = new JLabel("합계 ");
		lbDSum = new JLabel("음료 ");
		lbCpSum = new JLabel("쿠폰 ");
		lbPtSum = new JLabel("포인트  ");
		tfPSum = new JTextField(); // 가격합계 텍스트필드
		tfPSum.setFont(p);
		tfDSum = new JTextField(); // 음료합계
		tfDSum.setFont(in);
		tfCpSum = new JTextField(); // 쿠폰할인가격
		tfCpSum.setFont(in);
		tfPtSum = new JTextField(); // 포인트 할인
		tfPtSum.setFont(in);

		// 음료 합계 order 에서 불러오기
		tfDSum.setText(order.tf.getText());
		tfPSum.setText(order.tf.getText());

		// 고객정보
		jpInfo = new JPanel(); // 고객정보
		lbName = new JLabel("이름");
		lbCell = new JLabel("C.P");
		lbSavePt = new JLabel("적립P");
		lbRePt = new JLabel("잔여P");

		tfName = new JTextField();
		tfCell = new JTextField(); // 핸드폰번호
		tfSavePt = new JTextField(); // 적립예정 포인트
		tfRePt = new JTextField(); // 잔여포인트

		tfName.setFont(in);
		tfCell.setFont(in);
		tfSavePt.setFont(in);
		tfRePt.setFont(in);

		//// 결체창////

		// 카드레이아웃
		cl = new CardLayout();
		jpCardLayout = new JPanel();
		jpCardLayout.setLayout(cl);
		jpCardLayout.setBackground(Color.cyan);
		jpCardLayout.setBounds(200, 400, 490, 320);
		jpPay.add(jpCardLayout);

		// 현금결제
		jpCash = new JPanel(); // 현금결제 프레임
		lbCsRcv = new JLabel("받은돈", JLabel.CENTER); // 받은돈
		lbCsCng = new JLabel("잔돈", JLabel.CENTER); // 잔돈
		tfCsRcv = new JTextField(); // 받은돈
		tfCsCng = new JTextField(); // 잔돈
		btnCsPay = new JButton("결제"); // 현금결제 결제
		btnCsPay.setFont(f);
		btnCsPay.setForeground(Color.white);

		// 카드결제
		jpCard = new JPanel(); // 카드결제 패널
		lbCdNum = new JLabel("카드번호", JLabel.CENTER); // 카드번호라벨
		lbCdSum = new JLabel("결제금액", JLabel.CENTER); // 카드 결제금액
		tfCdNum = new JTextField("카드리더사용"); // 카드번호
		tfCdSum = new JTextField(); // 카드 결제금액
		btnCdPay = new JButton("결제"); // 카드결제

		// 쿠폰선택
		jpCp = new JPanel();
		// 쿠폰테이블
		CpModel = new DefaultTableModel();
		jtCpList = new JTable(CpModel);
		jsCpList = new JScrollPane(jtCpList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// 쿠폰패널 핸드폰번호부분
		lbCpCell = new JLabel("번호", JLabel.CENTER);
		tfCpCell = new JTextField("01000000000");
		btnCpCS = new JButton("검색"); // 핸드폰번호 검색버튼
		btnCpCR = new JButton("지우기");

		// 테이블 선택확인
		btnCpList = new JButton("확인");

		// 포인트

		jpPt = new JPanel(); // 포인트프레임
		lbPtCell = new JLabel("번호"); // 포인트검색 위한 핸드폰번호 입력
		tfPtCell = new JTextField(); // 번호입력창
		btnPtCell = new JButton("검색"); // 검색버튼
		lbPtPs = new JLabel("현재P"); // 사용가능 포인트
		tfPtPs = new JTextField();
		lbPtUse = new JLabel("사용P"); // 사용할 포인트
		tfPtUse = new JTextField();
		btnPtUse = new JButton("입력"); // 사용값 입력 버튼
		lbPtRe = new JLabel("잔여P"); // 사용하고 남는 포인트
		tfPtRe = new JTextField();
		btnPtIs = new JButton("사용"); // 포인트사용 최종 버튼

		///////////////// 배치구성//////////////////

		// 가격정보
		jpPrc.setLayout(null);
		jpPrc.setBounds(20, 20, 330, 350);
		jpPrc.setBackground(new Color(215, 188, 171));
		lbPSum.setBounds(25, 60, 50, 50);
		lbDSum.setBounds(25, 150, 50, 50);
		lbCpSum.setBounds(25, 210, 50, 50);
		lbPtSum.setBounds(25, 270, 50, 50);
		tfPSum.setBounds(90, 60, 230, 70);
		tfDSum.setBounds(90, 150, 230, 50);
		tfCpSum.setBounds(90, 210, 230, 50);
		tfPtSum.setBounds(90, 270, 230, 50);

		jpPrc.add(lbPSum);
		jpPrc.add(lbDSum);
		jpPrc.add(lbCpSum);
		jpPrc.add(lbPtSum);
		jpPrc.add(tfPSum);
		jpPrc.add(tfDSum);
		jpPrc.add(tfCpSum);
		jpPrc.add(tfPtSum);

		jpPay.add(jpPrc);

		// 고객정보
		jpInfo.setLayout(null);
		jpInfo.setBounds(360, 20, 330, 350);
		jpInfo.setBackground(new Color(215, 188, 171));
		lbName.setBounds(40, 30, 50, 50);
		lbCell.setBounds(40, 110, 50, 50);
		lbSavePt.setBounds(40, 190, 50, 50);
		lbRePt.setBounds(40, 270, 50, 50);
		tfName.setBounds(90, 30, 230, 50);
		tfCell.setBounds(90, 110, 230, 50);
		tfSavePt.setBounds(90, 190, 230, 50);
		tfRePt.setBounds(90, 270, 230, 50);

		jpInfo.add(lbName);
		jpInfo.add(lbCell);
		jpInfo.add(lbSavePt);
		jpInfo.add(lbRePt);
		jpInfo.add(tfName);
		jpInfo.add(tfCell);
		jpInfo.add(tfSavePt);
		jpInfo.add(tfRePt);

		jpPay.add(jpInfo);

		jpPm.setLayout(null);
		jpPm.setBounds(20, 400, 170, 340);
		jpPm.setBackground(new Color(228, 202, 165));
		btnCash.setBounds(10, 0, 150, 80);
		btnCash.setBorderPainted(false);

		btnCard.setBounds(10, 80, 150, 80);
		btnCard.setBackground(new Color(97, 126, 166));
		btnCard.setBorderPainted(false);

		btnCp.setBounds(10, 160, 150, 80);
		btnCp.setBackground(new Color(97, 126, 166));
		btnCp.setBorderPainted(false);

		btnPt.setBounds(10, 240, 150, 80);
		btnPt.setBackground(new Color(97, 126, 166));
		btnPt.setBorderPainted(false);

		jpPm.add(btnCash);
		jpPm.add(btnCard);
		jpPm.add(btnCp);
		jpPm.add(btnPt);
		// add(jpPm);

		jpPay.add(jpPm);

		//// 결제부분////

		// 현금 결제
		jpCash.setLayout(null);
		jpCash.setBounds(200, 400, 490, 325);
		jpCash.setBackground(new Color(175, 148, 131));
		lbCsRcv.setBounds(25, 90, 80, 80); // 받은돈 레이블
		tfCsRcv.setBounds(100, 100, 230, 50); // 받은돈 텍스트필드
		lbCsCng.setBounds(30, 160, 80, 80); // 잔돈 레이블
		tfCsCng.setBounds(100, 170, 230, 50); // 잔돈 텍스트필드
		btnCsPay.setBounds(350, 100, 100, 120); // 현금 결제 버튼
		// btnCsPay.setBackground(new Color(178,145,128));
		btnCsPay.setBackground(new Color(100, 110, 81));
		btnCsPay.setBorderPainted(false);
		btnCdPay.setFont(f);
		btnCsPay.setForeground(Color.WHITE);

		jpCash.add(lbCsRcv);
		jpCash.add(tfCsRcv);
		jpCash.add(lbCsCng);
		jpCash.add(tfCsCng);
		jpCash.add(btnCsPay);

		jpCardLayout.add(jpCash, "cash");

		// 카드결제 구성
		jpCard.setLayout(null);
		jpCard.setBounds(200, 400, 490, 325);
		jpCard.setBackground(new Color(175, 148, 131));
		lbCdNum.setBounds(25, 90, 80, 80);
		tfCdNum.setBounds(100, 100, 230, 50); // 카드번호
		lbCdSum.setBounds(25, 160, 80, 80); // 카드번호 레이블
		tfCdSum.setBounds(100, 170, 230, 50); // 카드결제금액텍스트필드

		btnCdPay.setForeground(Color.WHITE);
		btnCdPay.setBounds(350, 100, 100, 120); // 카드 결제 버튼
		btnCdPay.setFont(f);
		btnCdPay.setBackground(new Color(173, 93, 94));
		btnCdPay.setBorderPainted(false);

		jpCard.add(lbCdNum);
		jpCard.add(tfCdNum);
		jpCard.add(lbCdSum);
		jpCard.add(tfCdSum);
		jpCard.add(btnCdPay);

		jpCardLayout.add(jpCard, "card");

		// 쿠폰 선택 구성
		jpCp.setLayout(null);
		jpCp.setBounds(200, 380, 490, 325);
		jpCp.setBackground(new Color(175, 148, 131));
		lbCpCell.setBounds(30, 50, 50, 30);
		tfCpCell.setBounds(80, 40, 230, 40);
		btnCpCR.setBounds(315, 40, 80, 40);
		btnCpCR.setBackground(new Color(216, 175, 70));
		btnCpCR.setFont(s);
		btnCpCR.setForeground(Color.white);
		btnCpCR.setBorderPainted(false);

		btnCpCS.setBounds(400, 40, 80, 40);
		btnCpCS.setBackground(new Color(100, 110, 81));
		btnCpCS.setFont(s);
		btnCpCS.setForeground(Color.white);
		btnCpCS.setBorderPainted(false);

		jsCpList.setBounds(40, 100, 355, 200);
		btnCpList.setBounds(400, 220, 80, 80);
		btnCpList.setBackground(new Color(173, 93, 94));
		btnCpList.setFont(s);
		btnCpList.setForeground(Color.white);
		btnCpList.setBorderPainted(false);

		jpCp.add(lbCpCell);
		jpCp.add(tfCpCell);
		jpCp.add(btnCpCS);
		jpCp.add(btnCpCR);
		jpCp.add(jsCpList);
		jpCp.add(btnCpList);

		jpCardLayout.add(jpCp, "coupon");

		// 포인트 구성
		jpPt.setLayout(null);
		jpPt.setBounds(200, 380, 490, 325);
		jpPt.setBackground(new Color(175, 148, 131));

		lbPtCell.setBounds(50, 40, 50, 40);
		tfPtCell.setBounds(100, 40, 250, 40);
		btnPtCell.setBounds(370, 40, 80, 40);
		btnPtCell.setBackground(new Color(216, 175, 70));
		btnPtCell.setBorderPainted(false);
		btnPtCell.setFont(s);
		btnPtCell.setForeground(Color.white);

		lbPtPs.setBounds(50, 100, 50, 40);
		tfPtPs.setBounds(100, 100, 250, 40);
		lbPtUse.setBounds(50, 160, 50, 40);
		tfPtUse.setBounds(100, 160, 250, 40);

		btnPtUse.setBounds(370, 160, 80, 40);
		btnPtUse.setBackground(new Color(100, 110, 81));
		btnPtUse.setBorderPainted(false);
		btnPtUse.setFont(s);
		btnPtUse.setForeground(Color.WHITE);

		lbPtRe.setBounds(50, 220, 50, 40);
		tfPtRe.setBounds(100, 220, 250, 40);

		btnPtIs.setBounds(370, 220, 80, 40);
		btnPtIs.setBackground(new Color(173, 93, 94));
		btnPtIs.setBorderPainted(false);
		btnPtIs.setFont(s);
		btnPtIs.setForeground(Color.WHITE);

		jpPt.add(lbPtCell); // 포인트검색 위한 핸드폰번호 입력
		jpPt.add(tfPtCell); // 번호입력창
		jpPt.add(btnPtCell); // 검색버튼
		jpPt.add(lbPtPs); // 사용가능 포인트
		jpPt.add(tfPtPs);
		jpPt.add(lbPtUse); // 사용포인트
		jpPt.add(tfPtUse);
		jpPt.add(btnPtUse); // 사용값 입력
		jpPt.add(lbPtRe); // 사용하고 남는 포인트
		jpPt.add(tfPtRe);
		jpPt.add(btnPtIs); // 포인트사용 최종 버튼

		jpCardLayout.add(jpPt, "point");

		// add(jpPay);

		///// 숫자창////

		String[] btn = { "7", "8", "9", "4", "5", "6", "1", "2", "3", "0", "c", "입력" };
		jbtn = new JButton[4][3];
		// setLayout(null);
		jbtnc = new JButton("C");
		jbtninput = new JButton("입력");

		jbtnc.setBackground(new Color(216, 175, 70));
		jbtninput.setBackground(new Color(216, 175, 70));
		jbtnc.setBorderPainted(false);
		jbtninput.setBorderPainted(false);

		jp2 = new JPanel();
		jp2.setBackground(new Color(215, 188, 171));

		jp2.add(jbtnc);
		jp2.add(jbtninput);

		int x = 20, y = 20;
		for (int i = 0; i < jbtn.length; i++) {
			x = 20;
			for (int j = 0; j < jbtn[i].length; j++) {
				if (i != 0) {
					jbtn[i][j] = new JButton(btn[i * jbtn[i - 1].length + j]);
				} else {
					jbtn[i][j] = new JButton(btn[j]);
				}
				jbtn[i][j].setLocation(x, y);
				x += 80;
				jbtn[i][j].setSize(60, 60);
				jp2.add(jbtn[i][j]);
				jbtn[i][j].setBackground(new Color(216, 175, 70));
				jbtn[i][j].addActionListener(this);
				jbtn[i][j].setBorderPainted(false);
				jbtninput.addActionListener(this);
				jbtnc.addActionListener(this);

			}
			y += 70;
		}

		jbtnc.setBounds(100, 230, 60, 60);
		jbtninput.setBounds(180, 230, 60, 60);

		jp2.setLayout(null);
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		jp2.setBounds(750, 400, 260, 320);

		// add(jp2);
		jpPay.add(jp2);
		jpPay.add(jpCardLayout);

		btnCash.addActionListener(this);
		btnCsPay.addActionListener(this);
		btnCard.addActionListener(this);
		btnCdPay.addActionListener(this);
		btnCp.addActionListener(this);
		btnCpCS.addActionListener(this);
		btnCpCR.addActionListener(this);
		btnPt.addActionListener(this);
		btnPtUse.addActionListener(this);
		jbtninput.addActionListener(this);
		btnPtIs.addActionListener(this);
		btnPtCell.addActionListener(this);
		btnCpList.addActionListener(this);

		tfCsRcv.addMouseListener(this);
		tfCdNum.addMouseListener(this);
		tfCpCell.addMouseListener(this);
		tfPtCell.addMouseListener(this);
		tfPtUse.addMouseListener(this);

		// setBounds(20, 20, 1100, 900);
		// setVisible(true);
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		jpPay.repaint();
	}

	/*
	 * public static void main(String[] args) { new Cafe(); }
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		c = e.getComponent();

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		String obj2 = e.getActionCommand();

		if (obj == btnCash) {
			cl.show(jpCardLayout, "cash");
			System.out.println("cash");
		} else if (obj == btnCard) {
			cl.show(jpCardLayout, "card");
			System.out.println("card");
		} else if (obj == btnCp) {
			cl.show(jpCardLayout, "coupon");
			System.out.println("coupon");
		} else if (obj == btnPt) {
			cl.show(jpCardLayout, "point");
			System.out.println("point");
		}

		/////////////// 숫자창 입력//////////////////

		// 받은금액
		if (c == tfCsRcv && obj != btnCsPay && obj != jbtninput && obj != jbtnc && obj != btnCard && obj != btnCash
				&& obj != btnCp && obj != btnPt && obj != btnCpCR && obj != btnCpCS && obj != btnCpList) {
			String data = tfCsRcv.getText();
			tfCsRcv.setText(data + obj2);
			a = Integer.parseInt(data + obj2);
			System.out.println("받은금액");
		} else if (obj == jbtnc) {
			tfCsRcv.setText("");
			System.out.println("657");
		}
		// 카드번호
		if (c == tfCdNum && obj != btnCard && obj != btnCash && obj != btnCp && obj != btnPt && obj != btnCdPay
				&& obj != jbtninput && obj != jbtnc && obj != btnCpCR && obj != btnCpCS && obj != btnCpList) {
			String data = tfCdNum.getText();
			tfCdNum.setText(data + obj2);
			a = Integer.parseInt(data + obj2);
			System.out.println("665");
		} else if (obj == jbtnc) {
			tfCdNum.setText("");
			System.out.println("668");
		}
		/// 쿠폰패널 핸드폰번호 입력창
		if (c == tfCpCell && obj != btnCpCS && obj != btnCpCR && obj != btnCpList && obj != btnCard && obj != btnCash
				&& obj != btnCp && obj != btnPt && obj != jbtninput && obj != jbtnc
				&& obj != btnCsPay && obj != btnCdPay) {
			String data = tfCpCell.getText();
			tfCpCell.setText(data + obj2);
			a = Integer.parseInt(data + obj2);
			System.out.println("676");
		} else if (obj == jbtnc) {
			tfCpCell.setText("");
			System.out.println("679");
		}
		// 포인트창 핸드폰번호 입력
		if (c == tfPtCell && obj != btnPtCell && obj != btnPtIs && obj != btnPtUse && obj != jbtninput && obj != btnCard
				&& obj != btnCash && obj != btnCp && obj != btnPt && obj != jbtninput && obj != jbtnc
				&& obj != btnCard && obj != btnCsPay && obj != btnCdPay) {
			String data = tfPtCell.getText();
			tfPtCell.setText(data + obj2);
			a = Integer.parseInt(data + obj2);
			System.out.println("687");
		} else if (obj == jbtnc) {
			tfPtCell.setText("");
			System.out.println("690");
		}
		// 포인트창 사용포인트 입력
		if (c == tfPtUse && obj != btnPtCell && obj != btnPtIs && obj != btnPtUse && obj != jbtninput && obj != btnCard
				&& obj != btnCash && obj != btnCp && obj != btnPt && obj != jbtninput && obj != jbtnc) {
			String data = tfPtUse.getText();
			tfPtUse.setText(data + obj2);
			a = Integer.parseInt(data + obj2);
			System.out.println("698");
		} else if (obj == jbtnc) {
			tfPtUse.setText("");
			System.out.println("701");
		}

		/*
		 * if (obj == btnCard) { // 카드창으로 가면 int sum = 0; sum =
		 * Integer.parseInt(tfPSum.getText()); tfCdSum.setText("" + sum);
		 * 
		 * if (obj == jbtninput) { // 숫자입력값 받아서 카드넘버에 넣기 }
		 * 
		 * if (obj == btnCdPay) { // 3초뒤 초기화면으로 이동
		 * 
		 * } CustomerVO csvo = csdao.selectCustomerOne(tfPtCell.getText());
		 * csvo.setPoint(Integer.parseInt(tfPtRe.getText()));
		 * csdao.UpdateCustomerPoint(csvo);
		 * 
		 * }
		 */

		////// dao 선언//////////////
		CustomerDAO csdao = new CustomerDAO();
		OwnCouponDAO ocdao = new OwnCouponDAO();
		CouponDAO cpdao = new CouponDAO();

		ArrayList<CouponVO> list = new ArrayList<CouponVO>();
		ArrayList<OwnCouponVO> cpv = ocdao.selectAll(tfCpCell.getText(), 1);
		
		// OwnCouponVO ocpvo = new OwnCouponVO();
		
		//// 쿠폰///////
		if (obj == btnCpCS) {
			System.out.println("730");
			CpModel.setNumRows(0);
			// 쿠폰창 핸드폰번호 일치여부
			CustomerVO csvo = csdao.selectCustomerOne(tfCpCell.getText());
			
			if (csvo != null) {
				Login.COSTOMER_HP = tfCpCell.getText();
				System.out.println("736");
				tfName.setText(csvo.getcName());
				tfCell.setText(csvo.getHp());
				tfPtPs.setText("" + csvo.getPoint()); // 고객정보창에 표시
				tfRePt.setText("" + csvo.getPoint()); // 포인트패널에있는 현재포인트에 표시
				tfSavePt.setText("" + pt);

				// 쿠폰에서 핸드폰검색하면 포인트에도 그 값을 입력해둔다. -부가기능 필수 아님-
				tfPtCell.setText("" + csvo.getHp());
				tfRePt.setText("" + csvo.getPoint()); // 포인트패널에있는 현재포인트에 표시

				/// 쿠폰 테이블에 데이터 불러오기

				for (OwnCouponVO cpvo : cpv) {

					// a = 쿠폰 날짜 / b = 현재 날짜
					int a = Integer.parseInt(cpvo.getEndDate().toString().replaceAll("-", ""));
					String g = year + "" + month + "" + day;
					int b = Integer.parseInt(g);

					String[] cp = { "쿠폰이름", "할인금액", "취득일", "유효기간" };
					String[] cpp = { cpvo.getCpName(), "" + cpvo.getReducedprice(), "" + cpvo.getEndDate(),
							"" + cpvo.getEndDate() };
					CpModel.setColumnIdentifiers(cp);

					if (a >= b) { // 현재날짜보다 기한 더 많이 남은것만 테이블에 추가
						CpModel.addRow(cpp);
					}
				}

				// 핸드폰번호와 일치하는 고객 없으면
			} else {
				tfCell.setText("존재하지않는 고객입니다");
			}

		} else if (obj == btnCpCR) { // 지우기 누르면 텍스트필드 초기화
			tfCpCell.setText("");
		}
		if (obj == btnCpList) {
			System.out.println("775");
			int cp = 0;
			dsum = Integer.parseInt(tfDSum.getText());
			tfPtSum.setText("");// 포인트랑 동시적용 안되게
			tfPSum.setText(""); // 혹시 값이 있을수도 있으니까 널값넣기

			int row = jtCpList.getSelectedRow(); // 테이블의 선택한로우값
			Object value = jtCpList.getValueAt(row, 1); // 선택 // 1은 가격의 위치값
			tfCpSum.setText("" + value);
			cp = Integer.parseInt(tfCpSum.getText()); ///////// 테이블 열 선택하면 해당 열의
														///////// 쿠폰할인금액 선택해서
														///////// 넣기///////////////
			sum = dsum - cp;
			tfPSum.setText("" + sum);
			tfSavePt.setText("" + (sum / 50 - sum/50%100));
			cl.show(jpCardLayout, "cash");

			if (obj == btnCsPay) {
				System.out.println("793");
				String payMethod = "CASH";
				new CompleteOrder(order, payMethod);
			}

		}

		/// 포인트//////

		CustomerVO csvo = csdao.selectCustomerOne(tfPtCell.getText());
		if (obj == btnPtCell) {
			System.out.println("804");
			if (csvo != null) {
				System.out.println("806");
				tfName.setText(csvo.getcName());
				tfCell.setText(csvo.getHp());
				tfPtPs.setText("" + csvo.getPoint()); // 고객정보창에 표시
				tfRePt.setText("" + csvo.getPoint()); // 포인트패널에있는 현재포인트에 표시
				tfPtRe.setText("" + csvo.getPoint()); // 포인트패널 - 잔여포인트

				tfCpCell.setText("" + csvo.getHp()); // 쿠폰창에도 핸드폰번호 띄워놓기
				// 단 다시 텍스트필드 선택하고 검색버튼을 눌러야 쿠폰이 뜬다는 단점이 있음
				// 고치면 지워////////////////////

			} else {
				System.out.println("818");
				tfName.setText("");
				tfCell.setText("존재하지않는 고객입니다");
				tfPtPs.setText("");
				tfRePt.setText("");

			}
		}

		if (obj == btnPtUse) {
			System.out.println("828 입력");
			pp = Integer.parseInt(tfPtPs.getText()); // 잔여포인트값 받아오기
			up = Integer.parseInt(tfPtUse.getText()); // 사용할 포인트 값 받아오기
			if (pp >= up) { // 잔여포인트가 사용포인트보다 크면 실행
				System.out.println("832");
				dsum = Integer.parseInt(tfDSum.getText());
				rep = (pp - up); // 잔여포인트
				tfPtRe.setText("" + rep);
			} else { // 잔여포인트가 사용포인트보다 작으면 사용포인트에표시
				System.out.println("837");
				tfPtUse.setText("포인트가 부족합니다.");
			}

		}
		if (obj == btnPtIs) { // 포인트 결제버튼 누르면
			System.out.println("843 사용");
			tfPtSum.setText(tfPtUse.getText());
			// 가격정보창에있는 사용포인트텍스트필드에 사용할 포인트 값 넣기
			tfCpSum.setText("");// 쿠폰이랑 동시적용 안되게
			tfPSum.setText(""); // 혹시 값이 있을수도 있으니까 널값넣기
			dsum = Integer.parseInt(tfDSum.getText());
			sum = dsum - up; // 합계는 음료가격 - 사용포인트
			System.out.println("sum : "+sum);
			System.out.println("dsum : "+dsum);
			System.out.println("up : "+up);
			tfPSum.setText("" + sum); // 합계에
			tfSavePt.setText("" + sum / 50);
			tfRePt.setText(tfPtRe.getText());
			cl.show(jpCardLayout, "cash");

		}

		// 현금결제//////

		if (obj == btnCsPay) { // 잔돈 표시
			System.out.println("859 결제 -> 에러");
			sum = 0;
			sum = Integer.parseInt(tfPSum.getText()); // 가격합계
			cspay = Integer.parseInt(tfCsRcv.getText()); // 받은돈
			cscg = cspay - sum; // 잔돈 = 받은돈 -가격합계
			tfCsCng.setText("" + cscg);
			Login.COSTOMER_HP = tfCpCell.getText();
			try {
				// 포인트 사용했으므로 업데이트 ㄱㄱ CustomerVO csvo =
				csdao.selectCustomerOne(tfPtCell.getText());
				csvo.setPoint(Integer.parseInt(tfPtRe.getText()) + Integer.parseInt(tfSavePt.getText()));
				csdao.UpdateCustomerPoint(csvo);
				System.out.println("871");
			} catch (Exception e1) {
				System.out.println("873");
			}

			try {
				if (jtCpList.getSelectedRowCount() != 0) {
					System.out.println("877");
					// 쿠폰
					int rowcu = jtCpList.getSelectedRow();// 테이블의 선택한로우값
					String cpName = (String) jtCpList.getValueAt(rowcu, 0); // 쿠폰이름
																			// 선택
					// Object cafeID = jtCpList.getValueAt(rowcu, 2);
					String cpid = cpdao.selectCouponID(cpName);
					CouponDAO cpdao2 = new CouponDAO();
					if (!cpName.equals("")) {
						System.out.println("886");
						String cafeid = "cafe0001";
						ocdao.updateOCUSed(cpid, cafeid, tfCpCell.getText());

					}
				}
			} catch (Exception e1) {
				System.out.println("892");
			}

			// 포인트적립
			try {
				System.out.println("897");
				csdao.selectCustomerOne(tfPtCell.getText());
				csvo.setPoint(Integer.parseInt(tfRePt.getText()) + Integer.parseInt(tfSavePt.getText()));
				csdao.UpdateCustomerPoint(csvo);
			} catch (Exception e1) {
				System.out.println("902");
			}

			
			String payMethod = "CASH";
			new CompleteOrder(order, payMethod);
			 

		} // 현금결제 ~ 여기까지 복붙

		if (obj == btnCard) { // 카드창으로 가면
			System.out.println("905");
			int sum = 0;
			sum = Integer.parseInt(tfPSum.getText());
			tfCdSum.setText("" + sum);
		}
		if (obj == jbtninput) {
			System.out.println("911");
			// 숫자입력값 받아서 카드넘버에 넣기
		}

		if (obj == btnCdPay) {
			Login.COSTOMER_HP = tfCpCell.getText();
			System.out.println("916");
			String payMethod = "CARD";
			new CompleteOrder(order, payMethod);
		}

			// 포인트 사용했으므로 업데이트 ㄱㄱ CustomerVO csvo =
		try {
			csdao.selectCustomerOne(tfPtCell.getText());
			csvo.setPoint(Integer.parseInt(tfPtRe.getText()) + Integer.parseInt(tfSavePt.getText()));
			csdao.UpdateCustomerPoint(csvo);
		} catch (Exception e1) {
		}

		// 포인트적립
		try {
			csdao.selectCustomerOne(tfCpCell.getText());
			csvo.setPoint(Integer.parseInt(tfRePt.getText()) + Integer.parseInt(tfSavePt.getText()));
			csdao.UpdateCustomerPoint(csvo);
		} catch (Exception e1) {
		}

		// 쿠폰
		try {
			int rowcu = jtCpList.getSelectedRow();// 테이블의 선택한로우값
			String cpName = (String) jtCpList.getValueAt(rowcu, 0); // 쿠폰이름
																		// 선택
			// Object cafeID = jtCpList.getValueAt(rowcu, 2);
			String cpid = cpdao.selectCouponID(cpName);
			CouponDAO cpdao2 = new CouponDAO();
			if (!cpName.equals("")) {
				String cafeid = "cafe0001";
				ocdao.updateOCUSed(cpid, cafeid, tfCpCell.getText());
			}
		} catch (Exception e1) {

		}
		try {
			CustomerVO csvo1 = csdao.selectCustomerOne(tfPtCell.getText());
			csvo1.setPoint(Integer.parseInt((tfSavePt.getText() + tfPtRe.getText())));
		} catch (Exception e1) {

	} // 카드결제 ~ 여기까지 복붙
			//

	}

}
