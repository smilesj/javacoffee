package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Popup2 extends JFrame implements ActionListener {
	JButton btn1, btn2, btn3;
	private int num = 1, price = 0;
	private String name = null;
	JPanel jp;
	Order order;
	int totalPrice = 0;
	

	public Popup2(String title, int price, Order order) {
		// initialize
		this.price = price;
		this.name = title;
		this.order = order;
		
		JPanel jp = new JPanel();

		setTitle(title);
		btn1 = new JButton("tall");
		btn2 = new JButton("grande");
		btn3 = new JButton("venti");
		
		Font f = new Font("���� ����", Font.BOLD, 15);
	

		// button layout
		btn1.setBounds(30, 30, 120, 120);
		btn2.setBounds(170, 30, 120, 120);
		btn3.setBounds(310, 30, 120, 120);
		
		btn1.setBackground(new Color(241,195,90));
		btn2.setBackground(new Color(241,195,90));
		btn3.setBackground(new Color(241,195,90));
		
		btn1.setBorderPainted(false);
		btn2.setBorderPainted(false);
		btn3.setBorderPainted(false);
		
		btn1.setForeground(Color.darkGray);
		btn2.setForeground(Color.darkGray);
		btn3.setForeground(Color.darkGray);
		
		btn1.setFont(f);
		btn2.setFont(f);
		btn3.setFont(f);
		
	
		// add
		jp.setLayout(null);
		jp.setBackground(new Color(228, 202, 165));
		jp.add(btn1);
		jp.add(btn2);
		jp.add(btn3);
		
		jp.setBounds(0, 0, 600, 200);
		add(jp);
		
	
		// layout
		setLayout(null);
		setBounds(200, 200, 470, 210);
		setVisible(true);

		// call action listener
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn1) {
			price += 0;
			name += (" TALL");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		} else if (obj == btn2) {
			price += 500;
			name += (" GRANDE");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		} else if (obj == btn3) {
			price += 1000;
			name += (" VENTI");
			Object[] menu = { name, num, price };
			order.model.addRow(menu);
			order.cal();
			setVisible(false);
		} 
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
