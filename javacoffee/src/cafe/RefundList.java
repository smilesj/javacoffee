package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.dao.OrdersDAO;
import database.vo.OrdersVO;

public class RefundList implements ActionListener {
	JPanel jp;
	JScrollPane jsp;
	JTable jtb;
	DefaultTableModel model;
	JButton btnHistory, btnCancel;
	
	RefundList(JPanel jp){
		// initialize
		this.jp = jp;
		jp.removeAll();
		
		Font f = new Font("맑은 고딕", Font.BOLD, 15);
		
		btnHistory = new JButton("히스토리");
		btnHistory.setBackground(new Color(216,175,70));
		btnHistory.setBorderPainted(false);
		btnHistory.setFont(f);
		btnHistory.setForeground(Color.WHITE);
		
		btnCancel = new JButton("환 불");
		btnCancel.setBackground(new Color(173,93,94));
		btnCancel.setFont(f);
		btnCancel.setBorderPainted(false);
		btnCancel.setForeground(Color.WHITE);

		
		// scrollpane & table
		model = new DefaultTableModel();
		String[] colName = { "시간", "OrderID", "ID", "결제방법", "합계" };
		model.setColumnIdentifiers(colName);
		jtb = new JTable(model);
		jtb.setRowHeight(30);
		jsp = new JScrollPane(jtb, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		
		// layout
		jp.setLayout(null);
		btnHistory.setBounds(50, 10, 150, 60);
		btnCancel.setBounds(860, 630, 150, 60);
		jsp.setBounds(50,100,960,500);
		
		// add
		jp.add(btnHistory);
		jp.add(btnCancel);
		jp.add(jsp);
		
		
		// call action listener
		btnHistory.addActionListener(this);
		btnCancel.addActionListener(this);
		
		jp.repaint();
		// basic window

		OrdersDAO dao = new OrdersDAO();
		ArrayList<OrdersVO> list = dao.selectOne();
		
		for (OrdersVO vo : list) {
			
			Object[] menu = {vo.getOrderTime(),vo.getOrderid(), vo.getHp(),vo.getPayMethod(),vo.getTotalPrice()};
			model.addRow(menu);
			
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if(obj == btnHistory){
			new Sales(jp);
		}else if (obj == btnCancel){
			new Refund((String)jtb.getValueAt(jtb.getSelectedRow(), 1));
			new RefundList(jp);
		}
		
	}
}
