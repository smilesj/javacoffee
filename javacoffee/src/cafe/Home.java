package cafe;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
 
public class Home extends JPanel implements ActionListener {
 
    JButton btnOrder, btnOrderList, btnSal, btnCharge, btnCash, btnEnd, btnRefund, btnExit;
    JPanel jp;
    boolean selectOK = false;
    static int cash = 0;
     
     ImageIcon powered, power, orderlistx, /*orderlist,*/ orderx, order, salx, sal,
        chargex, charge, cashx, casho, endx, end, refundx, refund;
        Image img1, img2;
 
     
    Home(){};
     
    Home(JPanel jp) {
		jp = new JPanel();
		this.jp = jp;
		
		jp.removeAll();
		jp.setLayout(null);
		jp.setBounds(20, 120, 1060, 750);		
		jp.setBackground(new Color(228, 202, 165));
		
		 // 전원버튼 이미지
        powered = new ImageIcon("src/img/power.png");
        img1 = powered.getImage();
        img2 = img1.getScaledInstance(80, 80, java.awt.Image.SCALE_SMOOTH);
        power = new ImageIcon(img2);
        btnExit = new JButton(power);
        btnExit.setBorderPainted(false);
        btnExit.setContentAreaFilled(false);
 
        // 주문
        orderx = new ImageIcon("src/img/order1.png");
        img1 = orderx.getImage();
        img2 = img1.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH);
        order = new ImageIcon(img2);
        btnOrder = new JButton(order);
        btnOrder.setBorderPainted(false);
        btnOrder.setContentAreaFilled(false);
         
        
        // 매출
        salx = new ImageIcon("src/img/매출1.png");
        img1 = salx.getImage();
        img2 = img1.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH);
        sal = new ImageIcon(img2);
        btnSal = new JButton(sal);
        btnSal.setBorderPainted(false);
        btnSal.setContentAreaFilled(false);
         
         
        // 충전
        chargex = new ImageIcon("src/img/charge1.png");
        img1 = chargex.getImage();
        img2 = img1.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH);
        charge = new ImageIcon(img2);
        btnCharge = new JButton(charge);
        btnCharge.setBorderPainted(false);
        btnCharge.setContentAreaFilled(false);      
         
         
        // 시제
        cashx = new ImageIcon("src/img/시제1.png");
        img1 = cashx.getImage();
        img2 = img1.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH);
        casho = new ImageIcon(img2);
        btnCash = new JButton(casho);
        btnCash.setBorderPainted(false);
        btnCash.setContentAreaFilled(false);    
         
         
        // 마감
        endx= new ImageIcon("src/img/마감1.png");
        img1 = endx.getImage();
        img2 = img1.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH);
        end = new ImageIcon(img2);
        btnEnd = new JButton(end);
        btnEnd.setBorderPainted(false);
        btnEnd.setContentAreaFilled(false);         
         
        // 환불
        refundx= new ImageIcon("src/img/환불1.png");
        img1 = refundx.getImage();
        img2 = img1.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH);
        refund = new ImageIcon(img2);
        btnRefund = new JButton(refund);
        btnRefund.setBorderPainted(false);
        btnRefund.setContentAreaFilled(false);  
         
     
 
        btnOrder.addActionListener(this);
        btnSal.addActionListener(this);
        btnCharge.addActionListener(this);
        btnCash.addActionListener(this);
        btnEnd.addActionListener(this);
        btnRefund.addActionListener(this);
        btnExit.addActionListener(this);
 
        btnOrder.setBounds(120, 50, 180, 200);
        btnSal.setBounds(120, 320, 180, 200);
        btnCharge.setBounds(420, 50, 180, 200);
        btnCash.setBounds(420, 340, 180, 200);
        btnEnd.setBounds(700, 320, 180, 200);
        btnRefund.setBounds(700, 50, 180, 200);
        btnExit.setBounds(870, 530, 180, 200);


		// add
		jp.add(btnOrder);
		jp.add(btnSal);
		jp.add(btnCharge);
		jp.add(btnCash);
		jp.add(btnEnd);
		jp.add(btnRefund);
		jp.add(btnExit);
		
		jp.repaint();
	}

 
    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == btnOrder) {
            new Order(this.jp);
        } else if (obj == btnSal) {
            new Sales(this.jp);
            System.out.println("매출");
        } else if (obj == btnCharge) {
            new Charge(this.jp);
            System.out.println("충전");
        } else if (obj == btnCash) {
            Cash c = new Cash();
            this.cash = c.cash;
            System.out.println(cash);
            System.out.println("시재");
        } else if (obj == btnEnd) {
            new Calculate(this.jp);
            System.out.println("마감");
        } else if (obj == btnRefund) {
            new RefundList(this.jp);
            System.out.println("환불");
        } else if (obj == btnExit) {
 
            System.exit(0);
        }
 
    }
 
}

