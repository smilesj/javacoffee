package cafe;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Order extends JPanel implements ActionListener {
	// variable
	JPanel jp, jpMenu;
	JButton btnCoffee, btnTea, btnSmoothie, btnService;
	JButton btnPlus, btnMinus, btnCancel, btnComplete;
	JScrollPane jsp;
	JTable jtb;
	JLabel lb1, lb2;
	JTextField tf;
	DefaultTableModel model;

	String name;
	int num, price;
	Popup popup;
	SalesCondition sc;

	// �������̹���
	ImageIcon upx, up, downx, downn, iconxx, iconx;
	Image img1, img2;

	Order(JPanel jp) {
		// initialize

		this.jp = jp;
		jp.removeAll();

		// up �̹���
		upx = new ImageIcon("src/img/up.png");
		img1 = upx.getImage();
		img2 = img1.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH);
		up = new ImageIcon(img2);
		btnPlus = new JButton(up);
		btnPlus.setBorderPainted(false);
		btnPlus.setContentAreaFilled(false);

		// down �̹���
		downx = new ImageIcon("src/img/down.png");
		img1 = downx.getImage();
		img2 = img1.getScaledInstance(40, 40, java.awt.Image.SCALE_SMOOTH);
		downn = new ImageIcon(img2);
		btnMinus = new JButton(downn);
		btnMinus.setBorderPainted(false);
		btnMinus.setContentAreaFilled(false);

		// x �̹���
		iconxx = new ImageIcon("src/img/iconx3.png");
		img1 = iconxx.getImage();
		img2 = img1.getScaledInstance(43, 43, java.awt.Image.SCALE_SMOOTH);
		iconx = new ImageIcon(img2);
		btnCancel = new JButton(iconx);
		btnCancel.setBorderPainted(false);
		btnCancel.setContentAreaFilled(false);

		jpMenu = new JPanel();
		btnCoffee = new JButton("Ŀ��");
		btnTea = new JButton("Ƽ");
		btnSmoothie = new JButton("SPECIAL");
		btnService = new JButton("����");
		// btnPlus = new JButton("+");
		// btnMinus = new JButton("-");
		// btnCancel = new JButton("X");
		btnComplete = new JButton("�ֹ��Ϸ�");

		model = new DefaultTableModel();
		String[] colName = { "�޴�", "����", "�ݾ�" };
		model.setColumnIdentifiers(colName);

		jtb = new JTable(model);
		jtb.setRowHeight(30);

		jsp = new JScrollPane(jtb, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		lb1 = new JLabel("�հ�");
		lb2 = new JLabel("��");
		tf = new JTextField();

		// font
		Font m = new Font("���� ����", Font.BOLD, 20);
		lb1.setFont(new Font("���� ����", Font.BOLD, 25));
		lb2.setFont(new Font("���� ����", Font.BOLD, 25));
		tf.setFont(new Font("���� ����", Font.BOLD, 20));
		btnPlus.setFont(new Font("���� ����", Font.BOLD, 15));
		btnMinus.setFont(new Font("���� ����", Font.BOLD, 15));
		btnCancel.setFont(new Font("���� ����", Font.BOLD, 15));
		btnComplete.setFont(new Font("���� ����", Font.BOLD, 20));

		// user layout
		jpMenu.setLayout(null);

		btnCoffee.setBounds(30, 600, 150, 80);
		btnCoffee.setBackground(new Color(100,110,81));
		btnCoffee.setBorderPainted(false);
		btnCoffee.setFont(m);
		btnCoffee.setForeground(Color.white);
		
		btnTea.setBounds(216, 600, 150, 80);
		btnTea.setBackground(new Color(100,110,81));
		btnTea.setBorderPainted(false);
		btnTea.setFont(m);
		btnTea.setForeground(Color.white);
		
		btnSmoothie.setBounds(402, 600, 150, 80);
		btnSmoothie.setBackground(new Color(100,110,81));
		btnSmoothie.setBorderPainted(false);
		btnSmoothie.setFont(m);
		btnSmoothie.setForeground(Color.white);
		
		btnService.setBounds(588, 600, 150, 80);
		btnService.setBackground(new Color(100,110,81));
		btnService.setBorderPainted(false);
		btnService.setFont(m);
		btnService.setForeground(Color.white);
		
		btnMinus.setBounds(750, 630, 45, 45);
		btnPlus.setBounds(800, 630, 45, 45);
		btnCancel.setBounds(850, 630, 45, 45);
		
		btnComplete.setBounds(910, 620, 120, 60);
		btnComplete.setBackground(new Color(173,93,94));
		btnComplete.setBorderPainted(false);
		btnComplete.setForeground(Color.white);
		
		jsp.setBounds(760, 30, 270, 500);
		lb1.setBounds(760, 550, 100, 50);
		lb2.setBounds(1010, 550, 100, 50);
		tf.setBounds(830, 550, 170, 50);

		// panel layout
		jpMenu.setBounds(30, 10, 710, 560);
		jpMenu.setBackground(new Color(215,188,171));

		// call action listener
		btnCoffee.addActionListener(this);
		btnTea.addActionListener(this);
		btnSmoothie.addActionListener(this);
		btnService.addActionListener(this);
		btnPlus.addActionListener(this);
		btnMinus.addActionListener(this);
		btnCancel.addActionListener(this);
		btnComplete.addActionListener(this);

		// add
		jp.add(jpMenu);
		jp.add(btnCoffee);
		jp.add(btnTea);
		jp.add(btnSmoothie);
		jp.add(btnService);
		jp.add(btnPlus);
		jp.add(btnMinus);
		jp.add(btnCancel);
		jp.add(btnComplete);
		jp.add(jsp);
		jp.add(lb1);
		jp.add(lb2);
		jp.add(tf);

		// basic window
		new CoffeeMenu(this);

		jp.repaint();
	}

	public void cal() {
		int count = jtb.getRowCount();
		int totalPrice = 0;
		for (int i = 0; i < count; i++) {
			totalPrice += (int) jtb.getValueAt(i, 2);
		}
		tf.setText(Integer.toString(totalPrice));
	}

	// action performed
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnCoffee)
			new CoffeeMenu(this);
		else if (obj == btnTea)
			new TeaMenu(this);
		else if (obj == btnSmoothie)
			new SpecialMenu(this);
		else if (obj == btnService)
			new ServiceMenu(this);
		else if (obj == btnPlus)
			new PlusMenu(this);
		else if (obj == btnMinus)
			new MinusMenu(this);
		else if (obj == btnCancel)
			new CancelMenu(this);
		else if (obj == btnComplete)
			new Cafe(this);
	}
}
