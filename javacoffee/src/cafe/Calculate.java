package cafe;
 
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
 
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
 
import database.dao.OrdersDAO;
import database.vo.OrdersVO;
 
public class Calculate extends JPanel implements ActionListener, MouseListener {
    JPanel jp, jp2;
    JLabel jl5man, jl1man, jl5chen, jl1chen, jl5bec, jl1bec, jlcash, jlcard, jlcoupon, jlpoint, jlresult;
    JTextField jtf[] = new JTextField[10];
    JButton[][] jbtn;
    JButton jbtnend;
    int x1, x2;
    int y1, y2;
    int Number1, Number2, Number3, Number4, Number5, Number6;
    String oper = "";
    Component c;
    int m;
 
    private int cash = 0, card = 0;
 
    private int currentBtnIdx = 0; // 초기값 설정
 
    public Calculate(JPanel jp) {
        this.jp = jp;
        Home h = new Home();
        int cash1 = h.cash;
        System.out.println(cash1);
        jp.removeAll();
        String[] btn = { "7", "8", "9", "4", "5", "6", "1", "2", "3", "0", "c", "입력" };
        
 
        jbtn = new JButton[4][3];
        addMouseListener(this);
        jp.setLayout(null);
        Font f = new Font("맑은 고딕", Font.BOLD, 25);
        Font f2 = new Font("맑은 고딕", Font.BOLD, 13);
        jp2 = new JPanel();
        jp2.setBackground(new Color(200, 190, 150));
 
        jl5man = new JLabel("5만원");
        jl1man = new JLabel("1만원");
        jl5chen = new JLabel("5천원");
        jl1chen = new JLabel("1천원");
        jl5bec = new JLabel("5백원");
        jl1bec = new JLabel("1백원");
        jlcash = new JLabel("현금");
        jlcard = new JLabel("카드");
        jlcoupon = new JLabel("쿠폰");
        jlpoint = new JLabel("포인트");
        jlresult = new JLabel();
 
        // db에서 시제 불러오기
        OrdersDAO dao = new OrdersDAO();
 
        ArrayList<OrdersVO> list = dao.selectOne();
        ArrayList li = new ArrayList();
        for (OrdersVO vo : list) {
            li.add(String.valueOf(vo.getTotalPrice()));
            li.add(vo.getPayMethod());
        }
 
        for (int i = 0; i < li.size(); i++) {
            if (li.get(i).equals("CASH") || li.get(i).equals("cash")) {
                cash += Integer.parseInt((String) li.get(i - 1));
            } else if (li.get(i).equals("card") || li.get(i).equals("CARD")) {
                card += Integer.parseInt((String) (li.get(i - 1)));
            }
        }
 
        cash += cash1;
 
        jtf[0] = new JTextField();// 5만
        jtf[0].setText("0");
        jtf[1] = new JTextField();// 1만
        jtf[1].setText("0");
        jtf[2] = new JTextField();// 5천
        jtf[2].setText("0");
        jtf[3] = new JTextField();// 1천
        jtf[3].setText("0");
        jtf[4] = new JTextField();// 5백
        jtf[4].setText("0");
        jtf[5] = new JTextField();// 1백
        jtf[5].setText("0");
        jtf[6] = new JTextField();// 현금
        jtf[7] = new JTextField();// 카드
        jtf[8] = new JTextField();// 쿠폰
        jtf[9] = new JTextField();// 포인트
 
        // 가격 입력
        jlresult.setText(String.valueOf(cash));
        jtf[6].setText(String.valueOf(cash));
        jtf[7].setText(String.valueOf(card));
 
        jtf[0].addMouseListener(this);
        jtf[1].addMouseListener(this);
        jtf[2].addMouseListener(this);
        jtf[3].addMouseListener(this);
        jtf[4].addMouseListener(this);
        jtf[5].addMouseListener(this);
 
        jbtnend = new JButton("마감하기");
        jbtnend.setBackground(new Color(173,93,94));
        jbtnend.setBorderPainted(false);
        jbtnend.setFont(f);
        jbtnend.setForeground(Color.white);
 
        jp.add(jp2);
        jp.add(jl5man);
        jp.add(jl1man);
        jp.add(jl5chen);
        jp.add(jl1chen);
        jp.add(jl5bec);
        jp.add(jl1bec);
        jp.add(jlcash);
        jp.add(jlcard);
        jp.add(jlcoupon);
        jp.add(jlpoint);
        jp.add(jlresult);
 
        jp.add(jtf[0]);
        jp.add(jtf[1]);
        jp.add(jtf[2]);
        jp.add(jtf[3]);
        jp.add(jtf[4]);
        jp.add(jtf[5]);
        jp.add(jtf[6]);
        jp.add(jtf[7]);
        jp.add(jtf[8]);
        jp.add(jtf[9]);
        jp.add(jbtnend);
 
        jl5man.setFont(f);
        jl1man.setFont(f);
        jl5chen.setFont(f);
        jl1chen.setFont(f);
        jl5bec.setFont(f);
        jl1bec.setFont(f);
        jlcash.setFont(f);
        jlcard.setFont(f);
        jlcoupon.setFont(f);
        jlpoint.setFont(f);
        jbtnend.setFont(f);
        jlresult.setFont(f);
 
        jbtnend.addActionListener(this);
 
        jl5man.setBounds(50, 80, 100, 50);
        jl1man.setBounds(50, 160, 100, 50);
        jl5chen.setBounds(50, 240, 100, 50);
        jl1chen.setBounds(50, 320, 100, 50);
        jl5bec.setBounds(50, 400, 100, 50);
        jl1bec.setBounds(50, 480, 100, 50);
        jlcash.setBounds(400, 80, 100, 50);
        jlcard.setBounds(400, 160, 100, 50);
        jlcoupon.setBounds(400, 240, 100, 50);
        jlpoint.setBounds(400, 320, 100, 50);
        jlresult.setBounds(50, 600, 300, 50);
 
        jtf[0].setBounds(150, 80, 150, 50);
        jtf[1].setBounds(150, 160, 150, 50);
        jtf[2].setBounds(150, 240, 150, 50);
        jtf[3].setBounds(150, 320, 150, 50);
        jtf[4].setBounds(150, 400, 150, 50);
        jtf[5].setBounds(150, 480, 150, 50);
        jtf[6].setBounds(500, 80, 150, 50);
        jtf[7].setBounds(500, 160, 150, 50);
        jtf[8].setBounds(500, 240, 150, 50);
        jtf[9].setBounds(500, 320, 150, 50);
 
        int x = 20, y = 20;
        for (int i = 0; i < jbtn.length; i++) {
            x = 20;
            for (int j = 0; j < jbtn[i].length; j++) {
                if (i != 0) {
                    jbtn[i][j] = new JButton(btn[i * jbtn[i - 1].length + j]);
                } else {
                    jbtn[i][j] = new JButton(btn[j]);
                }
                jbtn[i][j].setLocation(x, y);
                x += 80;
                jbtn[i][j].setSize(60, 60);
                jp2.add(jbtn[i][j]);
                jbtn[i][j].addActionListener(this);
                jbtn[i][j].setBackground(new Color(215,188,171));
                jbtn[i][j].setBorderPainted(false);
                
 
            }
            y += 70;
        }
 
        jbtnend.setBounds(750, 600, 200, 100);
 
        jp.setLayout(null);
        jp2.setLayout(null);
        jp.setBounds(20, 120, 1060, 750);
        jp2.setBounds(700, 70, 260, 310);
        jp.repaint();
        calc();
 
    }
 
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawLine(50, 700, 370, 700);
        g.drawLine(390, 220, 390, 800);
 
    }
 
    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
 
    }
 
    @Override
    public void mousePressed(MouseEvent e) {
        c = e.getComponent();
        if (c instanceof JTextField) {
            if (c == jtf[0]) {
                currentBtnIdx = 0;
            } else if (c == jtf[1]) {
                currentBtnIdx = 1;
            } else if (c == jtf[2]) {
                currentBtnIdx = 2;
            } else if (c == jtf[3]) {
                currentBtnIdx = 3;
            } else if (c == jtf[4]) {
                currentBtnIdx = 4;
            } else if (c == jtf[5]) {
                currentBtnIdx = 5;
            }
 
        }
 
        /*
         * System.out.println(e); System.out.println(c);
         */ /*
             * x1 = e.getX(); y1 = e.getY(); System.out.println(x1 + "   " +
             * y1);
             */
    }
 
    @Override
    public void mouseReleased(MouseEvent e) {
 
    }
 
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
 
    }
 
    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
 
    }
 
    @Override
    public void actionPerformed(ActionEvent e) {
        String obj = e.getActionCommand();
 
        Object obj1 = e.getSource();
 
        // setValueToNumber();
        if (obj1 == jbtn[3][1]) {
            if (c != null) { // c가 널이 아니고
                try {
                    JTextField textField = (JTextField) c;
                    textField.setText(""); // 공백할당
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
 
        } else if (obj1 == jbtn[3][2]) { // [3][2]눌렀을때 다음 텍스트필드로 넘어가는 구문
            JTextField textField = jtf[currentBtnIdx];
            System.out.println("textField : " + textField.getText());
            if (textField.getText().trim().equals("")) {
                // 경고문 처리
                return;
            }
 
            // 이상 없으면 인덱스 증가시키고 다음 텍스트필드 가져와서 포커스
            currentBtnIdx++;
            System.out.println("currentBtnIdx : " + currentBtnIdx);
            textField = jtf[currentBtnIdx];
            textField.requestFocus();
            c = textField;
 
        } else if (obj1 == jbtnend) { // 마감하기 눌렀을때
            setVisible(false);
            new MainFrame();
 
            System.out.println("마감하기");
 
        } else if (c == jtf[0]) {
            int val = 0;
 
            try {
                val = Integer.parseInt(jtf[0].getText().trim() + obj);
            } catch (Exception e1) {
                // e1.printStackTrace();
            }
 
            jtf[0].setText(String.valueOf(val));
            Number1 = val;
            calc();
        } else if (c == jtf[1]) {
            int val = 0;
            try {
                val = Integer.parseInt(jtf[1].getText().trim() + obj);
            } catch (Exception e1) {
                // e1.printStackTrace();
            }
 
            jtf[1].setText(String.valueOf(val));
            Number2 = val;
            calc();
        } else if (c == jtf[2]) {
            int val = 0;
            try {
                val = Integer.parseInt(jtf[2].getText().trim() + obj);
            } catch (Exception e1) {
                // e1.printStackTrace();
            }
 
            jtf[2].setText(String.valueOf(val));
            Number3 = (val);
            calc();
        } else if (c == jtf[3]) {
            int val = 0;
            try {
                val = Integer.parseInt(jtf[3].getText().trim() + obj);
            } catch (Exception e1) {
                // e1.printStackTrace();
            }
 
            jtf[3].setText(String.valueOf(val));
            Number4 = (val);
            calc();
        } else if (c == jtf[4]) {
            int val = 0;
            try {
                val = Integer.parseInt(jtf[4].getText().trim() + obj);
            } catch (Exception e1) {
                // e1.printStackTrace();
            }
 
            jtf[4].setText(String.valueOf(val));
            Number5 = (val);
            calc();
        } else if (c == jtf[5]) {
            int val = 0;
            try {
                val = Integer.parseInt(jtf[5].getText().trim() + obj);
            } catch (Exception e1) {
                // e1.printStackTrace();
            }
 
            jtf[5].setText(String.valueOf(val));
            Number6 = (val);
            calc();
        }
 
    }
 
    private void calc() {
 
        int result = 0;
 
        result = ((Number1 * 50000) + (Number2 * 10000) + (Number3 * 5000) + (Number4 * 1000) + (Number5 * 500)
                + (Number6 * 100));
        result = cash - result;
        String str = "" + result;
        jlresult.setText(str);
    }
    /*
     * if (c == jtf[0]){
     * 
     * }String data1 = jtf[0].getText(); // 숫자가 연속으로 쓰여지게함 jtf[0].setText(data1
     * + obj);
     */
 
}
