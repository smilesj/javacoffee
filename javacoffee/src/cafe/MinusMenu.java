package cafe;

public class MinusMenu {
	Order order;

	MinusMenu(Order order) {
		this.order = order;
		if (order.jtb.getSelectedRow() != -1) {
			int num = (int) order.jtb.getValueAt(order.jtb.getSelectedRow(), 1);
			int price = (int) order.jtb.getValueAt(order.jtb.getSelectedRow(), 2);
			price /= (num);
			if (num > 1) {
				num--;
				order.jtb.setValueAt(num, order.jtb.getSelectedRow(), 1);
				order.jtb.setValueAt(num*price, order.jtb.getSelectedRow(), 2);
				order.cal();
			}
		}
	}
}
