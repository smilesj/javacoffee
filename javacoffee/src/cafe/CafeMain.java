package cafe;

import javax.swing.JFrame;

import server.TServer;

public class CafeMain {
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setTitle("[Cafe] JAVA COFFEE");
		f.add(new Login());
		f.setBounds(20, 20, 1100, 900);
		f.setVisible(true);
		new TServer();
	}
}