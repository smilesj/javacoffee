package customer;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import database.dao.CustomerDAO;
import database.vo.CustomerVO;

public class CMain extends JFrame {

	static JPanel jpBg;
	static JPanel jpInMain;
	static JButton btnBack;
	static JLabel lbName;
	static JLabel lbTBalance;
	static JTextField tfBalance;

	static CardLayout mainCL;
	static JPanel jpLogin; 	// 로그인
	static JPanel jpSignUp; // 회원가입
	static JPanel jpHome; 	// 홈
	static JPanel jpOrder; // 주문
	static JPanel jpHistory; // 히스토리
	static JPanel jpCoupon; // 쿠폰

	static String CUSTOMER_HP = "";
	static int CUSTOMER_POINT = 0;
	static int CUSTOMER_BALANCE = 0; // 충전카드금액
	
	ImageIcon bg, backx, back, od,odx, hmx, hm;
	Image img1, img2;

	CMain() {
		// 컴포넌트 생성
		
		jpBg = new JPanel();
		jpBg.setBackground(new Color(228, 202, 165));
		jpInMain = new JPanel();

		jpLogin = new CLogin();
		jpSignUp = new CSignUp();
		jpHome = new CHome();
		jpHistory = new CHistory();
		jpCoupon = new CCoupon();
		jpOrder = new COrder();
		mainCL = new CardLayout();
		
		btnBack = new JButton("Home");
		lbName = new JLabel("", Label.RIGHT);
		lbTBalance = new JLabel("카드 잔액 :                   원", Label.RIGHT);
		tfBalance = new JTextField(CUSTOMER_BALANCE + "");

		// 컴포넌트 크기, 위치, 초기 설정
		jpInMain.setLayout(mainCL);

		btnBack.setFont(new Font("굴림", Font.BOLD, 13));
		lbName.setFont(new Font("굴림", Font.BOLD, 20));
		lbTBalance.setFont(new Font("굴림", Font.BOLD, 15));
		tfBalance.setHorizontalAlignment(JTextField.RIGHT); // 우측정렬
		btnBack.setBackground(new Color(215,188,171));
		btnBack.setBorderPainted(false);
		
		jpInMain.setBounds(7, 80, 480, 630); // 학원
		btnBack.setBounds(20, 20, 80, 50);
		lbName.setBounds(370, 5, 150, 50);
		lbTBalance.setBounds(270, 45, 250, 30);
		tfBalance.setBounds(350, 45, 110, 27);
		tfBalance.setEditable(false);
		
		btnBack.setVisible(false);
		lbName.setVisible(false);
		lbTBalance.setVisible(false);
		tfBalance.setVisible(false);
		
		// 카드 금액 변경하기
		tfBalance.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!CUSTOMER_HP.equals("")) {
					
					CustomerVO user;
					CustomerDAO customerDAO = new CustomerDAO();
					if((user = customerDAO.selectCustomerOne(CMain.CUSTOMER_HP, CLogin.CUSTOMER_PW)) != null){		
						CMain.CUSTOMER_HP = user.getHp();	// 손님 HP 정보 저장
						CMain.CUSTOMER_BALANCE = user.getBalance();
						CMain.CUSTOMER_POINT = user.getPoint();
								
						CMain.tfBalance.setText(CMain.CUSTOMER_BALANCE + "");			
					}
				}

			}
		});

		btnBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!CUSTOMER_HP.equals("")) {
					btnBack.setVisible(false);
					lbName.setVisible(false);
					lbTBalance.setVisible(false);
					tfBalance.setVisible(false);
					CustomerVO user;
					CustomerDAO customerDAO = new CustomerDAO();
					if((user = customerDAO.selectCustomerOne(CMain.CUSTOMER_HP, CLogin.CUSTOMER_PW)) != null){		
						CMain.CUSTOMER_HP = user.getHp();	// 손님 HP 정보 저장
						CMain.CUSTOMER_BALANCE = user.getBalance();
						CMain.CUSTOMER_POINT = user.getPoint();
								
						CMain.tfBalance.setText(CMain.CUSTOMER_BALANCE + "");
				
						CMain.mainCL.show(CMain.jpInMain, "HomePage");
						
					} else {
						System.out.println("ID/PW를 확인해주세요.");
					}
					CMain.mainCL.show(CMain.jpInMain, "HomePage");
				}

			}
		});

		setLayout(null);
		
		jpInMain.add(jpLogin, "LoginPage");
		jpInMain.add(jpSignUp, "SignUpPage");
		jpInMain.add(jpOrder, "OrderPage");
		jpInMain.add(jpHome, "HomePage");
		jpInMain.add(jpHistory, "HistoryPage");
		jpInMain.add(jpCoupon, "CouponPage");

		// 컴포넌트 추가
		add(jpInMain);
		add(jpBg);
		add(btnBack);
		jpBg.add(lbName);
		add(lbTBalance);
		add(tfBalance);
		
		jpBg.setLayout(null);
		setTitle("JAVA COFFEE");
		jpBg.setBounds(0, 0, 500, 750);
		setBounds(600, 10, 500, 770);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false); // 창크기고정
		setVisible(true);
	}

	public static void main(String[] args) {
		new CMain();
	}

}
