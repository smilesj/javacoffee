package customer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.dao.OwnCouponDAO;
import database.vo.OwnCouponVO;

public class CCoupon extends JPanel implements ActionListener{

	JLabel lbTitle;
	JTable tbCoupon; // 주문내역 테이블
	JScrollPane jspTbCoupon; // 주문내역 테이블 스크롤
	DefaultTableModel tbCouponModel;

	OwnCouponDAO ocDAO;	//1
	static ArrayList<OwnCouponVO> ocv;

	CCoupon() {

		ocDAO = new OwnCouponDAO();	//1
		ocv = ocDAO.selectAll(CMain.CUSTOMER_HP);	//1
		

		setComponent();
		// setCouponTableData();
	}

	public void setComponent() {
		// 컴포넌트 생성
		lbTitle = new JLabel("<쿠폰 목록>");
		tbCouponModel = new DefaultTableModel();
		String[] colName = { "쿠폰명", "할인금액", "사용여부", "사용처", "사용일/만료일" };
		tbCoupon = new JTable(tbCouponModel);
		jspTbCoupon = new JScrollPane(tbCoupon, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		// 컴포넌트 위치, 크기, 초기설정
		lbTitle.setBounds(20, 10, 200, 50);
		lbTitle.setFont(new Font("굴림", Font.BOLD, 20));
		jspTbCoupon.setBounds(20, 70, 430, 500);
		tbCouponModel.setColumnIdentifiers(colName);
		Object[] row2 = { "가입감사", 2500, 1, "cafe001", null };

		tbCouponModel.addRow(row2);
		
		for(OwnCouponVO vo : ocv){
			//System.out.println( vo.getCpName()+ " : " + vo.getReducedprice()+ " : " +vo.getUsed() + " : " + vo.getCafeId()+ " : " + (vo.getUsedTime() != null?"null":"1"));
			Object[] row = { vo.getCpName(), vo.getReducedprice(), vo.getUsed(), vo.getCafeId(), "2016-12-12" };
			//System.out.println("row : " + row[0]+row[1]+row[2]+row[3]+row[4]);
			tbCouponModel.addRow(row);
			
		}
		tbCouponModel.fireTableDataChanged();
		//setCouponTableData();
		//tbCouponModel.fireTableDataChanged();
		
		// 컴포넌트 추가
		add(lbTitle);
		add(jspTbCoupon);

		setLayout(null);
		setBackground(new Color(228, 202, 165));
		setVisible(true);
	}

	// 쿠폰테이블에 데이터 추가하기
	public void setCouponTableData() {

		// tbCouponModel.setRowCount(0);
		for (OwnCouponVO vo : ocv) {
			Object[] row = { vo.getCpName(), vo.getReducedprice(), vo.getUsed(), vo.getCafeId(), vo.getUsedTime() };
			//System.out.println("ArrayList : " + vo.getCpName() + " : " + vo.getUsedTime()) ;
			
			if (vo.getUsed() == 1) { // 미사용 쿠폰일 경우
				row[4] = vo.getEndDate();

			}
			//System.out.println("ArrayList : " + vo.getCpName() + " : " + vo.getUsedTime()) ;

			// System.out.println("B : " + tbCouponModel.getRowCount());
			tbCouponModel.addRow(row);

			tbCouponModel.fireTableDataChanged();
			//tbCoupon.revalidate();
			// tbCoupon.setModel(tbCouponModel);
			//System.out.println("A : " + tbCouponModel.getRowCount());
			//System.out.println(tbCouponModel.getValueAt(tbCouponModel.getRowCount() - 1, 0));
			
			// System.out.print(vo.getCpName() + "\t" + vo.getReducedprice() +
			// "\t"+vo.getUsed() + "\t" );
			// System.out.println( vo.getCafeId()+ "\t" +
			// vo.getUsedTime()+"/"+vo.getEndDate());
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

	}

	
}
