/*
 * 작성자 : 원선재
 * 설명 : 손님에서 음료주문 받을때 하나의 음료 정보를 가지고 있는 클래스
 * 	      
 */
package customer;

public class SelectedDrink {
	private String drinkInfo;	// 음료이름, 아이스/핫, 사이즈
	private int cnt;
	private int price;

	SelectedDrink(){
		
	}

	public SelectedDrink(String drinkInfo, int cnt, int price) {
		super();
		this.drinkInfo = drinkInfo;
		this.cnt = cnt;
		this.price = price;
	}

	public String getDrinkInfo() {
		return drinkInfo;
	}

	public void setDrinkInfo(String drinkInfo) {
		this.drinkInfo = drinkInfo;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
