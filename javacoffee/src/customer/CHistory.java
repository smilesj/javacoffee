package customer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CHistory extends JPanel implements ActionListener {
	
	JLabel lbTitle;
	JTable tbHistory; 		// 주문내역 테이블
	JScrollPane jspTbHistory; // 주문내역 테이블 스크롤
	DefaultTableModel tbHistoryModel;
	
	CHistory(){
		setComponent();	
	}
	
	public void setComponent(){

		// 컴포넌트 생성
		lbTitle = new JLabel("<주문내역 목록>");
		tbHistoryModel = new DefaultTableModel();
		String[] colName = { "번호","주문일자", "결제종류", "결제금액", "사용처" };
		tbHistory = new JTable(tbHistoryModel);
		jspTbHistory = new JScrollPane(tbHistory, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		// 컴포넌트 위치, 크기, 초기설정
		lbTitle.setBounds(20, 10, 200, 50);
		lbTitle.setFont(new Font("굴림", Font.BOLD, 20));
		jspTbHistory.setBounds(20, 70, 430, 500);
		tbHistoryModel.setColumnIdentifiers(colName);
		
		// 리스너 추가
		
		// 컴포넌트 추가
		add(lbTitle);
		add(jspTbHistory);
		
		setLayout(null);
		setBackground(new Color(228, 202, 165));
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		
	}
}
