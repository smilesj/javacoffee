package customer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.dao.CustomerDAO;
import database.vo.CustomerVO;

public class CLogin extends JPanel implements ActionListener {

	static String CUSTOMER_PW = "";
	JLabel lbId;
	JLabel lbPw;
	JTextField tfId;
	JPasswordField tfPw;
	JButton btnLogin; // 로그인
	JButton btnJoin; // 회원가입	
	//JButton btnlogo;
	JPanel jpBg;
	CustomerDAO customerDAO;
	CustomerVO user;
	ImageIcon bgx, bg;
	Image img1, img2;
	CLogin() {

		setComponent();
		
		customerDAO = new CustomerDAO();
		
	}

	// 컴포넌트 메서드
	public void setComponent(){
		

		jpBg = new JPanel();
		jpBg.setBackground(new Color(228, 202, 165));
		
				
		bg = new ImageIcon("src/img/logo.png");
		jpBg = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				// drawImage(이미지객체,x,y,너비,높이,null);
				g.drawImage(bg.getImage(), 140, 50, 200, 200, null);
				setOpaque(false);
				super.paintComponent(g);
			
			}
		};
		
				
		Font f = new Font("맑은 고딕",Font.ITALIC, 15);
		Font l = new Font("맑은 고딕", Font.BOLD, 20);
		
		lbId = new JLabel("ID", JLabel.CENTER);
		lbPw = new JLabel("PW", JLabel.CENTER);
		tfId = new JTextField();
		tfPw = new JPasswordField();
		btnLogin = new JButton("로그인");
		btnLogin.setBackground(new Color(183,103,104));
		btnLogin.setBorderPainted(false);
		
		btnJoin = new JButton("아직 회원이 아니신가요?");
		btnJoin.setContentAreaFilled(false);
		btnJoin.setBorderPainted(false);
		
		Font font = new Font("돋움", Font.BOLD, 16);
		
		
		// 컴포넌트 크기,위치, 초기설정	
		lbId.setBounds(40, 290, 100, 30);
		lbPw.setBounds(30, 370, 120, 30);
		tfId.setBounds(120, 280, 260, 50);
		tfPw.setBounds(120, 360, 260, 50);
		btnLogin.setBounds(120, 440, 260, 50);
		btnJoin.setBounds(120, 510, 260, 40);
		//btnlogo.setBounds(100, 200, 300, 300)
		//CMain.btnBack.setVisible(false);
		
	
		lbId.setFont(font);
		lbPw.setFont(font);
		tfId.setFont(font);
		btnLogin.setFont(l);
		btnJoin.setFont(f);
	

		// 리스너 추가
		btnLogin.addActionListener(this);
		btnJoin.addActionListener(this);
		
		// 컴포넌트 추가
		setLayout(null);
		jpBg.add(lbId);
		jpBg.add(lbPw);
		jpBg.add(tfId);
		jpBg.add(tfPw);
		jpBg.add(btnLogin);
		jpBg.add(btnJoin);
		       
		add(jpBg);
		jpBg.setLayout(null);
		jpBg.setBounds(0, 0, 500, 750);		
		
		setVisible(true);
	}
		
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		if(obj == btnLogin) {
			String hp = tfId.getText();
			String pw = tfPw.getText();
			CUSTOMER_PW = pw;
			if((user = customerDAO.selectCustomerOne(hp, pw)) != null){		
				CMain.CUSTOMER_HP = user.getHp();	// 손님 HP 정보 저장
				CMain.CUSTOMER_BALANCE = user.getBalance();
				CMain.CUSTOMER_POINT = user.getPoint();
						
				// CMain의 상단 버튼 설정
				CMain.btnBack.setVisible(false);
				CMain.lbName.setVisible(true);
				CMain.lbTBalance.setVisible(true);
				CMain.tfBalance.setVisible(true);
				
				CMain.lbName.setText(user.getcName() + " 님");
				CMain.tfBalance.setText(CMain.CUSTOMER_BALANCE + "");
		
				CMain.jpOrder = new COrder();
				CMain.jpCoupon = new CCoupon();
				CMain.mainCL.show(CMain.jpInMain, "HomePage");
				
			} else {
				System.out.println("ID/PW를 확인해주세요.");
			}
			
		} else if(obj == btnJoin) {
			CMain.mainCL.show(CMain.jpInMain, "SignUpPage");
		}

	}

}
