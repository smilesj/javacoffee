package customer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class CHome extends JPanel implements ActionListener {
	JPanel jp;
	JButton btnOrder;
	JButton btnHistory;
	JButton btnCoupon;
	ImageIcon bg, od, odx, olx, ol, cpx, cp;
	Image img1, img2;

	CHome() {
		repaint();
		setComponent();
	}

	public void setComponent() {
		
		repaint();

		// 컴포넌트 생성
		//btnOrder = new JButton("주문");
		//btnHistory = new JButton("주문내역");
		//btnCoupon = new JButton("쿠폰");
		
		bg = new ImageIcon("src/img/logo.png");
		jp = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				// drawImage(이미지객체,x,y,너비,높이,null);
				g.drawImage(bg.getImage(), 140, 50, 200, 200, null);
				setOpaque(false);
				super.paintComponent(g);
			
			}
		};
		
		
		
		
		 // 주문 이미지
       odx = new ImageIcon("src/img/Corder.png");
       img1 = odx.getImage();
       img2 = img1.getScaledInstance(180, 200, java.awt.Image.SCALE_SMOOTH);
       od = new ImageIcon(img2);
       btnOrder = new JButton(od);
       btnOrder.setBorderPainted(false);
       btnOrder.setContentAreaFilled(false);
       
		 // 주문내역 이미지
       olx = new ImageIcon("src/img/Chistory.png");
       img1 = olx.getImage();
       img2 = img1.getScaledInstance(160, 90, java.awt.Image.SCALE_SMOOTH);
       ol = new ImageIcon(img2);
       btnHistory = new JButton(ol);
       btnHistory.setBorderPainted(false);
       btnHistory.setContentAreaFilled(false);
       
		 // 주문 이미지
       cpx = new ImageIcon("src/img/Ccoupon.png");
       img1 = cpx.getImage();
       img2 = img1.getScaledInstance(160, 90, java.awt.Image.SCALE_SMOOTH);
       od = new ImageIcon(img2);
       btnCoupon = new JButton(od);
       btnCoupon.setBorderPainted(false);
       btnCoupon.setContentAreaFilled(false);
       
      
		// 컴포넌트 위치, 크기, 초기설정
		btnOrder.setBounds(60, 350, 180, 200);
		btnHistory.setBounds(250, 350, 160, 90);
		btnCoupon.setBounds(250,460, 160, 90);

		// 리스너 추가
		btnOrder.addActionListener(this);
		btnHistory.addActionListener(this);
		btnCoupon.addActionListener(this);

		// 컴포넌트 추가
		setLayout(null);
		jp.setLayout(null);
		
		jp.add(btnOrder);
		jp.add(btnHistory);
		jp.add(btnCoupon);
		add(jp);
		
		jp.setBounds(0, 0, 500, 700);
		setBackground(new Color(228, 202, 165));
		setVisible(true);
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnOrder) {
			CMain.btnBack.setVisible(true);
			CMain.lbName.setVisible(true);
			CMain.lbTBalance.setVisible(true);
			CMain.tfBalance.setVisible(true);
			CMain.mainCL.show(CMain.jpInMain, "OrderPage");
			
		} else if (obj == btnHistory) {
			CMain.btnBack.setVisible(true);
			CMain.lbName.setVisible(true);
			CMain.lbTBalance.setVisible(true);
			CMain.tfBalance.setVisible(true);
			CMain.mainCL.show(CMain.jpInMain, "HistoryPage");
		} else if (obj == btnCoupon) {
			CMain.btnBack.setVisible(true);
			CMain.lbName.setVisible(true);
			CMain.lbTBalance.setVisible(true);
			CMain.tfBalance.setVisible(true);
			CMain.mainCL.show(CMain.jpInMain, "CouponPage");
		}

	}
}
