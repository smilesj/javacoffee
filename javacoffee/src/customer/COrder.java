package customer;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableModel;

import database.dao.CouponDAO;
import database.dao.CustomerDAO;
import database.dao.DrinkDAO;
import database.dao.OrderDetailDAO;
import database.dao.OrdersDAO;
import database.dao.OwnCouponDAO;
import database.vo.CouponVO;
import database.vo.CustomerVO;
import database.vo.DrinkVO;
import database.vo.OrderDetailVO;
import database.vo.OrdersVO;
import database.vo.OwnCouponVO;

public class COrder extends JPanel implements ActionListener, Runnable {

	// Socket
	//String serverIP = "192.168.0.191";
	String serverIP = "192.168.0.6";
	int port = 5000;
	Socket socket;
	PrintWriter pw;
	BufferedReader br;	
	
	JPanel jpOrder; // 메뉴 주문 Panel
	JPanel jpBottom; // 계속주문, 주문완료 버튼이 있는 Panel
	
	ImageIcon upx, up, downx, downn, iconxx, iconx;
	Image img1, img2;

	// jpOrder 컴포넌트
	CardLayout cl;
	int cardPage = 1;
	JPanel jpOCard1;
	JPanel jpOCard2;
	JPanel jpOCard3;

	JPanel jpMenuBar; // 커피, 스페셜, 티 버튼있는 Panel
	ButtonGroup btnKindsGroup;
	JToggleButton btnCoffee;
	JToggleButton btnSpecial;
	JToggleButton btnTea;

	JScrollPane jspDrink; // 메뉴 리스트 스크롤
	JList<String> drinkList;

	JPanel jpBtnIceHot;
	ButtonGroup btnIceHotGroup;
	JToggleButton btnIce;
	JToggleButton btnHot;

	JPanel jpSize; // 음료 사이즈
	ButtonGroup btnSizeGroup;
	JToggleButton btnSTall;
	JToggleButton btnSGrande;
	JToggleButton btnSVenti;

	JPanel jpCustom; // 샷추가, 시럽추가, 휘핑추가
	JToggleButton btnShot;
	JToggleButton btnSyrup;
	JToggleButton btnWhipping;
	NumPM npmShot;
	NumPM npmSyrup;

	////
	JLabel lbOTitle;
	JTable tbOrder; // 주문음료 테이블
	JScrollPane jspTbOrder; // 주문음료 테이블 스크롤
	DefaultTableModel tbOrderModel;
	GridBagConstraints gbc;

	JPanel jpOrderPM;
	NumPM npmOrder;
	boolean addOrder = false;

	////
	JTable tbPay; // 결제 테이블
	JScrollPane jspTbPay; // 결제 테이블 스크롤
	DefaultTableModel tbPayModel;

	JPanel jpTotal;
	JLabel lbTSum; // 합계
	JLabel lbSum; // 합계(숫자)
	JLabel lbCou; // 쿠폰
	// JLabel lbTCou; // 쿠폰(숫자)
	JLabel lbTPoint; // 포인트
	JLabel lbPoint; // 포인트(숫자)
	JLabel lbTTotal; // 총합계금액
	JLabel lbTotal; // 총합계금액(숫자)
	JComboBox<String> comCou; // 쿠폰 콤보박스
	JComboBox comPoint; // 포인트 콤보박스 , 100 단위로 사용가능
	
	

	// jpBottom 컴포넌트
	JButton btnPre; // 주문받을때 이전 penal로
	JButton btnNext; // 주문받을때 다음 penal로
	JButton btnOAdd; // 계속주문
	JButton btnOEnd; // 주문완료(결제하기)
	boolean payCheck = false; // 결제 다이얼로그창 보여주기 확인

	// DATA
	ArrayList<SelectedDrink> orderList;
	ArrayList<CouponVO> couponList;
	int point = CMain.CUSTOMER_POINT; // 임시 포인트

	// DB : 음료목록
	ArrayList<DrinkVO> drinkListDB;
	DrinkDAO drinkDAO;

	Set<String> coffeeNameList;
	Set<String> specialNameList;
	Set<String> teaNameList;

	// 음료별 가격 정보가져올때 필요한 변수들
	int drinkPrice = 0;
	String kinds = "COFFEE";
	String dname = "";
	char dsize = 'T';

	// DB : 주문
	ArrayList<OrdersVO> OrdersListDB;
	OrdersDAO ordersDAO;

	ArrayList<OrderDetailVO> ODListDB; // 주문 상세 내역 리스트
	OrderDetailDAO odDAO;

	static ArrayList<OwnCouponVO> notUsedCP; // 미사용 쿠폰정보 담고있는 리스트
	OwnCouponDAO ocDAO;
		
	ImageIcon lfx, lf, rtx, rt;
	Image img3, img4;
	
	COrder() {
		// DATA
		orderList = new ArrayList<SelectedDrink>();

		// couponList = new ArrayList<CouponVO>();
		// couponList.add(new CouponVO("cp001", "가입축하쿠폰", null, null, 1000));
		// couponList.add(new CouponVO("cp002", "메리크리스마스쿠폰", null, null, 3000));
		// couponList.add(new CouponVO("cp003", "졸리다쿠폰", null, null, 2000));

		ocDAO = new OwnCouponDAO();
		notUsedCP = ocDAO.selectAll(CMain.CUSTOMER_HP, 1);

		// DB
		drinkDAO = new DrinkDAO();
		drinkListDB = drinkDAO.selectAll(); // DB : drink Table 전체 읽어오기
		coffeeNameList = new HashSet<String>();
		specialNameList = new HashSet<String>();
		teaNameList = new HashSet<String>();

		for (DrinkVO vo : drinkListDB) {
			if (vo.getKinds().equals("COFFEE")) {
				coffeeNameList.add(vo.getDname());
			} else if (vo.getKinds().equals("SPECIAL")) {
				specialNameList.add(vo.getDname());
			} else if (vo.getKinds().equals("TEA")) {
				teaNameList.add(vo.getDname());
			}
		}

		// OrdersListDB = new ArrayList<OrdersVO>();
		ordersDAO = new OrdersDAO();
		odDAO = new OrderDetailDAO();

		// SWING
		setComponent(); // 컴포넌트 설정
		
		// Thread
		startCurrentClient();
	}
	

	public void setComponent() {

		setLayout(new BorderLayout());

		Font f = new Font("맑은 고딕", Font.BOLD, 20);
		
		//// 컴포넌트 생성
		jpOrder = new JPanel();
		jpBottom = new JPanel();

		// jpOrder
		cl = new CardLayout();
		jpOCard1 = new JPanel();
		jpOCard2 = new JPanel();
		jpOCard3 = new JPanel();

		// jpOrderCard1
		jpMenuBar = new JPanel();
		btnKindsGroup = new ButtonGroup();
		btnCoffee = new JToggleButton("Coffee");
		btnCoffee.setBackground(new Color(140, 160, 170));
		btnCoffee.setBorderPainted(false);
		btnCoffee.setForeground(Color.WHITE);
		btnCoffee.setFont(f);
		
		btnSpecial = new JToggleButton("Special");
		btnSpecial.setBackground(new Color(140, 160, 170));
		btnSpecial.setBorderPainted(false);
		btnSpecial.setForeground(Color.WHITE);
		btnSpecial.setFont(f);
		
		btnTea = new JToggleButton("Tea");
		btnTea.setBackground(new Color(140, 160, 170));
		btnTea.setBorderPainted(false);
		btnTea.setForeground(Color.WHITE);
		btnTea.setFont(f);
		
		drinkList = new JList<String>();
		jspDrink = new JScrollPane(drinkList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jpBtnIceHot = new JPanel();
		btnIceHotGroup = new ButtonGroup();
		btnIce = new JToggleButton("ICE");
		btnIce.setBackground(new Color(140, 160, 170));
		btnIce.setBorderPainted(false);
		btnIce.setForeground(Color.WHITE);
		btnIce.setFont(f);

		
		btnHot = new JToggleButton("HOT");
		btnHot.setBackground(new Color(140, 160, 170));
		btnHot.setBorderPainted(false);
		btnHot.setForeground(Color.WHITE);
		btnHot.setFont(f);

		jpSize = new JPanel();
		btnSizeGroup = new ButtonGroup();
		btnSTall = new JToggleButton("Tall");
		btnSTall.setBackground(new Color(140, 160, 170));
		btnSTall.setBorderPainted(false);
		btnSTall.setForeground(Color.WHITE);
		btnSTall.setFont(f);

		btnSGrande = new JToggleButton("Grande");
		btnSGrande.setBackground(new Color(140, 160, 170));
		btnSGrande.setBorderPainted(false);
		btnSGrande.setForeground(Color.WHITE);
		btnSGrande.setFont(f);

		btnSVenti = new JToggleButton("Venti");
		btnSVenti.setBackground(new Color(140, 160, 170));
		btnSVenti.setBorderPainted(false);
		btnSVenti.setForeground(Color.WHITE);
		btnSVenti.setFont(f);

		// jpOrderCard2
		jpCustom = new JPanel();
		btnShot = new JToggleButton("샷 추가");
		btnShot.setBackground(new Color(150, 170, 170));
		btnShot.setBorderPainted(false);
		btnShot.setForeground(Color.WHITE);
		btnShot.setFont(f);
		
		btnSyrup = new JToggleButton("시럽 추가");
		btnSyrup.setBackground(new Color(150, 170, 170));
		btnSyrup.setBorderPainted(false);
		btnSyrup.setForeground(Color.WHITE);
		btnSyrup.setFont(f);
		
		btnWhipping = new JToggleButton("휘핑 추가");
		btnWhipping.setBackground(new Color(150, 170, 170));
		btnWhipping.setBorderPainted(false);
		btnWhipping.setForeground(Color.WHITE);
		btnWhipping.setFont(f);
		
		npmShot = new NumPM();
		npmSyrup = new NumPM();

		lbOTitle = new JLabel("주문음료");
		tbOrderModel = new DefaultTableModel();
		String[] colName = { "주문음료", "개수", "가격" };
		jpOrderPM = new JPanel();
		npmOrder = new NumPM(1);

		tbOrder = new JTable(tbOrderModel);
		jspTbOrder = new JScrollPane(tbOrder, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		// jpOrderCard3
		tbPayModel = new DefaultTableModel();
		tbPay = new JTable(tbPayModel);
		jspTbPay = new JScrollPane(tbPay, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		jpTotal = new JPanel();
		lbTSum = new JLabel("합계", JLabel.CENTER);
		lbSum = new JLabel("2500", JLabel.RIGHT);
		lbCou = new JLabel("쿠폰", JLabel.CENTER);
		lbTPoint = new JLabel("포인트", JLabel.CENTER);
		lbPoint = new JLabel("/ " + point + "P", JLabel.RIGHT);
		lbTTotal = new JLabel("총결제금액", JLabel.CENTER);
		lbTotal = new JLabel("1500", JLabel.RIGHT);
		comPoint = new JComboBox();
		comCou = new JComboBox<String>();

		// jpBottom
		btnPre = new JButton("<");
		lfx = new ImageIcon("src/img/왼.png");
		img3 = lfx.getImage();
		img4 = img3.getScaledInstance(43, 43, java.awt.Image.SCALE_SMOOTH);
		lf = new ImageIcon(img4);
		btnPre = new JButton(lf);
		btnPre.setBorderPainted(false);
		btnPre.setContentAreaFilled(false);
		
		btnNext = new JButton(">");
		rtx = new ImageIcon("src/img/오른.png");
		img3 = rtx.getImage();
		img4 = img3.getScaledInstance(43, 43, java.awt.Image.SCALE_SMOOTH);
		rt = new ImageIcon(img4);
		btnNext = new JButton(rt);
		btnNext.setBorderPainted(false);
		btnNext.setContentAreaFilled(false);
		
		
		
		
		
		btnOAdd = new JButton("주문추가");
		btnOAdd.setBackground(new Color(150, 170, 150));
		btnOAdd.setBorderPainted(false);
		btnOAdd.setForeground(Color.WHITE);
		btnOAdd.setFont(f);
		
		btnOEnd = new JButton("결제하기");
		btnOEnd.setBackground(new Color(183,103,104));
		btnOEnd.setBorderPainted(false);
		btnOEnd.setForeground(Color.WHITE);
		btnOEnd.setFont(f);
		//// 컴포넌트 크기 및 위치 설정
		jpBottom.setPreferredSize(new Dimension(100, 60));
		jpBottom.setLayout(null);

		// jpOrder
		jpOrder.setLayout(cl);
		jpOCard1.setLayout(null);
		jpOCard2.setLayout(null);
		jpOCard3.setLayout(null);

		// jpOCard1
		jpMenuBar.setBounds(10, 0, 450, 60);
		jpMenuBar.setLayout(null);
		jspDrink.setBounds(10, 70, 450, 385);
		jpBtnIceHot.setBounds(10, 460, 450, 45);
		jpSize.setBounds(10, 510, 450, 45);

		btnCoffee.setBounds(40, 5, 120, 50);
		btnSpecial.setBounds(173, 5, 120, 50);
		btnTea.setBounds(305, 5, 120, 50);
		drinkList.setFixedCellHeight(50); // 리스트 아이템 높이 설정
		jpBtnIceHot.setLayout(null);
		btnIce.setBounds(20, 5, 200, 40);
		btnHot.setBounds(230, 5, 200, 40);
		jpSize.setLayout(null);
		btnSTall.setBounds(20, 5, 130, 40);
		btnSGrande.setBounds(160, 5, 130, 40);
		btnSVenti.setBounds(300, 5, 130, 40);

		// jpOCard2
		jpCustom.setLayout(new GridLayout(3, 3, 10, 10));
		jpCustom.setBounds(10, 5, 450, 150);
		lbOTitle.setBounds(20, 170, 450, 20);
		lbOTitle.setFont(new Font("맑은 고딕", Font.BOLD, 15));
		jspTbOrder.setBounds(20, 200, 430, 315);
		tbOrderModel.setColumnIdentifiers(colName);
		jpOrderPM.setBounds(230, 520, 220, 40);
		jpOrderPM.setLayout(null);

		// jpOCard3
		jspTbPay.setBounds(20, 10, 430, 280);
		tbPayModel.setColumnIdentifiers(colName);
		jpTotal.setLayout(null);
		jpTotal.setBounds(20, 295, 430, 250);

		GridBagLayout gbl = new GridBagLayout();
		gbc = new GridBagConstraints();
		jpTotal.setLayout(gbl);

		gbc = new GridBagConstraints();
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;

		jpTotalLayoutSet(lbTSum, 0, 0, 1, 1);
		jpTotalLayoutSet(lbSum, 1, 0, 2, 1);
		jpTotalLayoutSet(lbCou, 0, 1, 1, 1);
		jpTotalLayoutSet(comCou, 1, 1, 2, 1);
		jpTotalLayoutSet(lbTPoint, 0, 2, 1, 1);
		jpTotalLayoutSet(comPoint, 1, 2, 1, 1);
		jpTotalLayoutSet(lbPoint, 2, 2, 1, 1);
		jpTotalLayoutSet(lbTTotal, 0, 3, 1, 1);
		jpTotalLayoutSet(lbTotal, 1, 3, 2, 1);

		lbTSum.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		lbSum.setFont(new Font("맑은 고딕", Font.BOLD, 15));
		lbCou.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		lbTPoint.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		lbPoint.setFont(new Font("맑은 고딕", Font.BOLD, 15));
		lbTTotal.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		lbTotal.setFont(new Font("맑은 고딕", Font.BOLD, 27));

		// jpBottom
		btnPre.setBounds(10, 5, 50, 50);
		btnNext.setBounds(410, 5, 50, 50);
		btnOAdd.setBounds(70, 5, 160, 50);
		btnOEnd.setBounds(240, 5, 160, 50);

		//// 버튼그룹 설정
		btnKindsGroup.add(btnCoffee);
		btnKindsGroup.add(btnSpecial);
		btnKindsGroup.add(btnTea);

		btnIceHotGroup.add(btnIce);
		btnIceHotGroup.add(btnHot);

		btnSizeGroup.add(btnSTall);
		btnSizeGroup.add(btnSGrande);
		btnSizeGroup.add(btnSVenti);

		//// Panel에 컴포넌트 추가
		jpMenuBar.add(btnCoffee);
		jpMenuBar.add(btnSpecial);
		jpMenuBar.add(btnTea);

		jpBtnIceHot.add(btnIce);
		jpBtnIceHot.add(btnHot);

		jpSize.add(btnSTall);
		jpSize.add(btnSGrande);
		jpSize.add(btnSVenti);

		jpCustom.add(btnShot);
		jpCustom.add(npmShot);
		jpCustom.add(btnSyrup);
		jpCustom.add(npmSyrup);
		jpCustom.add(btnWhipping);
		JPanel j = new JPanel();
		j.setBackground(new Color(228, 202, 165));
		jpCustom.add(j); ///////////////////////////////////////////////////
		
		jpOrderPM.add(npmOrder);

		// 리스너 추가
		btnCoffee.addActionListener(this);
		btnSpecial.addActionListener(this);
		btnTea.addActionListener(this);
		btnIce.addActionListener(this);
		btnHot.addActionListener(this);
		btnShot.addActionListener(this);
		btnSyrup.addActionListener(this);
		btnWhipping.addActionListener(this);
		btnSTall.addActionListener(this);
		btnSGrande.addActionListener(this);
		btnSVenti.addActionListener(this);
		btnOAdd.addActionListener(this);
		btnOEnd.addActionListener(this);
		btnNext.addActionListener(this);
		btnPre.addActionListener(this);
		npmShot.btnMinus.addActionListener(this);
		npmShot.btnPlus.addActionListener(this);
		npmSyrup.btnMinus.addActionListener(this);
		npmSyrup.btnPlus.addActionListener(this);
		npmOrder.btnMinus.addActionListener(this);
		npmOrder.btnPlus.addActionListener(this);
		npmOrder.btnDelete.addActionListener(this);
		comCou.addActionListener(this);
		comPoint.addActionListener(this);

		/// 컴포넌트 초기값 설정
		btnOAdd.setVisible(false);
		btnCoffee.setSelected(true);
		btnIce.setSelected(true);
		btnSTall.setSelected(true);
		btnPre.setVisible(false);
		btnOEnd.setVisible(false);
		String[] listData = new String[coffeeNameList.size()];
		coffeeNameList.toArray(listData); // coffeeNameList를 배열로 바꿔서 listData에
											// 저장
		drinkList.setListData(listData);
		drinkList.setSelectedIndex(0);

		//// 컴포넌트 추가
		jpOCard1.add(jpMenuBar);
		jpOCard1.add(jspDrink);
		jpOCard1.add(jpBtnIceHot);
		jpOCard1.add(jpSize);

		jpOCard2.add(jpCustom);
		jpOCard2.add(lbOTitle);
		jpOCard2.add(jspTbOrder);
		jpOCard2.add(jpOrderPM);

		jpOCard3.add(jspTbPay);
		jpOCard3.add(jpTotal);

		jpBottom.add(btnPre);
		jpBottom.add(btnNext);
		jpBottom.add(btnOAdd);
		jpBottom.add(btnOEnd);

		jpOrder.add(jpOCard1, "jpOCard1");
		jpOrder.add(jpOCard2, "jpOCard2");
		jpOrder.add(jpOCard3, "jpOCard3");

		add(jpOrder, BorderLayout.CENTER);
		add(jpBottom, BorderLayout.SOUTH);

		//// 지울부분
		jpOCard1.setBackground(new Color(228, 202, 165));
		jpOCard2.setBackground(new Color(228, 202, 165));
		jpOCard3.setBackground(new Color(228, 202, 165));
		jpOrder.setBackground(new Color(228, 202, 165)); // 메뉴 주문 Panel
		jpBottom.setBackground(new Color(228, 202, 165));
		jpMenuBar.setBackground(new Color(228, 202, 165)); 
		jpBtnIceHot.setBackground(new Color(228, 202, 165));
		jpSize.setBackground(new Color(228, 202, 165));
		jpCustom.setBackground(new Color(228, 202, 165));
		jpOrderPM.setBackground(new Color(228, 202, 165));
		jpTotal.setBackground(new Color(228, 202, 165));
		
		// cl.show(jpOrder, "jpOCard2");

		setBackground(new Color(228, 202, 165));
		// setBounds(7, 65, 470, 790); // 학원
		// setBounds(7, 0, 470, 790); // 학원
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		if (obj == btnCoffee) {

			String[] listData = new String[coffeeNameList.size()];
			coffeeNameList.toArray(listData); // coffeeNameList를 배열로 바꿔서 listData에 저장
			btnHot.setEnabled(true);
			drinkList.setListData(listData);
			drinkList.setSelectedIndex(0);
			kinds = "COFFEE";

		} else if (obj == btnSpecial) {
			String[] listData = new String[specialNameList.size()];
			specialNameList.toArray(listData); // specialNameList를 배열로 바꿔서 listData에 저장
			btnHot.setEnabled(false);
			drinkList.setListData(listData);
			drinkList.setSelectedIndex(0);
			kinds = "SPECIAL";
		} else if (obj == btnTea) {
			String[] listData = new String[teaNameList.size()];
			teaNameList.toArray(listData); // teaNameList를 배열로 바꿔서 listData에 저장
			
			btnHot.setEnabled(true);
			drinkList.setListData(listData);
			drinkList.setSelectedIndex(0);
			kinds = "TEA";
		} else if (obj == btnIce) {

			//System.out.println("ICE btn click");

		} else if (obj == btnHot) {

			//System.out.println("HOT btn click");

		} else if (obj == btnShot) {

			int cnt = Integer.parseInt(npmShot.tfNum.getText());
			customClick(btnShot, "샷", cnt, 500 * cnt);

			if (btnShot.isSelected()) {
				npmShot.btnMinus.setEnabled(true);
				npmShot.btnPlus.setEnabled(true);
				npmShot.tfNum.setEditable(true);
			} else {
				npmShot.btnMinus.setEnabled(false);
				npmShot.btnPlus.setEnabled(false);
				npmShot.tfNum.setEditable(false);
				npmShot.tfNum.setText("1");
			}

		} else if (obj == btnSyrup) {

			int cnt = Integer.parseInt(npmSyrup.tfNum.getText());
			customClick(btnSyrup, "시럽", cnt, 500 * cnt);

			if (btnSyrup.isSelected()) {
				npmSyrup.btnMinus.setEnabled(true);
				npmSyrup.btnPlus.setEnabled(true);
				npmSyrup.tfNum.setEditable(true);
			} else {
				npmSyrup.btnMinus.setEnabled(false);
				npmSyrup.btnPlus.setEnabled(false);
				npmSyrup.tfNum.setEditable(false);
				npmSyrup.tfNum.setText("1");
			}

		} else if (obj == btnWhipping) {
			customClick(btnWhipping, "휘핑", 1, 700);

		} else if (obj == btnSTall) {
			dsize = 'T';
		} else if (obj == btnSGrande) {
			dsize = 'G';
		} else if (obj == btnSVenti) {
			dsize = 'V';
		} else if (obj == btnOAdd) {
			cardPage = 1;
			cl.show(jpOrder, "jpOCard1");

			checkOrderOne();
			settingVisible();

		} else if (obj == btnOEnd) {
			//System.out.println("결제하기");
			cardPage = 3;

			if (comCou.getItemCount() == 0) {
				comCou.addItem("쿠폰 미사용");
				for (OwnCouponVO list : notUsedCP) {
					comCou.addItem(list.getCpName() + " ( -" + list.getReducedprice() + "원)");
				}
			}
			/////////////

			point = CMain.CUSTOMER_POINT;
			lbPoint.setText(" / " + point + "P");

			if (comPoint.getItemCount() == 0) {
				comPoint.addItem("포인트 미사용");
				while (point >= 100) {
					comPoint.addItem(point);
					point -= 100;
				}
			}

			showTbPay();
			settingVisible();
			calPay();
			///////////////////////////////
			payCheck = !payCheck;
			if (!payCheck) {
				String msg = "";
				int totalCnt = tbPayModel.getRowCount();
				String totalPay = lbTotal.getText();
				if (totalCnt > 1) {
					msg = tbPayModel.getValueAt(0, 0).toString() + "외 " + totalCnt + "잔 " + totalPay + "원을 결제하시겠습니까?";
				} else {
					msg = tbPayModel.getValueAt(0, 0).toString() + " " + totalPay + "원을 결제하시겠습니까?";
				}
				int result = JOptionPane.showConfirmDialog(null, msg, "결제확인", JOptionPane.YES_NO_CANCEL_OPTION);

				if (result == JOptionPane.YES_OPTION) {
					// 결제하기
					/*** 수정하기 : 시퀀스 ORDERID, ****/
					String SEQ_orderID = null; // 수정하기
					String cafeID = "cafe0001"; // 수정하기
					String cpID = null; // 수정하기
					String payMethod = "charge"; // 충전카드로 결제
					int total = Integer.parseInt(lbTotal.getText().toString());
					int usePoint = 0;
					if (!comPoint.getSelectedItem().toString().equals("포인트 미사용")) {
						usePoint = Integer.parseInt(comPoint.getSelectedItem().toString());
					}

					OrdersVO ov = new OrdersVO(SEQ_orderID, cafeID, CMain.CUSTOMER_HP, cpID, null, payMethod, total,
							usePoint);
					if (ordersDAO.insertOrder(ov)) {
						
						SEQ_orderID = ordersDAO.selectOrderID(cafeID, CMain.CUSTOMER_HP);

						//System.out.println("SEQ_orderID :\t" + SEQ_orderID);
						addOrderDetailList(SEQ_orderID);
						for (OrderDetailVO odv : ODListDB) {
							odDAO.insertODOne(odv);
						}

						// 사용한 쿠폰 변경
						if(!comCou.getSelectedItem().toString().equals("쿠폰 미사용")){
							String couName = comCou.getSelectedItem().toString().substring(0, comCou.getSelectedItem().toString().indexOf(" ("));
							CouponDAO cdao = new CouponDAO();
							String couId = cdao.selectCouponID(couName);
							if (!couId.equals("")) {
								ocDAO.updateOCUSed(couId, cafeID, CMain.CUSTOMER_HP);
							}
						}
						
						// 충전카드에서 금액 차감
						CustomerDAO customerDAO = new CustomerDAO();
						customerDAO.UpdateCustomerBalance(CMain.CUSTOMER_HP, CMain.CUSTOMER_BALANCE-total);
						

						// 사용한 포인트 차감
						// CustomerDAO cDAO = new CustomerDAO().;
						if(!comPoint.getSelectedItem().toString().equals("포인트 미사용")){
							new CustomerDAO().updateCustomerPoint(CMain.CUSTOMER_HP, CMain.CUSTOMER_POINT - usePoint);
						}
						
						// 카페에 알림
						//ex) [주문]#주문번호#주문음료명
						//ex) [주문]#OR2016121300000001#아메리카노/I/T 외3잔
						String clientMsg = "[주문]#"+ SEQ_orderID + "#" + tbPayModel.getValueAt(0, 0).toString() + "외 " + totalCnt + "잔";
						pw.println(clientMsg);
						pw.flush();
						
						// 주문완료 후 데이터 초기화
						orderList = new ArrayList<SelectedDrink>();
						tbOrderModel.setRowCount(0);
						tbPayModel.setRowCount(0);
						cl.show(jpOrder, "jpOCard1");
						cardPage = 1;
						settingVisible();

						CustomerVO user;
						if((user = customerDAO.selectCustomerOne(CMain.CUSTOMER_HP, CLogin.CUSTOMER_PW)) != null){		
							CMain.CUSTOMER_HP = user.getHp();	// 손님 HP 정보 저장
							CMain.CUSTOMER_BALANCE = user.getBalance();
							CMain.CUSTOMER_POINT = user.getPoint();
									
							CMain.tfBalance.setText(CMain.CUSTOMER_BALANCE + "");
							
						}

						int dresult = JOptionPane.showConfirmDialog(null, "주문 완료되었습니다.", "주문완료",
								JOptionPane.DEFAULT_OPTION);
						if (dresult == JOptionPane.YES_OPTION) {
							CMain.btnBack.setVisible(false);
							CMain.lbName.setVisible(false);
							CMain.lbTBalance.setVisible(false);
							CMain.tfBalance.setVisible(false);
							CMain.mainCL.show(CMain.jpInMain, "HomePage");
						}
					}

				} else if (result == JOptionPane.NO_OPTION) {
					// 주문수정
					//System.out.println("다이얼로그 NO click");

				} else if (result == JOptionPane.CANCEL_OPTION) {
					// 주문취소
					//System.out.println("다이얼로그 CANCEL click");
					CMain.mainCL.show(CMain.jpInMain, "HomePage");
				}
			}
		} else if (obj == btnNext) {

			if (cardPage < 3)
				cardPage++;

			if (cardPage == 2) {
				dname = drinkList.getSelectedValue();
				drinkPrice = drinkDAO.selectDrinkPrice(kinds, dname, dsize);
				checkOrderOne();
			}

			settingVisible();
		} else if (obj == btnPre) {

			if (cardPage > 1)
				cardPage--;
			settingVisible();
		} else if (obj == npmShot.btnMinus) {
			int cnt = Integer.parseInt(npmShot.tfNum.getText());
			if (cnt > 0)
				cnt--;
			if (cnt == 0) {
				npmShot.btnMinus.setEnabled(false);
				btnShot.setSelected(false);
			}
			if (!btnShot.isSelected()) {
				npmShot.btnPlus.setEnabled(false);
				npmShot.tfNum.setEditable(false);
			}
			npmShot.tfNum.setText(String.valueOf(cnt));

			int listSize = orderList.size();
			if (listSize > 0) {
				int row = 1;
				while (!(orderList.get(orderList.size() - row).getDrinkInfo().equals("샷"))) {
					row++;
				}

				// OrderList 값 변경
				orderList.get(listSize - row).setCnt(cnt);
				orderList.get(listSize - row).setPrice(cnt * 500);
				// Order테이블 Cell값 변경
				tbOrderModel.setValueAt(cnt, listSize - row, 1);
				tbOrderModel.setValueAt(cnt * 500, listSize - row, 2);
				
				if(cnt == 0){
					tbOrderModel.removeRow(listSize-row);
				}
			}

		} else if (obj == npmShot.btnPlus) {
			int listSize = orderList.size();
			int cnt = Integer.parseInt(npmShot.tfNum.getText());
			if (listSize > 0) {
				int row = 1;
				while (!(orderList.get(orderList.size() - row).getDrinkInfo().equals("샷"))) {
					row++;
				}
				npmShot.tfNum.setText(String.valueOf(cnt+1));

				// OrderList 값 변경
				orderList.get(listSize - row).setCnt(cnt + 1);
				orderList.get(listSize - row).setPrice((cnt + 1) * 500);
				// Order테이블 Cell값 변경
				tbOrderModel.setValueAt(cnt + 1, listSize - row, 1);
				tbOrderModel.setValueAt((cnt + 1) * 500, listSize - row, 2);

			}

		} else if (obj == npmSyrup.btnMinus) {
			int cnt = Integer.parseInt(npmSyrup.tfNum.getText());
			if (cnt > 0)
				cnt--;
			if (cnt == 0) {
				npmSyrup.btnMinus.setEnabled(false);
				btnSyrup.setSelected(false);
			}
			if (!btnSyrup.isSelected()) {
				npmSyrup.btnPlus.setEnabled(false);
				npmSyrup.tfNum.setEditable(false);
			}
			npmSyrup.tfNum.setText(String.valueOf(cnt));

			int listSize = orderList.size();
			if (listSize > 0) {
				int row = 1;
				while (!(orderList.get(orderList.size() - row).getDrinkInfo().equals("시럽"))) {
					row++;
				}
				// OrderList 값 변경
				orderList.get(listSize - row).setCnt(cnt);
				orderList.get(listSize - row).setPrice(cnt * 500);
				// Order테이블 Cell값 변경
				tbOrderModel.setValueAt(cnt, listSize - row, 1);
				tbOrderModel.setValueAt(cnt * 500, listSize - row, 2);
				
				if(cnt == 0){
					tbOrderModel.removeRow(listSize-row);
				}
			}
		} else if (obj == npmSyrup.btnPlus) {
			int cnt = Integer.parseInt(npmSyrup.tfNum.getText());
			int listSize = orderList.size();
			if (listSize > 0) {
				int row = 1;
				while (!(orderList.get(orderList.size() - row).getDrinkInfo().equals("시럽"))) {
					row++;
				}
				npmSyrup.tfNum.setText(String.valueOf(cnt+1));
				orderList.get(listSize - row).setCnt(cnt + 1);
				orderList.get(listSize - row).setPrice((cnt + 1) * 500);
				tbOrderModel.setValueAt(cnt + 1, listSize - row, 1);
				tbOrderModel.setValueAt((cnt + 1) * 500, listSize - row, 2);
			}
		} else if (obj == npmOrder.btnMinus) {
			int selectRow = tbOrder.getSelectedRow();
			int drinkCnt = Integer.parseInt(tbOrder.getValueAt(selectRow, 1).toString());
			int selectDrinkPrice = orderList.get(selectRow).getPrice() / drinkCnt; // 해당음료의 한잔																				// 가격
			if (drinkCnt > 1) {
				orderList.get(selectRow).setCnt(drinkCnt - 1);
				orderList.get(selectRow).setPrice((drinkCnt - 1) * selectDrinkPrice);
				tbOrder.setValueAt(drinkCnt - 1, selectRow, 1);
				tbOrder.setValueAt((drinkCnt - 1) * selectDrinkPrice, selectRow, 2);
			} else if (drinkCnt == 1) { // 선택한 음료 개수가 0이되면 주문목록에서 삭제
				orderList.remove(selectRow);
				tbOrderModel.removeRow(selectRow);
			}

		} else if (obj == npmOrder.btnPlus) {
			int selectRow = tbOrder.getSelectedRow();
			int drinkCnt = Integer.parseInt(tbOrder.getValueAt(selectRow, 1).toString());
			int selectDrinkPrice = orderList.get(selectRow).getPrice() / drinkCnt; // 해당음료의
																					// 한잔
																					// 가격
			if (drinkCnt >= 1) {
				orderList.get(selectRow).setCnt(drinkCnt + 1);
				orderList.get(selectRow).setPrice((drinkCnt + 1) * selectDrinkPrice);
				tbOrder.setValueAt(drinkCnt + 1, selectRow, 1);
				tbOrder.setValueAt((drinkCnt + 1) * selectDrinkPrice, selectRow, 2);
			}
		} else if (obj == npmOrder.btnDelete) {

			int selectRow = tbOrder.getSelectedRow();
			orderList.remove(selectRow);
			tbOrderModel.removeRow(selectRow);
		} else if (obj == comCou) {

			calPay();
			if (!comCou.getSelectedItem().toString().contains("쿠폰 미사용")) {

				//System.out.println(Integer.parseInt(lbTotal.getText()));
			}
			
		} else if (obj == comPoint) {
			calPay();
		}

//		System.out.println("------ < order list > ------");
//		for (SelectedDrink d : orderList) {
//			System.out.println(d.getDrinkInfo() + "\t" + d.getCnt() + "\t" + d.getPrice());
//		}
//		System.out.println();

	} // actionPerformed end

	// 현재 선택한 음료 orderList와 tbOrder에 추가하는 메서드
	// 주문 음료 확인 메서드
	public void checkOrderOne() {
		if (cardPage == 2) {
			String dname = drinkList.getSelectedValue();
			char iceHot = (btnIce.isSelected() ? 'I' : 'H');
			char size = (btnSTall.isSelected() ? 'T' : (btnSGrande.isSelected() ? 'G' : 'V'));

			int cnt = 1; // 수정

			SelectedDrink one = new SelectedDrink(dname + "/" + iceHot + "/" + size, cnt, drinkPrice);
			orderList.add(one);

			Object[] row = { one.getDrinkInfo(), one.getCnt(), one.getPrice() };
			tbOrderModel.addRow(row);
		}
	}

	// 최종으로 결제할 음료목록 보여주는 메서드
	// 결제하기 음료테이블 확인 메서드
	public void showTbPay() {
		tbPayModel.setNumRows(0); // 테이블 목록 초기화
		for (SelectedDrink one : orderList) {
			//System.out.println(one.getDrinkInfo() + "\t" + one.getCnt() + "\t" + one.getPrice());
			Object[] row = { one.getDrinkInfo(), one.getCnt(), one.getPrice() };
			tbPayModel.addRow(row);
		}
	}

	// 상세주문 목록에 넣을 정보를 리스트에 담는 메서드
	public void addOrderDetailList(String orderID) {
		ODListDB = new ArrayList<OrderDetailVO>();
		int a = 0;
		int odSize = orderList.size();
		for (int i = 0; i < odSize; i++) {
			SelectedDrink one = orderList.get(i);
			String[] info = one.getDrinkInfo().split("/");
			int shot = 0;
			int wh = 0;
			int syrup = 0;

			/**** 수정하기 : 시퀀스 : SEQ_OD_ODID ****/
			String SEQ_odID = "OD2016121100000" + (int) (Math.random() * 10) + (int) (Math.random() * 10) + (a++); // 수정하기
			String drinkID = drinkDAO.selectDrinkID(info[0], info[2].charAt(0));

			while ((i + 1) < odSize && (orderList.get(i + 1).getDrinkInfo().equals("샷")
					|| orderList.get(i + 1).getDrinkInfo().equals("시럽")
					|| orderList.get(i + 1).getDrinkInfo().equals("휘핑"))) {
				if (orderList.get(i + 1).getDrinkInfo().equals("샷"))
					shot = orderList.get(i + 1).getCnt();
				else if (orderList.get(i + 1).getDrinkInfo().equals("휘핑"))
					wh = orderList.get(i + 1).getCnt();
				else if (orderList.get(i + 1).getDrinkInfo().equals("시럽"))
					syrup = orderList.get(i + 1).getCnt();

				i++;
			}
			OrderDetailVO odv = new OrderDetailVO(SEQ_odID, orderID, drinkID, one.getCnt(), info[2].charAt(0),
					info[1].charAt(0), shot, wh, syrup);
			ODListDB.add(odv);
		}
	}

	// 총결제금액 계산(쿠폰사용, 포인트사용) 메서드
	public void calPay() {
		int paySum = 0; // 합계
		int payCoupon = 0; // 쿠폰할인금액
		int payPoint = 0; // 포인트사용금액
		int payTotal = 0; // 총결제금액
		for (SelectedDrink one : orderList) {
			paySum += one.getPrice();
		}

		if (comPoint.getItemCount() > 1 && !comCou.getSelectedItem().toString().equals("쿠폰 미사용")) {

			payCoupon = Integer.parseInt(
					comCou.getSelectedItem().toString().substring(comCou.getSelectedItem().toString().indexOf("-") + 1,
							comCou.getSelectedItem().toString().indexOf("원")));
		}

		if (comPoint.getItemCount() > 1 && !(comPoint.getSelectedItem().toString().equals("포인트 미사용"))) {
			payPoint = Integer.parseInt(comPoint.getSelectedItem().toString());

		}

		payTotal = paySum - payCoupon - payPoint;

		lbSum.setText(String.valueOf(paySum));
		lbTotal.setText(String.valueOf(payTotal));
	}

	// Panel별 컴포넌트 visible 설정 메서드
	public void settingVisible() {
		if (cardPage == 1) {
			cl.show(jpOrder, "jpOCard1");
			btnCoffee.setSelected(true);
			drinkList.setSelectedIndex(0);
			btnIce.setSelected(true);
			btnSTall.setSelected(true);
			btnPre.setVisible(false);
			btnOAdd.setVisible(false);
			btnOEnd.setVisible(false);
			btnNext.setVisible(true);
		} else if (cardPage == 2) {
			cl.show(jpOrder, "jpOCard2");
			btnShot.setSelected(false);
			btnSyrup.setSelected(false);
			btnWhipping.setSelected(false);
			btnPre.setVisible(true);
			btnOAdd.setVisible(true);
			btnOEnd.setVisible(true);
			btnNext.setVisible(false);
			if (!btnShot.isSelected()) {
				npmShot.btnMinus.setEnabled(false);
				npmShot.btnPlus.setEnabled(false);
				npmShot.tfNum.setEditable(false);
				npmShot.tfNum.setText("1");
			}

			if (!btnSyrup.isSelected()) {
				npmSyrup.btnMinus.setEnabled(false);
				npmSyrup.btnPlus.setEnabled(false);
				npmSyrup.tfNum.setEditable(false);
				npmSyrup.tfNum.setText("1");
			}

		} else if (cardPage == 3) {
			cl.show(jpOrder, "jpOCard3");
		}
	}

	// 커스텀 버튼 클릭 => 리스트, 테이블에 추가, 삭제하는 메서드
	public void customClick(JToggleButton btn, String cName, int cnt, int price) {
		if (btn.isSelected()) {
			int i = 1;
			while (!(orderList.get(orderList.size() - i).getDrinkInfo().contains("/T")
					|| orderList.get(orderList.size() - i).getDrinkInfo().contains("/G")
					|| orderList.get(orderList.size() - i).getDrinkInfo().contains("/V"))) {
				//System.out.println(orderList.get(orderList.size() - i).getDrinkInfo());
				i++;
			}
			//System.out.println("i == >   " + i);
			if (orderList.size() > 0 && i >= 1) {
				SelectedDrink drink = new SelectedDrink(cName, cnt, price);
				orderList.add(drink);
				Object[] row = { cName, cnt, price };
				tbOrderModel.addRow(row);
			}

		} else {
			int row = 1;
			while (!orderList.get(orderList.size() - row).getDrinkInfo().equals(cName)) {
				row++;
				if (orderList.get(orderList.size() - row).getDrinkInfo().equals(cName))
					break;
			}

			orderList.remove(orderList.size() - row);
			tbOrderModel.removeRow(orderList.size() - row + 1);
		}
	}

	// GridBagLayout 컴포넌트 배치 메서드
	public void jpTotalLayoutSet(Component obj, int x, int y, int width, int height) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = width;
		gbc.gridheight = height;
		jpTotal.add(obj, gbc);
	}

	// Thread
	@Override
	public void run() {
		
		try {
			socket = new Socket(serverIP, port);
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
		
			String data = null;
			while(true){	// 서버가 보낸 것을 계속반복해서 읽는다.
				data = br.readLine();
				//System.out.println(data);
				//ex) [완성]#주문번호#주문음료명
				//ex) [완성]#OR2016121300000001#아메리카노/I/T 외3잔
				if(!data.equals("주문 요청되었습니다.") || !data.equals("주문 취소되었습니다.")){
					System.out.println("===> 서버 : " + data);
					String[] sdata = data.split("#"); 
//					for(int i = 0; i < sdata.length; i++){
//						System.out.println("SDATA : " + i + " : " + sdata[i]);
//					}
					//if(sdata[0].equals("[완성]")){
					if(data.contains("[완성]")){
						JOptionPane.showConfirmDialog(null, "주문번호 : " + sdata[1] +"\n주문음료 : " + sdata[2] +"\n 나왔습니다.", "알림2",JOptionPane.DEFAULT_OPTION);
						
					}
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("포트번호 오류");
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.out.println("IP 번호 오류");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void startCurrentClient(){
		Thread th = new Thread(this);	// 현재 객체를 쓰레드로 
		th.start();		// 쓰레드 시작
	}
}

class NumPM extends JPanel implements ActionListener {
	JButton btnMinus;
	JButton btnPlus;
	JTextField tfNum;
	JButton btnDelete;
	
	ImageIcon upx, up, downx, downn, iconxx, iconx;
	Image img1, img2;

	NumPM() {
		setLayout(null);
		btnMinus = new JButton("-");
		btnPlus = new JButton("+");
		tfNum = new JTextField("1");

		
		// up 이미지
		upx = new ImageIcon("src/img/우.png");
		img1 = upx.getImage();
		img2 = img1.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		up = new ImageIcon(img2);
		btnPlus = new JButton(up);
		btnPlus.setBorderPainted(false);
		btnPlus.setContentAreaFilled(false);

		// down 이미지
		downx = new ImageIcon("src/img/좌.png");
		img1 = downx.getImage();
		img2 = img1.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		downn = new ImageIcon(img2);
		btnMinus = new JButton(downn);
		btnMinus.setBorderPainted(false);
		btnMinus.setContentAreaFilled(false);
		
		btnMinus.setBounds(30, 5, 50, 30);
		btnPlus.setBounds(130, 5, 50, 30);
		tfNum.setBounds(90, 5, 30, 30);
		// btnPlus.addActionListener(this);


		btnMinus.setEnabled(false);
		btnPlus.setEnabled(false);
		tfNum.setEditable(false);

		add(btnMinus);
		add(btnPlus);
		add(tfNum);
		setBackground(new Color(228, 202, 165));
		setBounds(0, 0, 50, 60);
		setVisible(true);
	}

	NumPM(int index) {
		this();
		btnDelete = new JButton("X");
		// x 이미지
		iconxx = new ImageIcon("src/img/iconx3.png");
		img1 = iconxx.getImage();
		img2 = img1.getScaledInstance(33,33, java.awt.Image.SCALE_SMOOTH);
		iconx = new ImageIcon(img2);
		btnDelete = new JButton(iconx);
		btnDelete.setBorderPainted(false);
		btnDelete.setContentAreaFilled(false);
		
		


		btnMinus.setBounds(40, 5, 70, 30);
		btnPlus.setBounds(100, 5, 70, 30);
		btnDelete.setBounds(160, 5, 70, 30);

//		 btnMinus.addActionListener(new ActionListener() {
//		 @Override
//		 public void actionPerformed(ActionEvent e) {
//		
//		 }
//		 });
//
//		 btnDelete.addActionListener(this);
		
		btnMinus.setEnabled(true);
		btnPlus.setEnabled(true);
		tfNum.setVisible(false);
		btnDelete.setEnabled(true);

		add(btnDelete);
		setBounds(0, 0, 240, 60);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		int cnt = Integer.parseInt(tfNum.getText());

		if (obj == btnPlus) {
			cnt++;
			tfNum.setText(String.valueOf(cnt));
			if (!btnMinus.isEnabled())
				btnMinus.setEnabled(true);
		} else if (obj == btnDelete) {
			//System.out.println("delete click");
		}

	}

}