package customer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.dao.CustomerDAO;
import database.dao.OwnCouponDAO;
import database.vo.CustomerVO;
import database.vo.OwnCouponVO;

public class CSignUp extends JPanel implements ActionListener {

	JLabel[] lbTName; // ID, PW, PW확인, 사용자이름 라벨
	JTextField tfId;
	JPasswordField tfPw;
	JPasswordField tfRPw;
	JTextField tfName;

	JButton btnDCheck; // 중복확인 버튼
	JButton btnSignUp; // 가입하기 버튼
	JButton btnCancel; // 가입취소 버튼

	boolean check = false; // 중복확인 했는지 확인하는 변수
	boolean idOnly = false; // 중복아이디가 있는지 확인하는 변수

	CustomerDAO customerDAO;
	OwnCouponDAO ownCouponDAO;

	CSignUp() {
		setComponent();
		customerDAO = new CustomerDAO();
		ownCouponDAO = new OwnCouponDAO();
	}

	public void setComponent() {

		// 컴포넌트 생성
		lbTName = new JLabel[4];
		String[] tName = { "아이디(HP)", "비밀번호", "비밀번호확인", "이름" };
		tfId = new JTextField();
		tfPw = new JPasswordField();
		tfRPw = new JPasswordField();
		tfName = new JTextField();

		btnDCheck = new JButton("중복확인");
		btnSignUp = new JButton("가입하기");
		btnCancel = new JButton("취소");

		Font f = new Font("맑은 고딕", Font.BOLD, 20);
		Font s = new Font("맑은 고딕", Font.BOLD, 12);
		Font d = new Font("맑은 고딕", Font.BOLD, 15);

		// 컴포넌트 위치, 크기, 초기설정
		int y = 120;
		for (int i = 0; i < lbTName.length; i++) {
			lbTName[i] = new JLabel(tName[i], JLabel.RIGHT);
			lbTName[i].setFont(s);
			lbTName[i].setBounds(0, y, 145, 50);
			y += 60;
			add(lbTName[i]);
		}

		tfId.setBounds(170, 120, 180, 45);
		tfPw.setBounds(170, 180, 180, 45);
		tfRPw.setBounds(170, 240, 180, 45);
		tfName.setBounds(170, 300, 180, 45);

		btnDCheck.setBounds(360, 120, 100, 45);
		btnDCheck.setBackground(new Color(180, 200, 180));
		btnDCheck.setBorderPainted(false);
		btnDCheck.setFont(d);
		btnDCheck.setForeground(Color.WHITE);
		
		btnSignUp.setBounds(150, 380, 200, 45);
		btnSignUp.setBackground(new Color(183,103,104));
		btnSignUp.setBorderPainted(false);
		btnSignUp.setFont(f);
		btnSignUp.setForeground(Color.WHITE);

		btnCancel.setBounds(360, 380, 100, 45);
		btnCancel.setBackground(new Color(180, 170, 180));
		btnCancel.setBorderPainted(false);
		btnCancel.setFont(f);
		btnCancel.setForeground(Color.WHITE);

		tfId.setFont(s);
		tfPw.setFont(s);
		tfRPw.setFont(s);
		tfName.setFont(s);

		// 리스너 추가
		btnDCheck.addActionListener(this);
		btnSignUp.addActionListener(this);
		btnCancel.addActionListener(this);

		// 컴포넌트 추가
		setLayout(null);

		add(tfId);
		add(tfPw);
		add(tfRPw);
		add(tfName);
		add(btnDCheck);
		add(btnSignUp);
		add(btnCancel);

		setBackground(new Color(228, 202, 165));
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnDCheck) {
			String id = tfId.getText().trim();
			if (!id.equals("")) {
				check = true;
				CustomerVO v;
				String msg = "";
				if ((v = customerDAO.selectCustomerOne(id)) == null) { // 중복id가  없으면
					idOnly = true;
					msg = "사용 가능한 아이디입니다.";
				} else {
					check = false;
					msg = "이미 가입된 아이디입니다.";
				}
				JOptionPane.showConfirmDialog(null, msg, "아이디 중복확인 알림", JOptionPane.DEFAULT_OPTION);
			}
		} else if (obj == btnSignUp) {
			boolean pwCheck = tfPw.getText().trim().equals(tfRPw.getText().trim());
			boolean nameCheck = !(tfName.getText().trim().equals(""));
			String msg = "가입 완료되었습니다.";
			if (check && idOnly && pwCheck && nameCheck) { // 중복확인을 했고, 중복아이디가 없고 입력항목이 가입조건에 일치하면
				//System.out.println("yes");
				String id = tfId.getText().trim();
				String pw = tfPw.getText().trim();
				String name = tfName.getText().trim();
				CustomerVO newCus = new CustomerVO(id, pw, name, 0, null, 0); // 가입날짜는 널로
		
				if (customerDAO.insertCustomer(newCus)) {
					/** 수정하기 : 가입시 발행가능한 쿠폰들(쿠폰만료일이 가입날짜보다 더 클때)에 대해서 OWNCOUPON에 추가해주기 **/
					String cafeID = "cafe0001";				// 수정하기
					boolean b1 = ownCouponDAO.insertOCOne(new OwnCouponVO("CP00000001", cafeID, id, 1, null));	// 가입축하쿠폰발행
					boolean b2 = ownCouponDAO.insertOCOne(new OwnCouponVO("CP00000002", cafeID, id, 1, null));
					CMain.mainCL.show(CMain.jpInMain, "LoginPage");
//					if(b1 && b2) System.out.println("가입되었습니다.");
//					else  System.out.println("다시 가입해주세요.");
				}

			} else {
				//System.out.println("no");
				
				if (!check) {
					msg = "중복확인을 해주세요.";
				} else if (!pwCheck) {
					msg = "비밀번호가 일치하지 않습니다.";
				} else if (!nameCheck){
					msg = "이름을 입력해주세요.";
				}
			}			
			JOptionPane.showConfirmDialog(null, msg, "알림", JOptionPane.DEFAULT_OPTION);		
		} else if (obj == btnCancel) {
			CMain.mainCL.show(CMain.jpInMain, "LoginPage");
		}
	}
}
