/*
 * 작성자 : 원선재
 * 설명 : CUSTOMER TABLE DAO
 */
package database.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.CustomerVO;

import java.sql.Date;

public class CustomerDAO extends ParentDAO {
	public CustomerDAO() {

	}

	public ArrayList<CustomerVO> selectAll() {
		ArrayList<CustomerVO> list = new ArrayList<CustomerVO>();

		sb.setLength(0);
		sb.append("SELECT HP, PASSWORD, CNAME, BALANCE, REGIDATE, POINT ");
		sb.append("FROM CUSTOMER ");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String hp = rs.getString("HP");
				String password = rs.getString("PASSWORD");
				String cname = rs.getString("CNAME");
				int balance = rs.getInt("BALANCE");
				Date regidate = rs.getDate("REGIDATE");
				int point = rs.getInt("POINT");
				CustomerVO vo = new CustomerVO(hp, password, cname, balance, regidate, point);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	// HP로 고객정보 얻어오기
	public CustomerVO selectCustomerOne(String inputHP) {
		CustomerVO vo = null;

		sb.setLength(0);
		sb.append("SELECT HP, PASSWORD, CNAME, BALANCE, REGIDATE, POINT ");
		sb.append("FROM CUSTOMER ");
		sb.append("WHERE HP = ?");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, inputHP);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String hp = rs.getString("HP");
				String password = rs.getString("PASSWORD");
				String cname = rs.getString("CNAME");
				int balance = rs.getInt("BALANCE");
				Date regidate = rs.getDate("REGIDATE");
				int point = rs.getInt("POINT");
				vo = new CustomerVO(hp, password, cname, balance, regidate, point);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return vo;
	}

	// HP, pw로 고객정보 가져오기 (로그인시 사용)
	public CustomerVO selectCustomerOne(String inputHP, String inputPW) {
		CustomerVO vo = null;
		sb.setLength(0);
		sb.append("SELECT HP, PASSWORD, CNAME, BALANCE, REGIDATE, POINT ");
		sb.append("FROM CUSTOMER ");
		sb.append("WHERE HP = ? AND PASSWORD = ?");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, inputHP);
			pstmt.setString(2, inputPW);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String hp = rs.getString("HP");
				String password = rs.getString("PASSWORD");
				String cname = rs.getString("CNAME");
				int balance = rs.getInt("BALANCE");
				Date regidate = rs.getDate("REGIDATE");
				int point = rs.getInt("POINT");
				vo = new CustomerVO(hp, password, cname, balance, regidate, point);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return vo;
	}

	// customer 추가하기
	public boolean insertCustomer(CustomerVO vo) {
		boolean success = false;
		sb.setLength(0);
		sb.append("INSERT INTO CUSTOMER (HP, PASSWORD, CNAME, BALANCE, REGIDATE, POINT) ");
		sb.append("VALUES (?, ?, ?, ?, SYSTIMESTAMP, 2000) ");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, vo.getHp());
			pstmt.setString(2, vo.getPassWord());
			pstmt.setString(3, vo.getcName());
			pstmt.setInt(4, vo.getBalance());
			//pstmt.setInt(5, vo.getPoint());

			pstmt.executeUpdate();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	// 주리 추가
	// 선재 수정
	// 고객정보 - 포인트 변경 
	public CustomerVO UpdateCustomerPoint(CustomerVO vo) {
		sb.setLength(0);
		sb.append("UPDATE CADMIN.CUSTOMER ");
		sb.append("SET POINT = ? ");
		sb.append("WHERE HP = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, vo.getPoint());
			pstmt.setString(2, vo.getHp());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return vo;
	}
	
	// 선재 추가
	// HP, 변경 POINT를 매개변수로 포인트 변경
	public boolean updateCustomerPoint(String inputHP, int inputPoint) {
		boolean success = false;
		sb.setLength(0);
		sb.append("UPDATE CUSTOMER ");
		sb.append("SET POINT = ? ");
		sb.append("WHERE HP = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, inputPoint);
			pstmt.setString(2, inputHP);
			pstmt.executeUpdate();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	// 주리 추가
    // 충전
    public CustomerVO UpdateCustomerBalance(CustomerVO vo) {
        sb.setLength(0);
        sb.append("UPDATE CADMIN.CUSTOMER ");
        sb.append("SET BALANCE = ? ");
        sb.append("WHERE HP = ?");
 
        try {
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.setInt(1, vo.getBalance());
            pstmt.setString(2, vo.getHp());
            pstmt.executeUpdate();
 
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return vo;
    }
    
    // 선재 추가
    // Balance값 수정
    public void UpdateCustomerBalance(String inputHP, int inputBalance) {
        sb.setLength(0);
        sb.append("UPDATE CADMIN.CUSTOMER ");
        sb.append("SET BALANCE = ? ");
        sb.append("WHERE HP = ?");
 
        try {
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.setInt(1, inputBalance);
            pstmt.setString(2, inputHP);
            pstmt.executeUpdate();
 
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
