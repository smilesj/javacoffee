/*
 * 작 성 자 : 원 선 재
 * 설명 : ORDERDETAIL table DAO
 */
package database.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.CafeVO;
import database.vo.OrderDetailVO;
import database.vo.OrdersVO;

public class OrderDetailDAO extends ParentDAO {

	public OrderDetailDAO() {

	}

	// 상제주문 전체 목록 조회
	public ArrayList<OrderDetailVO> selectAll() {
		ArrayList<OrderDetailVO> list = new ArrayList<OrderDetailVO>();

		sb.setLength(0);
		sb.append("SELECT ODID, ORDERID, DRINKID, QUANTITY, DSIZE, ICEHOT, SHOT, WHIPPING, SYRUP ");
		sb.append("FROM ORDERDETAIL");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String cdid = rs.getString("ODID");
				String orderid = rs.getString("ORDERID");
				String drinkid = rs.getString("DRINKID");
				int quantity = rs.getInt("QUANTITY");
				char dsize = rs.getString("DSIZE").charAt(0);
				char icehot = rs.getString("ICEHOT").charAt(0);
				int shot = rs.getInt("SHOT");
				int whipping = rs.getInt("WHIPPING");
				int syrup = rs.getInt("SYRUP");
				OrderDetailVO vo = new OrderDetailVO(cdid, orderid, drinkid, quantity, dsize, icehot, shot, whipping, syrup);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	// 상세주문 추가
	public boolean insertODOne(OrderDetailVO vo) {

		boolean success = false;
		sb.setLength(0);
		sb.append("INSERT INTO ORDERDETAIL (ODID, ORDERID, DRINKID, QUANTITY, DSIZE, ICEHOT, SHOT, WHIPPING, SYRUP) ");
		sb.append("VALUES (('OD'||TO_CHAR(SYSDATE, 'yyyymmdd')||LPAD(SEQ_OD_ODID.NEXTVAL, 8, 0)), ?, ?, ?, ?, ?, ?, ?, ?) ");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, vo.getOrderId());
			pstmt.setString(2, vo.getDrinkId());
			pstmt.setInt(3, vo.getQuantity());
			pstmt.setString(4, String.valueOf(vo.getDsize()));
			pstmt.setString(5, String.valueOf(vo.getIcehot()));		
			pstmt.setInt(6, vo.getShot());
			pstmt.setInt(7, vo.getWhipping());
			pstmt.setInt(8, vo.getSyrup());

			pstmt.executeUpdate();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return success;
	}

	
	// 주문 취소하기
	public void deleteOrder(String orderid){
		boolean success = false;
		sb.setLength(0);
		sb.append("delete orderdetail ");
		sb.append("where orderid =? ");
		
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, orderid);
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}
