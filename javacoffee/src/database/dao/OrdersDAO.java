/*
 * 작 성 자 : 원 선 재
 * 설명 : ORDERS table DAO
 */
package database.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.OrdersVO;

public class OrdersDAO extends ParentDAO {

	public OrdersDAO() {
	}

	// 주문 전체 목록 조회
	public ArrayList<OrdersVO> selectAll() {
		ArrayList<OrdersVO> list = new ArrayList<OrdersVO>();

		sb.setLength(0);
		sb.append("SELECT ORDERID, CAFEID, HP, CPID, ORDERTIME, PAYMETHOD, TOTALPRICE, USEPOINT ");
		sb.append("FROM ORDERS");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String orderid = rs.getString("ORDERID");
				String cafeid = rs.getString("CAFEID");
				String hp = rs.getString("HP");
				String cpid = rs.getString("CPID");
				Date ordertime = rs.getDate("ORDERTIME");
				String paymethod = rs.getString("PAYMETHOD");
				int totalprice = rs.getInt("TOTALPRICE");
				int usepoint = rs.getInt("USEPOINT");

				OrdersVO vo = new OrdersVO(orderid, cafeid, hp, cpid, ordertime, paymethod, totalprice, usepoint);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	// 오늘 날짜로 목록 조회
	public ArrayList<OrdersVO> selectOne() {
		ArrayList<OrdersVO> list = new ArrayList<OrdersVO>();

		sb.setLength(0);
		sb.append("SELECT ORDERID, CAFEID, HP, CPID, ORDERTIME, PAYMETHOD, TOTALPRICE, USEPOINT ");
		sb.append("FROM ORDERS ");
		sb.append("WHERE to_char(ordertime,'YY/MM/DD')  = TO_CHAR(sysdate,'YY/MM/DD') ");
		sb.append("ORDER BY ORDERTIME DESC");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String orderid = rs.getString("ORDERID");
				String cafeid = rs.getString("CAFEID");
				String hp = rs.getString("HP");
				String cpid = rs.getString("CPID");
				Date ordertime = rs.getDate("ORDERTIME");
				String paymethod = rs.getString("PAYMETHOD");
				int totalprice = rs.getInt("TOTALPRICE");
				int usepoint = rs.getInt("USEPOINT");

				OrdersVO vo = new OrdersVO(orderid, cafeid, hp, cpid, ordertime, paymethod, totalprice, usepoint);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	
	// 특정 날짜로 목록 조회
		public ArrayList<OrdersVO> selectList(String ordertime1, String ordertime2) {
			ArrayList<OrdersVO> list = new ArrayList<OrdersVO>();

			sb.setLength(0);
			sb.append("SELECT ORDERID, CAFEID, HP, CPID, ORDERTIME, PAYMETHOD, TOTALPRICE, USEPOINT ");
			sb.append("FROM ORDERS ");
			sb.append("WHERE ORDERTIME between ? and ? ");
			sb.append("order by ordertime desc ");
			
			// 자리수 맞추는 알고리즘
			/*if(ordertime1.length()==7){
				String temp = ordertime1.substring(6);
				ordertime1 = ordertime1.substring(0,6);
				ordertime1 += 0;
				ordertime1 += temp;
				
			}else if (ordertime1.length()==6)
			if(ordertime2.length()<8){
				String temp = ordertime1.substring(6);
				ordertime2 = ordertime1.substring(0,6);
				ordertime2 += 0;
				ordertime2 += temp;
			}*/
			
			
			try {
				pstmt = conn.prepareStatement(sb.toString());
				pstmt.setString(1, ordertime1);
				pstmt.setString(2, ordertime2);
				//System.out.println(ordertime1);
				//System.out.println(ordertime2);
				
				rs = pstmt.executeQuery();
				while (rs.next()) {
					String orderid = rs.getString("ORDERID");
					String cafeid = rs.getString("CAFEID");
					String hp = rs.getString("HP");
					String cpid = rs.getString("CPID");
					Date ordertime = rs.getDate("ORDERTIME");
					String paymethod = rs.getString("PAYMETHOD");
					int totalprice = rs.getInt("TOTALPRICE");
					int usepoint = rs.getInt("USEPOINT");

					OrdersVO vo = new OrdersVO(orderid, cafeid, hp, cpid, ordertime, paymethod, totalprice, usepoint);
					list.add(vo);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			return list;
		}
	
	
	
	
	// ORDERID 조회
	public String selectOrderID(String cafeID, String hp){
		String orderID = "";
		sb.setLength(0);
		
		sb.append("SELECT ORDERID ");
		sb.append("FROM (SELECT ORDERID ");
		sb.append("FROM ORDERS ");
		sb.append("WHERE CAFEID = ? AND HP = ? ");
		sb.append("ORDER BY ORDERTIME DESC) ");
		sb.append("where ROWNUM = 1");
		
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, cafeID);
			pstmt.setString(2, hp);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderID = rs.getString("ORDERID");
			}		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return orderID;
	}
	
	// 주문 추가하기
	public boolean insertOrder(OrdersVO vo){
		boolean success = false;
		sb.setLength(0);
		sb.append("INSERT INTO ORDERS (ORDERID, CAFEID, HP, CPID, ORDERTIME, PAYMETHOD, TOTALPRICE, USEPOINT) ");
		sb.append("VALUES (('OR'||TO_CHAR(SYSDATE, 'yyyymmdd')||LPAD(SEQ_ORDERS_ORDERID.NEXTVAL, 8, 0)), ?, ?, ?, SYSTIMESTAMP, ?, ? , ?) ");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, vo.getCafeid());
			pstmt.setString(2, vo.getHp());
			pstmt.setString(3, vo.getCpId());
			pstmt.setString(4, vo.getPayMethod());
			pstmt.setInt(5, vo.getTotalPrice());
			pstmt.setInt(6, vo.getUsePoint());

			pstmt.executeUpdate();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return success;
	}
	
	// 주문 취소하기
	public void deleteOrder(String orderid){
		boolean success = false;
		sb.setLength(0);
		sb.append("delete orders ");
		sb.append("where orderid =? ");
		
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, orderid);
			
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	

	
	
}
