/*
 * 작 성 자 : 원 선 재
 * 설명 : COUPON table DAO
 * 수정하기 : INSERT 메서드 생성시 : COUPONID => CP||LPAD(SEQ_COUPON_CPID.NEXTVAL, 8, 0) 
 */
package database.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.CouponVO;

public class CouponDAO extends ParentDAO {

	public CouponDAO() {

	}

	// 쿠폰 전체 목록 조회
	public ArrayList<CouponVO> selectAll() {
		ArrayList<CouponVO> list = new ArrayList<CouponVO>();

		sb.setLength(0);
		sb.append("SELECT CPID, CPNAME, CPSTDATE, CPENDDATE, REDUCEDPRICE ");
		sb.append("FROM COUPON");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String cpid = rs.getString("CPID");
				String cpname = rs.getString("CPNAME");
				Date sdate = rs.getDate("CPSTDATE");
				Date edate = rs.getDate("CPENDDATE");
				int rprice = rs.getInt("REDUCEDPRICE");
				CouponVO vo = new CouponVO(cpid, cpname, sdate, edate, rprice);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;
	}
	
	// 쿠폰명으로 쿠폰ID 조회
	public String selectCouponID(String cpName){
		String cuID = "";
		
		sb.setLength(0);
		sb.append("SELECT CPID ");
		sb.append("FROM COUPON ");
		sb.append("WHERE CPNAME = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, cpName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cuID = rs.getString("CPID");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return cuID;
	}
	
}
