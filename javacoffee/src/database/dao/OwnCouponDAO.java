/*
 * 작 성 자 : 원 선 재
 * 설명 : OWNCOUPON table DAO
 */
package database.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.OwnCouponVO;

public class OwnCouponDAO extends ParentDAO {

	public OwnCouponDAO() {

	}

	// 소유쿠폰 전체 목록 조회
	public ArrayList<OwnCouponVO> selectAll() {
		ArrayList<OwnCouponVO> list = new ArrayList<OwnCouponVO>();

		sb.setLength(0);
		sb.append("SELECT CPID, CAFEID, HP, USED, USEDTIME ");
		sb.append("FROM OWNCOUPON");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String cpid = rs.getString("CPID");
				String cafeid = rs.getString("CAFEID");
				String hp = rs.getString("HP");
				int used = rs.getInt("USED");
				Date usedtime = rs.getDate("USEDTIME");
				OwnCouponVO vo = new OwnCouponVO(cpid, cafeid, hp, used, usedtime);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	// HP로 소유쿠폰 전체 목록 + 쿠폰정보(쿠폰이름, 할인금액) 조회
	public ArrayList<OwnCouponVO> selectAll(String inputHP) {
		ArrayList<OwnCouponVO> list = new ArrayList<OwnCouponVO>();

		sb.setLength(0);
		sb.append("SELECT O.CPID, O.CAFEID, O.HP, O.USED, O.USEDTIME, C.CPNAME, C.REDUCEDPRICE, C.CPSTDATE, C.CPENDDATE ");
		sb.append("FROM OWNCOUPON O, COUPON C ");
		sb.append("WHERE O.CPID = C.CPID AND O.HP = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, inputHP);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String cpid = rs.getString(1);//"O.CPID");
				String cafeid = rs.getString(2);//"O.CAFEID");
				String hp = rs.getString(3);//"O.HP");
				int used = rs.getInt(4);//"O.USED");
				Date usedtime = rs.getDate(5);//"O.USEDTIME");
				String cpname = rs.getString(6);//"C.CPNAME");
				int rprice = rs.getInt(7);//"C.REDUCEDPRICE");
				Date stdate = rs.getDate(8);
				Date enddate = rs.getDate(9);
				OwnCouponVO vo = new OwnCouponVO(cpid, cafeid, hp, used, usedtime, cpname, rprice, stdate, enddate);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	// HP, 사용여부 매개변수로 소유쿠폰 전체 목록 + 쿠폰정보(쿠폰이름, 할인금액) 조회
	// 미사용 : inputUsed = 1, 사용 : inputUsed = 0 
	public ArrayList<OwnCouponVO> selectAll(String inputHP, int inputUsed) {
		ArrayList<OwnCouponVO> list = new ArrayList<OwnCouponVO>();

		sb.setLength(0);
		sb.append("SELECT O.CPID, O.CAFEID, O.HP, O.USED, O.USEDTIME, C.CPNAME, C.REDUCEDPRICE, C.CPSTDATE, C.CPENDDATE  ");
		sb.append("FROM OWNCOUPON O, COUPON C ");
		sb.append("WHERE O.CPID = C.CPID AND O.HP = ? AND O.USED = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, inputHP);
			pstmt.setInt(2, inputUsed);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String cpid = rs.getString(1);//"O.CPID");
				String cafeid = rs.getString(2);//"O.CAFEID");
				String hp = rs.getString(3);//"O.HP");
				int used = rs.getInt(4);//"O.USED");
				Date usedtime = rs.getDate(5);//"O.USEDTIME");
				String cpname = rs.getString(6);//"C.CPNAME");
				int rprice = rs.getInt(7);//"C.REDUCEDPRICE");
				Date stdate = rs.getDate(8);
				Date enddate = rs.getDate(9);
				OwnCouponVO vo = new OwnCouponVO(cpid, cafeid, hp, used, usedtime, cpname, rprice, stdate, enddate);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	// 고객에게 쿠폰을 발행해주는 메서드
	public boolean insertOCOne(OwnCouponVO vo){
		boolean success = false;
		sb.setLength(0);
		sb.append("INSERT INTO OWNCOUPON (CPID, CAFEID, HP, USED, USEDTIME) ");
		sb.append("VALUES (?, ?, ?, 1, NULL) ");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, vo.getCpId());
			pstmt.setString(2, vo.getCafeId());
			pstmt.setString(3, vo.getHp());

			pstmt.executeUpdate();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return success;
	}
	
	// 쿠폰사용시 사용여부, 사용일자 update
	public boolean updateOCUSed(String inCpId, String inCafeId, String inHp){
		boolean success = false;
		sb.setLength(0);
		sb.append("UPDATE OWNCOUPON ");
		sb.append("SET USED = 0, USEDTIME = SYSTIMESTAMP ");
		sb.append("WHERE CPID = ? AND CAFEID = ? AND HP = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, inCpId);
			pstmt.setString(2, inCafeId);
			pstmt.setString(3, inHp);

			pstmt.executeUpdate();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
		return success;
	}
}
