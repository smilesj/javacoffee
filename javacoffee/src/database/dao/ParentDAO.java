/*
 * 작성자 : 원선재
 * 작성일 : 2016.12.08
 * 기능 : DB 연결
 */

package database.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ParentDAO {
	
	// 1. 변수설정
	private String driver = "oracle.jdbc.driver.OracleDriver";
	private String url = "jdbc:oracle:thin:@cafe.csz46hemdswu.ap-northeast-2.rds.amazonaws.com:1521:cafe";
	
	private String user = "cadmin";
	private String password = "americano8";
	
	static protected Connection conn = null;
	protected PreparedStatement pstmt = null;
	protected ResultSet rs = null;
	protected StringBuffer sb = new StringBuffer();
	
	public ParentDAO() {
		try {
			// 2. 드라이버 로딩
			Class.forName(driver);		
			
			// 3. Connection 객체 
			if(conn == null) {
				conn = DriverManager.getConnection(url, user, password);
				System.out.println("DB 연결 성공");
			}
			
		} catch (ClassNotFoundException e) {
			System.out.println("드라이버 로딩 실패");
		} catch (SQLException e) {
			System.out.println("DB 연결 실패");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new ParentDAO();
	}
}
