/*
 * 작 성 자 : 원 선 재
 * 설명 : REFUND table DAO
 * 수정하기 : INSERT 메서드 생성시 : REFUNDID => RF||LPAD(SEQ_REFUND_REFUNDID.NEXTVAL, 8, 0) 
 */
package database.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.RefundVO;

public class RefundDAO extends ParentDAO {

	public RefundDAO() {

	}

	// REFUND 전체 목록 조회
	public ArrayList<RefundVO> selectAll() {
		ArrayList<RefundVO> list = new ArrayList<RefundVO>();

		sb.setLength(0);
		sb.append("SELECT REFUNDID, ORDERID, RTIME, RPRICE, PAYMETHOD ");
		sb.append("FROM REFUND");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String refundid = rs.getString("REFUNDID");
				String orderid = rs.getString("ORDERID");
				Date rtime = rs.getDate("RTIME");
				int rprice = rs.getInt("RPRICE");
				String paymethod = rs.getString("PAYMETHOD");
				RefundVO vo = new RefundVO(refundid, orderid, rtime, rprice, paymethod);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;
	}
}
