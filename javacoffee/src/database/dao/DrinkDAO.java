/*
 * 설명 : DRINK table DAO
 */
package database.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.DrinkVO;

public class DrinkDAO extends ParentDAO {

	public DrinkDAO() {
	}

	public ArrayList<DrinkVO> selectAll() {
		ArrayList<DrinkVO> list = new ArrayList<DrinkVO>();
		// 기존에 사용한적 있다면 조희
		sb.setLength(0);
		sb.append("select drinkid, kinds, dname, price, dsize, shot, whipping, syrup ");
		sb.append("from cadmin.drink ");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String drinkid = rs.getString("drinkid");
				String kinds = rs.getString("kinds");
				String dname = rs.getString("dname");
				int price = rs.getInt("price");
				char dsize = rs.getString("dsize").charAt(0);
				int shot = rs.getInt("shot");
				int whipping = rs.getInt("whipping");
				int syrup = rs.getInt("syrup");
				DrinkVO vo = new DrinkVO(drinkid, kinds, dname, price, dsize, shot, whipping, syrup);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}

	public ArrayList<DrinkVO> selectCoffee() {
		ArrayList<DrinkVO> list = new ArrayList<DrinkVO>();
		// 기존에 사용한적 있다면 조희
		sb.setLength(0);
		sb.append("select drinkid, kinds, dname, price, dsize, shot, whipping, syrup ");
		sb.append("from cadmin.drink ");
		sb.append("where kinds = 'COFFEE' ");
		sb.append("order by dname, price ");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String drinkid = rs.getString("drinkid");
				String kinds = rs.getString("kinds");
				String dname = rs.getString("dname");
				int price = rs.getInt("price");
				char dsize = rs.getString("dsize").charAt(0);
				int shot = rs.getInt("shot");
				int whipping = rs.getInt("whipping");
				int syrup = rs.getInt("syrup");
				DrinkVO vo = new DrinkVO(drinkid, kinds, dname, price, dsize, shot, whipping, syrup);
				list.add(vo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}

	public ArrayList<DrinkVO> selectSpecial() {
		ArrayList<DrinkVO> list = new ArrayList<DrinkVO>();
		// 기존에 사용한적 있다면 조희
		sb.setLength(0);
		sb.append("select drinkid, kinds, dname, price, dsize, shot, whipping, syrup ");
		sb.append("from cadmin.drink ");
		sb.append("where kinds = 'SPECIAL' ");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String drinkid = rs.getString("drinkid");
				String kinds = rs.getString("kinds");
				String dname = rs.getString("dname");
				int price = rs.getInt("price");
				char dsize = rs.getString("dsize").charAt(0);
				int shot = rs.getInt("shot");
				int whipping = rs.getInt("whipping");
				int syrup = rs.getInt("syrup");
				DrinkVO vo = new DrinkVO(drinkid, kinds, dname, price, dsize, shot, whipping, syrup);
				list.add(vo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}

	public ArrayList<DrinkVO> selectTea() {
		ArrayList<DrinkVO> list = new ArrayList<DrinkVO>();
		// 기존에 사용한적 있다면 조희
		sb.setLength(0);
		sb.append("select drinkid, kinds, dname, price, dsize, shot, whipping, syrup ");
		sb.append("from cadmin.drink ");
		sb.append("where kinds = 'TEA' ");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String drinkid = rs.getString("drinkid");
				String kinds = rs.getString("kinds");
				String dname = rs.getString("dname");
				int price = rs.getInt("price");
				char dsize = rs.getString("dsize").charAt(0);
				int shot = rs.getInt("shot");
				int whipping = rs.getInt("whipping");
				int syrup = rs.getInt("syrup");
				DrinkVO vo = new DrinkVO(drinkid, kinds, dname, price, dsize, shot, whipping, syrup);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}

	// 선재 추가
	// 음료별 기본 가격 정보 가져오기
	public int selectDrinkPrice(String kinds, String dname, char dsize) {
		int price = 0;
		// 기존에 사용한적 있다면 조희
		sb.setLength(0);
		sb.append("SELECT PRICE ");
		sb.append("FROM DRINK ");
		sb.append("WHERE KINDS = ? AND DNAME = ? AND DSIZE = ?");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, kinds);
			pstmt.setString(2, dname);
			pstmt.setString(3, String.valueOf(dsize));
			rs = pstmt.executeQuery();
			while (rs.next()) {
				price = rs.getInt("PRICE");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return price;
	}

	// 선재 추가
	// 음로명, 사이즈로  DRINKID 가져오기
	public String selectDrinkID(String dname, char dsize) {
		String drinkID = "";
		// 기존에 사용한적 있다면 조희
		sb.setLength(0);
		sb.append("SELECT DRINKID ");
		sb.append("FROM DRINK ");
		sb.append("WHERE DNAME = ? AND DSIZE = ?");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, dname);
			pstmt.setString(2, String.valueOf(dsize));
			rs = pstmt.executeQuery();
			while (rs.next()) {
				drinkID = rs.getString("DRINKID");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return drinkID;
	}
}
