/*
 * 작 성 자 : 원 선 재
 * 설명 : CAFE table DAO
 */
package database.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import database.vo.CafeVO;

public class CafeDAO extends ParentDAO {

	public CafeDAO() {

	}

	// 카페 전체 목록 조회
	public ArrayList<CafeVO> selectAll() {
		ArrayList<CafeVO> list = new ArrayList<CafeVO>();

		sb.setLength(0);
		sb.append("SELECT CAFEID, PASSWORD ");
		sb.append("FROM CAFE");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String cafeid = rs.getString("CAFEID");
				String password = rs.getString("PASSWORD");
				CafeVO vo = new CafeVO(cafeid, password);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	
	// 카페 아이디있는지 조회하기
	public boolean cafeLogin(String inputId, String inputPw){
		boolean success = false;
		
		sb.setLength(0);
		sb.append("SELECT CAFEID, PASSWORD ");
		sb.append("FROM CAFE ");
		sb.append("WHERE CAFEID = ? AND PASSWORD = ?");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, inputId);
			pstmt.setString(2, inputPw);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				String cafeid = rs.getString("CAFEID");
				String password = rs.getString("PASSWORD");
				success = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			
		return success;
	}
}
