package database.vo;

import java.sql.Date;

public class OrdersVO {
	private String orderid;
	private String cafeid;
	private String hp;
	private String cpId;
	private Date orderTime;
	private String payMethod;
	private int totalPrice;
	private int usePoint;
	
	public OrdersVO(){}
	
	public OrdersVO(String orderid, String cafeid, String hp, String cpId, Date orderTime, String payMethod,
			int totalPrice, int usePoint) {
		super();
		this.orderid = orderid;
		this.cafeid = cafeid;
		this.hp = hp;
		this.cpId = cpId;
		this.orderTime = orderTime;
		this.payMethod = payMethod;
		this.totalPrice = totalPrice;
		this.usePoint = usePoint;
	}
	
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getCafeid() {
		return cafeid;
	}
	public void setCafeid(String cafeid) {
		this.cafeid = cafeid;
	}
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}
	public String getCpId() {
		return cpId;
	}
	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	public Date getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getUsePoint() {
		return usePoint;
	}
	public void setUsePoint(int usePoint) {
		this.usePoint = usePoint;
	}
	
}
