package database.vo;

public class CafeCustomerVO {
	 
    private String cafeId;
    private String hp;
    private int visitcnt;
 
    public CafeCustomerVO(String cafeId, String hp, int visitcnt) {
        super();
        this.cafeId = cafeId;
        this.hp = hp;
        this.visitcnt = visitcnt;
    }
 
    public String getCafeId() {
        return cafeId;
    }
 
    public void setCafeId(String cafeId) {
        this.cafeId = cafeId;
    }
 
    public String getHp() {
        return hp;
    }
 
    public void setHp(String hp) {
        this.hp = hp;
    }
 
    public int getVisitcnt() {
        return visitcnt;
    }
 
    public void setVisitcnt(int visitcnt) {
        this.visitcnt = visitcnt;
    }
 
}