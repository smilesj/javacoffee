package database.vo;

import java.sql.Date;

public class CouponVO {
 
    private String cpId;
    private String cpName;
    private Date cpstDate;
    private Date cpEndDate;
    private int reducedPrice;
 
    public CouponVO(String cpId, String cpName, Date cpstDate, Date cpEndDate, int reducedPrice) {
        super();
        this.cpId = cpId;
        this.cpName = cpName;
        this.cpstDate = cpstDate;
        this.cpEndDate = cpEndDate;
        this.reducedPrice = reducedPrice;
    }
 
    public String getCpId() {
        return cpId;
    }
 
    public void setCpId(String cpId) {
        this.cpId = cpId;
    }
 
    public String getCpName() {
        return cpName;
    }
 
    public void setCpName(String cpName) {
        this.cpName = cpName;
    }
 
    public Date getCpstDate() {
        return cpstDate;
    }
 
    public void setCpstDate(Date cpstDate) {
        this.cpstDate = cpstDate;
    }
 
    public Date getCpEndDate() {
        return cpEndDate;
    }
 
    public void setCpEndDate(Date cpEndDate) {
        this.cpEndDate = cpEndDate;
    }
 
    public int getReducedPrice() {
        return reducedPrice;
    }
 
    public void setReducedPrice(int reducedPrice) {
        this.reducedPrice = reducedPrice;
    }
}