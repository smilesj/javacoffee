package database.vo;

import java.sql.Date;

public class CustomerVO {

	private String hp;
	private String passWord;
	private String cName;
	private int balance;
	private Date regiDate;
	private int point;

	public CustomerVO(String hp, String passWord, String cName, int balance, Date regiDate, int point) {
		super();
		this.hp = hp;
		this.passWord = passWord;
		this.cName = cName;
		this.balance = balance;
		this.regiDate = regiDate;
		this.point = point;
	}
	
	public CustomerVO(String hp, String cName, int point) {
		super();
		this.hp = hp;
		this.cName = cName;
		this.point = point;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public Date getRegiDate() {
		return regiDate;
	}

	public void setRegiDate(Date regiDate) {
		this.regiDate = regiDate;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	
	
}
