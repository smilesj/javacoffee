package database.vo;

import java.sql.Date;

public class OwnCouponVO {
	private String cpId;
	private String cafeId;
	private String hp;
	private int used;
	private Date usedTime;
	
	// 선재추가
	private String cpName;
	private int reducedprice;
	private Date stDate;
	private Date endDate;
	
	public OwnCouponVO(){}
	public OwnCouponVO(String cpId, String cafeId, String hp, int used, Date usedTime) {
		super();
		this.cpId = cpId;
		this.cafeId = cafeId;
		this.hp = hp;
		this.used = used;
		this.usedTime = usedTime;
	}
	
	// 선재 추가
	// 소유한 쿠폰에 대한 전체 정보(쿠폰명 + 할인가격+ 만료일)를 담기위한 생성자
	public OwnCouponVO(String cpId, String cafeId, String hp, int used, Date usedTime, String cpName, int reducedprice, Date stDate, Date endDate) {
		super();
		this.cpId = cpId;
		this.cafeId = cafeId;
		this.hp = hp;
		this.used = used;
		this.usedTime = usedTime;
		this.cpName = cpName;
		this.reducedprice = reducedprice;
		this.stDate = stDate;
		this.endDate = endDate;
	}
	
	public String getCpId() {
		return cpId;
	}
	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	public String getCafeId() {
		return cafeId;
	}
	public void setCafeId(String cafeId) {
		this.cafeId = cafeId;
	}
	public String getHp() {
		return hp;
	}
	public void setHp(String hp) {
		this.hp = hp;
	}
	public int getUsed() {
		return used;
	}
	public void setUsed(int used) {
		this.used = used;
	}
	public Date getUsedTime() {
		return usedTime;
	}
	public void setUsedTime(Date usedTime) {
		this.usedTime = usedTime;
	}
	
	// 선재추가
	public String getCpName() {
		return cpName;
	}
	public void setCpName(String cpName) {
		this.cpName = cpName;
	}
	public int getReducedprice() {
		return reducedprice;
	}
	public void setReducedprice(int reducedprice) {
		this.reducedprice = reducedprice;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getStDate() {
		return stDate;
	}
	public void setStDate(Date stDate) {
		this.stDate = stDate;
	}
	
}
