/*
 * 작성자 : 원선재
 * 설명 : DRINK 테이블 VO(Value Object)
 */
package database.vo;

public class DrinkVO {

	private String drinkId;
	private String kinds;
	private String dname;
	private int price;
	private char dsize;
	private int shot;
	private int whipping;
	private int syrup;
	
	public DrinkVO(){
		
	}
	
	public DrinkVO(String kinds, String dname, int price) {
		super();
		this.kinds = kinds;
		this.dname = dname;
		this.price = price;
	}

	public DrinkVO(String drinkId, String kinds, String dname, int price, char dsize, int shot, int whipping,
			int syrup) {
		super();
		this.drinkId = drinkId;
		this.kinds = kinds;
		this.dname = dname;
		this.price = price;
		this.dsize = dsize;
		this.shot = shot;
		this.whipping = whipping;
		this.syrup = syrup;
	}

	public String getDrinkId() {
		return drinkId;
	}

	public void setDrinkId(String drinkId) {
		this.drinkId = drinkId;
	}

	public String getKinds() {
		return kinds;
	}

	public void setKinds(String kinds) {
		this.kinds = kinds;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public char getDsize() {
		return dsize;
	}

	public void setDsize(char dsize) {
		this.dsize = dsize;
	}

	public int getShot() {
		return shot;
	}

	public void setShot(int shot) {
		this.shot = shot;
	}

	public int getWhipping() {
		return whipping;
	}

	public void setWhipping(int whipping) {
		this.whipping = whipping;
	}

	public int getSyrup() {
		return syrup;
	}

	public void setSyrup(int syrup) {
		this.syrup = syrup;
	}
	
	
	
}
