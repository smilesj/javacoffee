package database.vo;

public class OrderDetailVO {
	private String odId;
	private String orderId;
	private String drinkId;
	private int quantity;
	private char dsize;
	private char icehot;
	private int shot;
	private int whipping;
	private int syrup;
	
	
	public OrderDetailVO() {}


	public OrderDetailVO(String odId, String orderId, String drinkId, int quantity, char dsize, char icehot, int shot,
			int whipping, int syrup) {
		super();
		this.odId = odId;
		this.orderId = orderId;
		this.drinkId = drinkId;
		this.quantity = quantity;
		this.dsize = dsize;
		this.icehot = icehot;
		this.shot = shot;
		this.whipping = whipping;
		this.syrup = syrup;
	}


	public String getOdId() {
		return odId;
	}


	public void setOdId(String odId) {
		this.odId = odId;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getDrinkId() {
		return drinkId;
	}


	public void setDrinkId(String drinkId) {
		this.drinkId = drinkId;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public char getDsize() {
		return dsize;
	}


	public void setDsize(char dsize) {
		this.dsize = dsize;
	}


	public char getIcehot() {
		return icehot;
	}


	public void setIcehot(char icehot) {
		this.icehot = icehot;
	}


	public int getShot() {
		return shot;
	}


	public void setShot(int shot) {
		this.shot = shot;
	}


	public int getWhipping() {
		return whipping;
	}


	public void setWhipping(int whipping) {
		this.whipping = whipping;
	}


	public int getSyrup() {
		return syrup;
	}


	public void setSyrup(int syrup) {
		this.syrup = syrup;
	}
	
	
	

}