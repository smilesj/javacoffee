package database.vo;

import java.sql.Date;

public class RefundVO {

   private String refundId;
   private String orderId;
   private Date rtime;
   private int rPrice;
   private String patMethod;
    
   public RefundVO(String refundId, String orderId, Date rtime, int rPrice, String patMethod) {
       super();
       this.refundId = refundId;
       this.orderId = orderId;
       this.rtime = rtime;
       this.rPrice = rPrice;
       this.patMethod = patMethod;
   }

   public String getRefundId() {
       return refundId;
   }

   public void setRefundId(String refundId) {
       this.refundId = refundId;
   }

   public String getOrderId() {
       return orderId;
   }

   public void setOrderId(String orderId) {
       this.orderId = orderId;
   }

   public Date getRtime() {
       return rtime;
   }

   public void setRtime(Date rtime) {
       this.rtime = rtime;
   }

   public int getrPrice() {
       return rPrice;
   }

   public void setrPrice(int rPrice) {
       this.rPrice = rPrice;
   }

   public String getPatMethod() {
       return patMethod;
   }

   public void setPatMethod(String patMethod) {
       this.patMethod = patMethod;
   }
    
    
    
}