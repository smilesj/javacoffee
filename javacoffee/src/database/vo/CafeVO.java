package database.vo;

public class CafeVO {
	 
    private String cafeId;
    private String passWord;
 
    public CafeVO(String cafeId, String passWord) {
        super();
        this.cafeId = cafeId;
        this.passWord = passWord;
    }
 
    public String getCafeId() {
        return cafeId;
    }
 
    public void setCafeId(String cafeId) {
        this.cafeId = cafeId;
    }
 
    public String getPassWord() {
        return passWord;
    }
 
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
 
}